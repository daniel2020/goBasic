package main

import (
	"io/ioutil"
	"strings"
	"fmt"
)

func main()  {
/*
ioutil
	ReadFile()
	WriteFile()
	ReadDir()

 */
	//1读取文件
/*	path := "/home/daniel/tmp/demo02_time.go"
	chs, err := ioutil.ReadFile(path)

	if err != nil {
		fmt.Println("err:", err)
		return
	}
	fmt.Println(string(chs))

	//2写文件
	path2 := "/home/daniel/tmp/f02.txt"
	err = ioutil.WriteFile(path2, []byte("天天向上"), os.ModePerm)

	if err != nil {
		fmt.Println("err:", err.Error())
	}
*/

	//从字符串中读取
	str := "asdfghjkl"
	reader := strings.NewReader(str)

	data,_ := ioutil.ReadAll(reader)
	fmt.Println(string(data))


	path2 := "/home/daniel/tmp/"
	infos, _ := ioutil.ReadDir(path2)
	fmt.Println(len(infos))

	for _,info := range infos{
		fmt.Println(info.Name(), info.IsDir(), info.Size(), info.ModTime())
	}

}

