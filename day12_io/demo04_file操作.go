package main

import (
	"fmt"
	"path/filepath"
	"os"
	/*
	文件操作
		打开文件
		创建文件，创建文件夹
		关闭文件
		删除文件
	 */
	"path"
)
func main()  {




	test1()
}

func test()  {

	//路径
	fileName1 := "/toMid/06_gitmayu2/src/goBasic/day12_io/demo02_time.go"
	fileName2 := "demo02_time.go"

	fmt.Println(filepath.IsAbs(fileName1))
	fmt.Println(filepath.IsAbs(fileName2))
	fmt.Println(filepath.Abs(fileName1))
	fmt.Println(filepath.Abs(fileName2))

	fmt.Println("获取父目录:", path.Join(fileName1, ".."))

	//创见目录
	//err := os.Mkdir("/home/daniel/tmp/aa", os.ModePerm)
	//if err != nil {
	//	fmt.Println(err.Error())
	//}

	//创建多次目录
	err := os.Mkdir("/home/daniel/tmp/aa/bb/cc/", os.ModePerm)
	if err!=nil {
		fmt.Println("err:", err.Error())
	} else {
		fmt.Println("文件夹创建成功")
	}

	//创建文件
	//file1, err := os.Create("/home/daniel/tmp/aa/abc.text")
	//if err != nil {
	//	fmt.Println(err.Error())
	//}
	//
	//fmt.Println(file1)


	//创见一个相对路径下的文件


	fmt.Println("======================")

	//打开文件
	//fileName3 := "/home/daniel/tmp/aa/abc.text"
	//file,err := os.Open(fileName3)
	//if err != nil {
	//	fmt.Println(err.Error())
	//}
	//fmt.Println(file)


	//file4, err := os.Open(fileName3, os.O_WRONLY|os.O_CREATE, os.ModePerm)

	//关闭文件
	//file4.Close()


	//删除文件
	fileName4 := "/home/daniel/tmp/aa/bb.text"
	err= os.Remove(fileName4)
	if err != nil {
		fmt.Println(err.Error())
	}
}

func test1()  {
	fmt.Println("=========================================")

	//路径  绝对路径
	path1 := "/home/daniel/tmp/aa/bb/cc/"
	path3 := "/home/daniel/tmp/aa/bb/cc/f01.text"
	path2 := "tmp/aa/bb/cc/"
	fmt.Println(filepath.IsAbs(path1))
	fmt.Println(filepath.IsAbs(path2))

	//创建文件夹
	err := os.Mkdir(path1, os.ModePerm)
	if err != nil {
		fmt.Println("err:", err.Error())
	} else {
		fmt.Println("11111----创建成功")
	}

	err = os.MkdirAll(path1, os.ModePerm)
	if err != nil {
		fmt.Println("err:", err.Error())
	} else {
		fmt.Println("22222--创建成功")
	}

	fmt.Println("-----------------------------")

	//创建文件
	file,err:= os.Create(path3)
	if err != nil {
		fmt.Println("err:", err.Error())
	}
	fmt.Println(file.Name())

	fmt.Println("-----------------------------")

	//打开文件
	file1, err := os.Open(path1)
	if err != nil {
		fmt.Println("err:", err.Error())
	}
	fmt.Println(file1.Name())
	fileInfo, _ := file1.Stat()
	fmt.Println(fileInfo.Name() , fileInfo.IsDir())

	fmt.Println("-----------------------------")

	file2, err := os.OpenFile(path3, os.O_RDONLY, os.ModePerm)
	fmt.Println(file2.Name())

	file3 ,err := os.OpenFile(path3, os.O_WRONLY, os.ModePerm)
	fmt.Println(file3.Name())

	fmt.Println("-----------------------------")

	//关闭文件
	err = file3.Close()
	fmt.Println("err:", err)

	err = file3.Close()
	fmt.Println("err:", err)		//file already closed
	fmt.Println("-----------------------------")

	//删除文件
	err = os.Remove(path3)
	if err != nil {
		fmt.Println("err:", err)
	} else {
		fmt.Println("33333----删除成功")
	}

	err = os.Remove(path3)
	if err != nil {
		fmt.Println("err:", err)
	} else {

		fmt.Println("33333----删除成功")
	}

}





