package main

import (
	"os"
	"fmt"
	"io"
)

func main()  {
	/*
	//路径
	fileName1 := "/toMid/06_gitmayu2/src/goBasic/day12_io/demo02_time.go"
	fileName2 := "demo02_time.go"

	*/

	//1打开文件
	//路径
	fileName1 := "/home/daniel/tmp/aa/abc.text"

	file,err := os.Open(fileName1)
	if err != nil {
		fmt.Println("err:", err.Error())
		return
	}

	//2读写文件
	//bs := make([]byte,4,10)
/*	n,err := file.Read(bs)

	fmt.Println(err)
	fmt.Println(n)
	fmt.Println(bs)
	fmt.Println(string(bs))

	//第二次读取
	n,err = file.Read(bs)

	fmt.Println(err)
	fmt.Println(n)
	fmt.Println(bs)
	fmt.Println(string(bs))

	//第三次读取
	n,err = file.Read(bs)

	fmt.Println(err)
	fmt.Println(n)
	fmt.Println(bs)
	fmt.Println(string(bs))

	//第四次读取
	n,err = file.Read(bs)

	fmt.Println(err)
	fmt.Println(n)
	fmt.Println(bs)
	fmt.Println(string(bs))
*/
	fmt.Println("======================")

	bs2 := make([]byte, 4, 4)
	for {
		n,err := file.Read(bs2)
		if n == 0 || err == io.EOF {
			fmt.Println("读取到文件末尾了")
			break
		}
		fmt.Println(string(bs2[:n]))
	}



	//3关闭文件
	file.Close()
}


