package main

import (
	"fmt"
	"io/ioutil"
)

var disPath string = "/toMid/0F_Chain/01_BChain0514/01_go/pro"
var dstFile string = "/home/daniel/tmp/萌猫3.jpg"
var disPath2 string = "/home/daniel/tmp"

/*
作业1：遍历文件夹pro，包含子目录
	递归函数
 */
func main() {
	listDist(disPath)
	//listDist(dstFile)
	//listDist(disPath2)

}

/**
遍历目录
 */
func listDist(path string) {

	//获取所文件info
	infos, err := ioutil.ReadDir(path)

	if err != nil {
		fmt.Println("err:", err.Error())
		return
	}

	//遍历
	for _, info := range infos {
		subPath := path + "/" + info.Name()

		//目录
		if info.IsDir() {
			fmt.Println("d", subPath)
			listDist(subPath)

			//是文件
		} else {
			fmt.Println("-", subPath)
		}
	}
}
