package main

import (
	"fmt"
	"os"
	"bufio"
	"time"
)

func main()  {
	fmt.Println("1111")
	start()
}

func start() {
	//聊天记录文件
	chatlogPath := "chatlog.text"
	chatlogFile,_ := os.OpenFile(chatlogPath, os.O_CREATE|os.O_WRONLY|os.O_APPEND, os.ModePerm)
	reader := bufio.NewReader(os.Stdin)

	defer chatlogFile.Close()

	//设置 名字，交替标记
	flag := true
	content := ""

	name := ""
	for {
		if flag {
			name = "小明:"
		} else {
			name = "小红:"
		}
		flag = !flag


		//从键盘读取
		data,_ := reader.ReadString('\n')

		if data == "over\n" {
			break
		}

		content = name + data
		dateStr := time.Now().Format("2006-01-02 ")
		timeStr := time.Now().Format("15:04:05")

		//写出
		fmt.Println(dateStr, timeStr)
		fmt.Println(content)

		chatlogFile.WriteString(dateStr)
		chatlogFile.WriteString(timeStr)
		chatlogFile.WriteString(content)

	}


}
