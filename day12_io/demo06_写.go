package main

import (
	"os"
	"fmt"
	"io"
)

func main()  {
	//test1()

	//test2()

	test3()

}

/**
写文件
*/
var words string = "urriedly  [ˈhə:ɪdlɪ]  adv. 仓促地，匆忙地 hid [hɪd]  隐瞒;隐藏"+"parcel  [ˈpɑ:rsl]  n. 包袱，包裹;（土地的）一块;一批"+ "obvious [ˈɑ:bviəs] 明显的;显著的;"+"embarrassed [ɪmˈbærəst] 尴尬的;为难的;窘迫的;"+ "guiltily  ['ɡɪltɪlɪ]  内疚地;罪地，有罪地"+ "explain  [ɪkˈsplen]讲解，解释,　说明"

func test3()  {
	fileName1 := "/home/daniel/tmp/aa/abc.text"

	//打开文件
	file, err := os.OpenFile(fileName1, os.O_WRONLY, os.ModePerm)
	if err != nil {
		fmt.Println("err:", err)
	}

	//写入数据---写一个字节
	str := "abcdef"
	bs := []byte(str)
	file.Write(bs)

	//写入字符串
	file.WriteString("hhhhhhh")

	//关闭文件
	file.Close()

	//重新打开写入数据----覆盖
	file2 ,_ := os.OpenFile(fileName1, os.O_WRONLY, os.ModePerm)

	file2.WriteString("000")
	file2.Close()

}



/*
读取一个图片文件，统计大小
 */
func test2()  {
	fileName := "/toMid/0F_Chain/01_BChain0514/01_go/pro/萌猫.jpg"

	//打开文件
	file, err := os.Open(fileName)
	if err != nil {
		fmt.Println("err：", err.Error())
	}

	//读取文件
	bs := make([]byte, 100, 100)
	count := 0
	for {
		n, err := file.Read(bs)

		if n == 0 || err == io.EOF {
			fmt.Println("文件到末尾了")
			break
		}
		//统计
		count += n

	}



	//关闭文件
	file.Close()
	fmt.Println("文件大小:", count)


}

/**
读取文件
 */
func test1()  {
	fileName1 := "/toMid/06_gitmayu2/src/goBasic/day12_io/demo02_time.go"

	//打开文件
	file,err := os.Open(fileName1)
	if err != nil {
		fmt.Println("err：", err.Error())
	}

	//读取数据
	bs := make([]byte, 1024, 1024)
	for  {
		n,err := file.Read(bs)
		if n == 0 || err == io.EOF {
			fmt.Println("文件到末尾了")
			break
		}
		fmt.Print(string(bs[:n]))
	}



	//关闭文件
	file.Close()




	//






}


