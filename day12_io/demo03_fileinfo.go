package main

import (
	"os"
	"fmt"
)
/*
/toMid/06_gitmayu2/src/goBasic/day12_io/demo02_time.go
 */
func main()  {
	fileInfo,err := os.Stat("/toMid/06_gitmayu2/src/goBasic/day12_io/demo02_time.go")

	if err != nil {
		fmt.Println("err:", err)
	}

	fmt.Printf("%T\n", fileInfo)

	fmt.Println(fileInfo.Name())	//文件名
	fmt.Println(fileInfo.IsDir())	//是否为目录
	fmt.Println(fileInfo.Size())	//大小
	fmt.Println(fileInfo.ModTime())	//修改时间
	fmt.Println(fileInfo.Mode())	//权限
	fmt.Println(fileInfo.Sys())		//




}
