package main

import "fmt"

func main()  {
	funA()
	funB()
	fmt.Println("main----over")

	funC()
}

func funA()  {
	fmt.Println("111111111111")
}

func funB()  {
	defer func() {
		if msg := recover(); msg != nil {
			fmt.Println(msg)
		}
	}()

	for i:=0; i<10; i++ {
		fmt.Println("i:", i)

		if i==5 {
			panic("funB函数恐慌了----")
		}
	}
	fmt.Println("2222222222222")
}


func funC()  {
	defer func() {
		fmt.Println("funC函数延迟函数----")
		//msg := recover()
		//if msg != nil {
		//	fmt.Println(msg)
		//}
		fmt.Println("3333333333333333")

	}()

	panic("funC函数恐慌--------")
}






























