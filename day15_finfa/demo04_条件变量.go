package main

import (
	"sync"
	"time"
	"fmt"
)

func main()  {
	var mutex sync.Mutex
	cond := sync.Cond{L:&mutex}
	condition := false


	go func() {
		time.Sleep(time.Second * 1)
		cond.L.Lock()


		//修改条件--并发通知
		condition = true
		cond.Signal()

		time.Sleep(time.Second *5)


		
		cond.L.Unlock()
	}()



	cond.L.Lock()

	if !condition {

		cond.Wait()

	}

	fmt.Println("main....继续")
	fmt.Println("main...解锁")

	cond.L.Unlock()

}

