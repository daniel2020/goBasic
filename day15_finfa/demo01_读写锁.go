package main

import (
	"sync"
	"fmt"
	"time"
)

/**
读写锁　sync.RWMutex

 */
func main()  {

	var rwm sync.RWMutex

	for i:=1; i<=3; i++ {
		go func(i int) {
			fmt.Printf("goroutine %s")
			rwm.RLock()

			time.Sleep(time.Second * 5)

			rwm.RUnlock()


		}(i)
	}

	rwm.Lock()


}




