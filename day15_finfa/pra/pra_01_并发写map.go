package main

import (
	"sync"
	"fmt"
	"time"
)

/**
并发往map写数据
 */
var wg sync.WaitGroup
var rwm sync.RWMutex

func main()  {
	map1 := make(map[int]int)
	wg.Add(3)
	for i:=1; i<=3; i++ {
		go write(map1, i)
	}
	wg.Wait()
}

func write(map1 map[int]int ,i int)  {

	//fmt.Println("G", i, "即将。。。写锁定")

	rwm.Lock()
	fmt.Println("G", i,"已经写锁定")
	time.Sleep(time.Second*3)
	fmt.Println("G", i,"即将往map写如数据")

	map1[1]= i*100
	fmt.Println("G", i,"已经往map写如数据－－－", i*100)

	fmt.Println("G", i,"即将。。。写锁 解除")
	rwm.Unlock()

	//fmt.Println("G", i,"已经写锁  已经解除")

	wg.Done()
}

func write1()  {
	
}

