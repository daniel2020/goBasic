package main

import "fmt"

func main()  {

	a:=10
	fmt.Println("调用函数前,num:", a)
	fun1(a)
	fmt.Println("调用函数前,num:", a)

	fun2(&a)
	fmt.Println("调用函数fun2后,num:", a)

	fmt.Println("=======================")

	arr1 := [4]int {1,2,3,4}
	fmt.Println("函数调用前：", arr1)
	fun3(arr1)
	fmt.Println("函数调用后：", arr1)

	fun4(&arr1)
	fmt.Println("函数fun4调用后：", arr1)

}

func fun6(s *[]int)  {

}
func fun5(s []int)  {

}

func fun4(p2 *[4]int)  {
	fmt.Println("函数中数组:", p2)
	p2[0] = 2000
	fmt.Println("函数中数组:", p2)
}

func fun1(num int)  {
	fmt.Println("函数中，num:", num)
	num = 100
	fmt.Println("函数中修改num:", num)
}

func fun2(p1 *int)  {	//传递的是 地址,相当于引用传递
	fmt.Println("函数fun2, p1:", *p1)
	*p1 = 200
	fmt.Println("函数fun2, p1:", *p1)
}

func fun3(arr2 [4]int)  {
	fmt.Println(arr2)
	arr2[0] = 100
	fmt.Println(arr2)
}
