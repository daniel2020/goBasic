package main

import "fmt"

func main()  {

	a := 10
	fmt.Println("a,", a*10)

	b := [4]int{1,2,3,4}
	b[0] = 100
	fmt.Println(b)

	fmt.Println("===========函数=============")
	fmt.Println(fun1)
	fmt.Printf("%T\n", fun1)
	fmt.Printf("%v\n", fun1)


	var c func(int,int)
	c = fun1

	fmt.Println(c)
	fmt.Printf("%T\n", c)

	c(1,2)

	fmt.Println("==============")
	res1 := fun2(1 , 2)
	fmt.Println(res1)

	res2 := fun2
	fmt.Println(res2)

	res3 := res2(10, 20)
	fmt.Println(res3)






}

func fun1(a,b int)  {
	fmt.Println("a:", a, ",b:", b)
}

func fun2(a,b int) int {
	return a+b
}