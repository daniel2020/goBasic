package main

import "fmt"

func main()  {

	arr1 := [4]int{1,2,3,4}
	fmt.Println(arr1)

	var p1 *[4]int
	p1 = &arr1
	fmt.Println(p1)
	fmt.Printf("%p\n",p1)

	(*p1)[0] = 100
	fmt.Println(arr1)

	fmt.Println("---------指针数组---------------")
	a := 1
	b := 2
	c := 3
	d := 4

	arr2 := [4]int{a,b,c,d}
	arr3 := [4]*int{&a, &b, &c, &d}

	fmt.Println(arr2)
	fmt.Println(arr3)

	arr2[0] = 100
	fmt.Println(arr2)
	fmt.Println(a)

	fmt.Println("==============")
	*arr3[1] = 1000
	fmt.Println(*arr3[0])
	fmt.Println(*arr3[1])
	fmt.Println(*arr3[2])
	fmt.Println(*arr3[3])
	fmt.Println(a,b,c,d)



}




