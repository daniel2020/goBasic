package main

import (
    "fmt"
)

func main() {
    var a [1]int
    c := a[:]
    fmt.Println(a)
    fmt.Println(c)
    fmt.Printf("%p\n",a)
    fmt.Printf("%p\n",c)
}

