package main

import "fmt"

func main()  {

	fun1()
	fun2 := fun1
	fun2()

	//声明匿名函数
	//func () {
	//	fmt.Println(".. 匿名fun...")
	//}

	//声明匿名调用
	func () {
		fmt.Println(".. 匿名fun...")
	}()


	//声明匿名带入参
	func (a,b int) {
		fmt.Println("......",a ,b)
	}(10, 20)


	//声明匿名带出参
	res1 := func(a,b,c int) int {
		return a+b+c
	}(1,2,3)

	fmt.Println("res1:", res1)

	//存放匿名函数的地址
	res2 := func() {
		fmt.Println("-------")
	}
	fmt.Println(res2)

	res3 := fun1
	fmt.Println(res3)
	res2()


}

func fun1()  {
	fmt.Println("...fun1..")
}