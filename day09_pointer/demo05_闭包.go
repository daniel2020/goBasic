package main

import "fmt"

func main()  {

	res1 := increment()
	fmt.Println(res1)
	fmt.Printf("%T\n", res1)
	res2 := res1()
	fmt.Println(res2)
	fmt.Println(res1())
	fmt.Println(res1())

	res5 := increment()
	fmt.Println(res5())
	fmt.Println(res5())
	fmt.Println(res5())
	fmt.Println(res5())


}

func increment() func() int {	//外层函数
	i := 0

	fun := func() int {			//内层函数
		i++
		return i
	}

	return fun
}
