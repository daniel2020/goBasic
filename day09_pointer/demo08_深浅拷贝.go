package main

import "fmt"

func main()  {

	map1 := make(map[int]string)
	map1[1] = "aaaa"
	map1[2] = "bbbb"
	fmt.Println(map1)
	fmt.Printf("%p\n", map1)

	//1.深拷贝
	a := 10
	b := a
	fmt.Println(a, b)
	b = 20
	fmt.Println(a, b)

	//2数组的拷贝
	arr1 := [4]int{1,2,3,4}
	arr2 := arr1
	fmt.Println(arr1, arr2)
	arr2[0] = 100
	fmt.Println(arr1, arr2)
	//数组的浅拷贝
	arr3 := &arr1
	fmt.Println(arr1, arr3)
	(*arr3)[1] = 200
	fmt.Println(arr1, arr3)
	arr3[1] = 250
	fmt.Println(arr1, arr3)




}

