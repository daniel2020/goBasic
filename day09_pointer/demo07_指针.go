package main

import "fmt"

func main()  {
	/**
	指针

	指针的类型
	指针中存储的数据
	指针自己的地址
	*/
	//1.定义一个
	a := 10
	fmt.Printf("a的数值:%d\n",a)
	fmt.Printf("a的类型：%T\n",a)
	fmt.Printf("a的地址：%p\n",&a)

	//声明一个指针变量, 用于ｃｈｕｃ
	var p1 *int
	fmt.Println(p1)

	p1 = &a
	fmt.Println("p1的数值：" ,p1)
	fmt.Println(*p1)
	fmt.Println("p1自己的地址:",&p1)
	fmt.Printf("p1的类型:%T\n", p1)

	//操作变量，更改数值
	a = 100
	fmt.Println("ａ的地址:", &a)
	*p1 = 200
	fmt.Println("a:", a)

	//5.指针的指针
	var p2 **int
	fmt.Println(p2)
	p2 = &p1
	fmt.Println(p2)

	fmt.Println(*p2)
	fmt.Println(**p2)

	fmt.Printf("%T,%T,%T\n", a, p1, p2)


	fmt.Println("============")
	fmt.Println("============")

	f1()
}

func f1()  {
	//创建变量，创建指针，操作指针更改变量，指针的指针。。
	//创建指针
	var a = "hello"
	var p1 *string = &a

	//指针
	fmt.Println("a的数值:",a)
	fmt.Println("a的地址:",&a)
	fmt.Println("p1的数值:", p1)
	*p1 = "world"
	fmt.Println("a的数值:",a)
	fmt.Println("a的地址:",&a)
	fmt.Println("p1的数值:", p1)

	b := "aaaaa"
	fmt.Println("b的数值:",b)
	fmt.Println("b的地址:",&b)
	b = "cccccc"
	fmt.Println("b的数值:",b)
	fmt.Println("b的地址:",&b)

	fmt.Println("===================")
	s1 := []int {1,2,3,4}




	//

}
