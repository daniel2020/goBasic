package main

import "fmt"

func main()  {

	//fun1("hello")
	//fmt.Println("123456789")
	//defer fun1("1111111111")
	//fmt.Println("2222222222")
	//defer fun1("33333333333")
	//defer fun1("4444444444")

	//a := 2
	//defer fun2(a)
	//a++
	//fmt.Println("main函数中，num:", a)
	

	//res1 := f1()
	//fmt.Println(res1)

	//res2 := f2()
	//fmt.Println(res2)

	res3 := f3()
	fmt.Println(res3)

}


func f1() (result int) {
	fmt.Println("11111")
	fmt.Println("222222")
	defer func() {
		result++
		fmt.Println("33333333")
	}()
	fmt.Println("444444444")
	return 0
}

func f2() (r int) {
	t := 5
	defer func() {
		t = t + 5
	}()
	return t
}

func f3() (r int) {
	defer func(r int) {
		fmt.Println("111111  r:", r)
		r = r + 5
		fmt.Println("2222222  r:", r)
	}(r)
	return 1
}



func fun1(s string)  {
	fmt.Println(s)
}

func fun2(num int)  {
	fmt.Println("fun2中，num:", num)
}

func fun3(s string)  {
	fmt.Print()
}
