package main

import "fmt"

func main() {
	/*

	 */

	var a func()
	a = fun1
	fmt.Println(a)
	a()

	arr1 := fun2()
	arr2 := fun2()
	fmt.Printf("arr1类型:%T,地址:%p,数值:%v\n", arr1, &arr1, arr1)
	fmt.Printf("arr1类型:%T,地址:%p,数值:%v\n", arr2, &arr2, arr2)
	//arr1 := fun2()
	fmt.Printf("arr1类型:%T,地址:%p,数值:%v\n", arr1, &arr1, arr1)

}

func fun2() [4]int {
	arr := [4]int{1, 2, 3, 4}
	return arr
}

func fun3() *[4]int {
	arr := [4]int{1, 2, 3, 4}
	return &arr
}

func fun1() {
	fmt.Println("fun1....")

}
