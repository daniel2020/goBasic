package main

import "fmt"

func main()  {

	fmt.Printf("%T\n", add)
	fmt.Printf("%T\n", oper)

	res1 := add(1,2)
	fmt.Println(res1)

	res2 := oper(10, 20, add)
	fmt.Println(res2)

	res3 := oper(7,4,sub)
	fmt.Println(res3)

	fun1 := func(a,b int) int {
		fmt.Println("a:", a, "b:", b)
		return a * b
	}

	res4 := oper(10, 4, fun1)
	fmt.Println(res4)


	res5 := oper(100, 8, func(a,b int)int{
		if b == 0 {
			fmt.Println("除数不能为0")
			return 0
		}
		return a/b
	})
	fmt.Println(res5)





}

func add(a,b int) int {
	return a+b
}

func sub(a,b int) int {
	return a-b
}

func oper(a,b int, fun func(int,int)int)  int {
	fmt.Println(a,b,fun)

	res := fun(a,b)
	return res
}

