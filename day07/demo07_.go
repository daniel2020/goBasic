package main

import (
	"strings"
	"fmt"
	"flag"
)

/**
练习1：熟练strings包中的常用方法

练习2：给定一个路径名：
pathName:="http://192.168.10.1:8080/Day33_Servlet/aa.jpeg"
获取文件名称：aa.jpeg

练习3：给定一个以下字符串：统计大写字母的个数，小写字母的个数，非字母的个数。
str:="aekjffjkJDJ294384848DKFJFJkdjfhfh2943845593nfnJRIEIFJ"
 */
func main()  {
	f2()
	f3()
}

func f1()  {
	fmt.Println("...")
}

func f2()  {
/*
练习2：给定一个路径名：
pathName:="http://192.168.10.1:8080/Day33_Servlet/aa.jpeg"
获取文件名称：aa.jpeg
 */
	pathName:="http://192.168.10.1:8080/Day33_Servlet/aa.jpeg"
	index := strings.LastIndex(pathName, "/")
 	fileName := pathName[index+1:]
 	fmt.Println(fileName)

 	ss := strings.Split(pathName, "/")
	fmt.Println(ss[len(ss)-1])
}

func f3()  {
/*
练习3：给定一个以下字符串：统计大写字母的个数，小写字母的个数，非字母的个数。
str:="aekjffjkJDJ294384848DKFJFJkdjfhfh2943845593nfnJRIEIFJ"
 */

	str:="aekjffjkJDJ294384848DKFJFJkdjfhfh2943845593nfnJRIEIFJ"

	chars := strings.Split(str, "")

	lowerCount := 0
	upperCount := 0
	diget := 0

	for _, char := range chars {
		if char >= 'a' && char <= 'z' {
			lowerCount ++

		} else if char >= 'A' && char <= 'Z' {
			upperCount ++

		} else if char >= '0' && char <= '9' {
			diget ++
		}
	}

	fmt.Println(chars)


}