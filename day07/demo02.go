package main

import "fmt"

/*
1.创建一个map用于存储一个人的信息：
 "name"：王二狗
 "age"：30
 sex：男性
 address："北京市xx路xx号"

2.打印遍历该map
3.修改sex为女性
4.如果想map中添加重复的key，会如何？
 */
func main()  {
	var map1 = map[string]string {}

	map1["name"] = "王二狗"
	map1["age"] = "30"
	map1["sex"] = "男"
	map1["address"] = "北京市北清路"

	for k,v := range map1 {
		fmt.Println(k, v)
	}

	//修改
	map1["sex"] = "女性"

	fmt.Println(map1)


	map2 := make(map[string]string)
	map2["name"] = "张三"
	map2["age"] = "28"
	map2["sex"] = "男"
	map2["address"] = "珠海市新香洲区唐家湾镇"

	s1 := []map[string]string{}

	s1 = append(s1, map1, map2)

	fmt.Println(s1)

	for i,val := range s1  {
		fmt.Println(i, "name", val["name"])
		fmt.Println(i, "age", val["age"])
		fmt.Println(i, "sex", val["sex"])
		fmt.Println(i, "address", val["address"])
	}
}
