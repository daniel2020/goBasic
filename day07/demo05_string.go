package main

import (
	"fmt"
)

func main()  {


	//定义字符串
	s1 := "hello中"
	s2 := `helloworld`

	fmt.Println(s1)
	fmt.Println(s2)

	//长度
	fmt.Println(len(s1))
	fmt.Println(len(s2))

	//获取某个字符
	fmt.Println(s1[0])
	fmt.Printf("%c\n", s1[0])

	//获取字符串中所有字符  遍历   //不能用中文
	for i:=0; i<len(s2); i++ {
		fmt.Printf("%c\t", s2[i])
	}


	fmt.Println("=========")
	for _,v := range s2 {
		fmt.Printf("%c\t", v)
	}
	fmt.Println()

	//字符串是字节的集合
	slice1 := []byte{65,66,67,68,69}
	s3 := string(slice1)

	fmt.Println(s3)

	s4 := "abcde"
	slice2 := []byte(s4)
	fmt.Println(slice2)


	//
	//ss := "aekjffjkJDJ294384848DKFJFJkdjfhfh2943845593nfnJRIEIFJ"
	//fmt.Println(strings.ToUpper(ss))
	//fmt.Println(strings.ToLower(ss))





}

