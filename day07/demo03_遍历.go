package main

import (
	"fmt"
	"sort"
)

func main()  {

	map1 := make(map[int]string)

	map1[1] = "红孩儿"
	map1[2] = "小钻风"
	map1[3] = "白骨精"
	map1[4] = "白素贞"
	map1[5] = "金叫大王"

	//遍历map
	for k,v := range map1 {
		fmt.Println(k, v)
	}

	fmt.Println("============")
	//有序遍历map
	for i:=1; i<= len(map1); i++ {
		fmt.Println(i, map1[i])
	}

	//把所有key取出，排序， 再按顺序取出value
	keys := []int{}
	fmt.Println(keys)
	for k := range map1{
		keys = append(keys, k)
	}
	fmt.Println(keys)

	sort.Ints(keys)
	fmt.Println(keys)

	for _,key := range keys{
		fmt.Println(key, map1[key])
	}

	//对string 切片排序
	fmt.Println("=================")
	s1 := []string{"apple", "window", "orange", "abc", "abd", "aaaa"}
	fmt.Println(s1)
	sort.Strings(s1)
	fmt.Println(s1)



}

