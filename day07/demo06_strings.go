package main

import (
	"fmt"
	"strings"
)

/**
strings包
 */
func main()  {
	s1 := "helloworld"
	fmt.Println(strings.Contains(s1, "hell"))
	fmt.Println(strings.Contains(s1, "hellAA"))
	fmt.Println(strings.Contains(s1, "hel"))

	fmt.Println("------------")
	fmt.Println(strings.ContainsAny(s1, "ab"))
	fmt.Println(strings.ContainsAny(s1, "abcde"))

	fmt.Println("-------")
	fmt.Println(strings.Count(s1, "ll"))
	fmt.Println(strings.Count("helloworldll", "ll"))

	//前缀、后缀
	fmt.Println("-------------")
	s2 := "20180522课堂笔记.text"
	if strings.HasPrefix(s2, "2018") {
		fmt.Println("是2018年的文件")
	}
	if strings.HasPrefix(s2, "2015") {
		fmt.Println("不是2015年的文件")
	}
	if strings.HasSuffix(s2, ".text") {
		fmt.Println("是text文件")
	}


	//索引
	fmt.Println(strings.Index(s1, "l"))
	fmt.Println(strings.Index())









}
