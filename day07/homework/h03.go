package main

import (
	"strings"
	"fmt"
)

/*
4.将字符串"aa:zhangsan@163.com!bb:lisi@sina.com!cc:wangwu@126.com"，
按照指定的key-value规则存入map中
	key：aa,bb,cc
	value:zhangsan@163.com,lisi@sina.com,wangwu@126.com

 */
func main()  {

	//方式１
	f1()

	//

}

func f1()  {
	str := "aa:zhangsan@163.com!bb:lisi@sina.com!cc:wangwu@126.com";

	s1 := strings.Split(str, "!")

	fmt.Println(s1)

	user := make(map[string]string)
	for _,val := range s1 {
		s2 := strings.Split(val, ":")
		user[s2[0]] = s2[1]
	}

	fmt.Println(user)

	for k,v := range user {
		fmt.Println(k, "---", v)
	}
}