package main

import (
	"fmt"
	"strings"
)

func main() {
	//2.str1 = "hello hello hello world"
	//统计字符串的长度:
	//统计"llo"出现的次数:
	//统计"wor"出现的位置:

	str := "hello hello hello world"
	fmt.Println("字符串长度:", len(str))

	str1 := str

	count1 := 0
	index := 0
	for {
		index = strings.LastIndex(str1, "llo")
		if index == -1 {
			break
		}

		count1 ++
		str1 = str1[:index-1]
	}
	fmt.Println("llo出现长度:", count1)

	post := strings.Index(str, "wor")
	fmt.Println("wor出现的位置:", post)

}

