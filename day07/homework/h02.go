package main

import "fmt"

/**
3.统计该字符串中每个字母出现的次数

"ECALIYHWEQAEFSZCVZTWEYXCPIURVCSWTDBCIOYXGTEGDTUMJHUMBJKHFGUKNKN"
*/
func main()  {
	str := "ECALIYHWEQAEFSZCVZTWEYXCPIURVCSWTDBCIOYXGTEGDTUMJHUMBJKHFGUKNKN"
	bytes := []byte(str)

	fmt.Println(bytes)
	fmt.Println("字符个数:", len(bytes))

	mapLetter := make(map[byte]int)

	//每个字符放入map, key:字母   value:出现次数
	for _,val := range bytes {
		value,ok := mapLetter[val]
		if ok {
			mapLetter[val] = value + 1
		} else {
			mapLetter[val] = 1
		}

	}

	sum := 0
	for letter, count := range mapLetter {
		fmt.Printf("%c:%d\n", letter, count)
		sum += count
	}
	fmt.Println("sum:", sum)

}

//转成数组   index: 字符编码　　　元素值：次数
func f1()  {

}

