package main

import "fmt"

func main()  {

	//创建map
	var map1 map[int]string
	var map2 = map[string]int{"go":98, "java":100, "python":11}
	var map3 = make(map[int]string)

	fmt.Println(map1)
	fmt.Println(map2)
	fmt.Println(map3)

	//nil
	fmt.Println(map1 == nil)
	fmt.Println(map2 == nil)
	fmt.Println(map3 == nil)

	if map1 == nil {
		map1 = make(map[int]string)
		fmt.Println(map1 == nil)
	}

	//存储 数据
	map1[1] = "hello"
	map1[2] = "world"
	map1[3] = "memeda"
	map1[4] = "三胖思密达"


	//取值
	fmt.Println(map1)
	fmt.Println(map1[1])
	fmt.Println(map1[30])

	value, ok := map1[30]
	if ok {
		fmt.Println(value)
	} else {
		fmt.Println("key不存在")
	}



	//遍历
	for k, v := range map1 {
		fmt.Println(k, v)
	}


	//修改


	//删除   内置函数delete
	fmt.Println(map1)
	delete(map1, 1)
	fmt.Println(map1)
	delete(map1, 11)
	fmt.Println(map1)

	//map长度
	fmt.Println("map1长度:", len(map1))





}

