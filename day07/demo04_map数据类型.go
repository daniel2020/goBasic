package main

import (
	"fmt"
	"sort"
)

func main()  {

	//map的类型
	map1 := make(map[int]string)
	map2 := make(map[string]int)

	fmt.Printf("%T\n", map1)
	fmt.Printf("%T\n", map2)

	map3 := make(map[string]map[string]string)
	m1 := make(map[string]string)
	m1["name"] = "zhangsan"
	m1["age"] = "18"
	m1["salary"] = "3000"

	map3["hr"] = m1

	m2 := make(map[string]string)
	m2["name"] = "lisi"
	m2["age"] = "22"
	m2["salary"] = "8100"

	map3["总经理"] = m2
	fmt.Println(map3)

	fmt.Println("========================")

	//引用类型
	map4 := make(map[string]string)
	map4["王二狗"] = "矮矬穷"
	map4["rose"] = "白富美"
	map4["田田田"] = "住在隔壁"
	fmt.Println(map4)

	map5 := map4
	fmt.Println(map5)

	map5["王二狗"] = "高富帅"
	fmt.Println(map4, map5)

	fmt.Println("========================")

	f1()

}

func f1()  {
	/*
5.创建一个map，存储以下数据，并按照key的大小顺序打印输出。
 1:"HR"
 2:"程序员"
 3:"程序员鼓励师"
 4:"行政专员"
 5:"前台妹子"
	 */

	map1 := make(map[int]string)
	map1[1] = "HR"
	map1[2] = "程序员"
	map1[3] = "程序员鼓励师"
	map1[4] = "行政专员"
	map1[5] = "前台妹子"

	keys := make([]int, 0, len(map1))


	for k := range map1 {
		keys = append(keys, k)
	}

	sort.Ints(keys)

	for _,k := range keys  {
		fmt.Println(k, map1[k])

	}



}
