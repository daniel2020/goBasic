package main

import (
	"net/http"
	"goBasic/ketanx/help"
	"fmt"
	"goBasic/ketanx/0605http/util"
)

var urlurl string = "https://www.toutiao.com/search/suggest/initial_page/"
func main()  {
	client := http.Client{}

	//创建请求
	request,err := http.NewRequest("GET", urlurl, nil)
	help.CheckError(err)

	//添加cookie
	cookie1 := &http.Cookie{Name:"username", Value:"kongyixueyuan"}
	request.AddCookie(cookie1)

	//设置请求头
	request.Header.Set("Accept-Language", "zh-cn")

	//发送请求
	response, err := client.Do(request)
	help.CheckError(err)

	//查看请求数据
	fmt.Printf("Head:%+v\n", request.Header)


	util.ShowResponse(response)

}
