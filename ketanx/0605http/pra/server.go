package main

import "net/http"

func main()  {
	//test1()
	//test2()
	test3()

}


func test1()  {		//用Handler接口的实现对象
	aa:= AA{}
	http.ListenAndServe(":3000", &aa)		//需要Handler接口的实现对象

}

type AA struct {

}
func (aa *AA) ServeHTTP(responseWriter http.ResponseWriter, request *http.Request) {
	responseWriter.Write([]byte("1111111122222"))
}

func test2()  {		//用Handler接口的实现对象
	aa := AA{}
	http.Handle("/", &aa)
	http.ListenAndServe(":3000", nil)

}

func test3()  {		//匹配任意路径和任意函数

	http.HandleFunc("/a1", ServeHTTP001)
	http.HandleFunc("/a2", ServeHTTP002)
	http.ListenAndServe(":3000", nil)

}

func ServeHTTP001(responseWriter http.ResponseWriter, request *http.Request)  {
	responseWriter.Write([]byte("------11111111111"))
}
func ServeHTTP002(responseWriter http.ResponseWriter, request *http.Request)  {
	responseWriter.Write([]byte("------222222222222"))
}
func ServeHTTP003(responseWriter http.ResponseWriter, request *http.Request)  {
	//request.FormValue()


	responseWriter.Write([]byte("------3333333333333"))
}

func test4()  {
	mux := http.NewServeMux()

	mux.HandleFunc("/a1", ServeHTTP001)
	mux.HandleFunc("/a2", ServeHTTP002)

	http.ListenAndServe(":3000", mux)
}



