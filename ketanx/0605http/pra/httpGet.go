package main

import (
	"net/http"
	"goBasic/ketanx/help"
	"goBasic/ketanx/0605http/util"
	"bytes"
	"net/url"
)

var url01 string = "http://www.baidu.com"

func main()  {

	//test1()
	//test2()
	//test3()
	//test4()
	//test5()
	test6()

}

//get  1
func test1() {
	response, err := http.Get("http://www.baidu.com")

	help.CheckError(err)

	util.ShowResponse(response)
}

//get 2	----构建 request
func test2()  {

	client := http.Client{}

	request, err := http.NewRequest("GET", url01, nil)
	help.CheckError(err)

	response,err := client.Do(request)
	help.CheckError(err)

	util.ShowResponse(response)

}

//get 3 -------用客户端对象 发请求
func test3()  {
	//创建客户端对象
	client := http.Client{}

	//客户端发请求
	response, err := client.Get(url01)
	help.CheckError(err)

	util.ShowResponse(response)

}


//1.post
func test4()  {

	param := url.Values{
		"theCityName":{"北京"},
	}

	buffer := bytes.NewBufferString(param.Encode())

	response,err := http.Post(url01,"application/x-www-form-urlencoded", buffer)
	help.CheckError(err)

	util.ShowResponse(response)

}


//2.post 构建 request	请求头
func test5()  {

	client := http.Client{}

	param := url.Values{
		"name":{"zhangsan"},
	}
	buffer := bytes.NewBufferString(param.Encode())

	request, err := http.NewRequest("POST", url01, buffer)
	help.CheckError(err)

	response,err := client.Do(request)
	help.CheckError(err)

	util.ShowResponse(response)

}

// clent 发post
func test6()  {

	client := http.Client{}

	param := url.Values{
		"name":{"zhangsan"},
	}

	//param.Set("time","1")
	//param.Set("url","http://www.baidu.com")
	//param.Set("ua","windows20")
	//str:=param.Encode()
	//str="http://www.baidu.com/index.php?"+str
	//fmt.Println(str)


	buffer := bytes.NewBufferString(param.Encode())

	response, err := client.Post(url01, "application/x-www-form-urlencoded", buffer)
	help.CheckError(err)

	util.ShowResponse(response)



}

// http.Get()	client.Do()		client.Get()
// http.Post()	client.Do()		client.Post()
//


















