package main

import (
	"net/http"
	"fmt"
)

func main()  {
	mux := http.NewServeMux()

	mux.HandleFunc("/hi", hi)
	mux.HandleFunc("/hello", hello)
	http.ListenAndServe(":2002", mux)



}

func hi(responseWriter http.ResponseWriter, request *http.Request) {
	request.ParseForm()

	fmt.Println(request.Header)
	fmt.Println(request.Method)
	fmt.Println(request.Cookie)
	fmt.Println(request.URL)

	fmt.Println(request.FormValue("theCityName"))


	responseWriter.Write([]byte("hi-------"))
}


func hello(responseWriter http.ResponseWriter, request *http.Request) {
	responseWriter.Write([]byte("hello-----"))
}

