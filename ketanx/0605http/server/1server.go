package main

import (
	"net/http"
	"net/url"
)

func main()  {
	test1()

}


func test1()  {
	http.HandleFunc("/", ServeHTTP)			//绑定路径
	http.HandleFunc("/hello", ServeHTTP1)
	http.HandleFunc("/bye", ServeHTTP2)
	http.HandleFunc("/a3", ServeHTTP3)
	http.ListenAndServe(":3000", nil)	//监听服务

}


func ServeHTTP(responseWriter http.ResponseWriter, request *http.Request) {
	param := url.Values{
		"name": {"zhangsna"},
		"age":  {"23"},
	}

	responseWriter.Write([]byte(param.Encode()))
}

func ServeHTTP1(responseWriter http.ResponseWriter, request *http.Request) {
	responseWriter.Write([]byte("hello"))
}
func ServeHTTP2(responseWriter http.ResponseWriter, request *http.Request) {
	responseWriter.Write([]byte("bye bye"))
}
func ServeHTTP3(responseWriter http.ResponseWriter, request *http.Request) {
	responseWriter.Write([]byte("3333333333"))
}

