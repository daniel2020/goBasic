package util

import (
	"io/ioutil"
	"goBasic/ketanx/help"
	"fmt"
	"net/http"
)

func ShowResponse(response *http.Response)  {
	defer response.Body.Close()

	if response.StatusCode == 200 {
		bs, err := ioutil.ReadAll(response.Body)
		help.CheckError(err)

		fmt.Println(string(bs))

	} else {
		fmt.Println("请求失败,StatusCode:", response.StatusCode)
	}


}
