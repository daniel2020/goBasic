package main

import (
	"net/http"
	"goBasic/ketanx/help"
	"fmt"
	"io/ioutil"
)

var url01 string = "https://www.toutiao.com/search/suggest/initial_page/"
var url02 string = "http://www.baidu.com"

func main()  {
	//发get请求
	response,err := http.Get(url02)
	help.CheckError(err)



	//看响应数据
	fmt.Printf("%+v\n", response)
	fmt.Println(response)

	if response.StatusCode == 200 {

		fmt.Println("请求成功，statusCode", response.StatusCode)
		//操作响应数据
		defer response.Body.Close()

		//读取body里的数据
		fmt.Println("======看body============", response.Body)

		bs, err := ioutil.ReadAll(response.Body)
		help.PrintError(err)

		fmt.Println(string(bs))



	} else {
		fmt.Println("请求失败，statusCode", response.StatusCode)
	}




}
