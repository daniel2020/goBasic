package main

import (
	"encoding/json"
	"fmt"
	"goBasic/ketanx/help"
)

func main()  {
	//test1()
	test2()

}

func test1()  {
	user := make(map[string]string)

	user["username"] = "zhangsan"
	user["age"] = "25"
	user["address"] = "北京"

	//编码----系列化  map ---> json字符串
	jsonbs, err := json.Marshal(user)
	help.CheckError(err)

	fmt.Println(string(jsonbs))
}

func test2()  {
	map1 := map[string]string{"name":"zhangsan", "address":"北京"}

	jsonbs, err := json.Marshal(map1)
	help.CheckError(err)

	fmt.Println(string(jsonbs))


}