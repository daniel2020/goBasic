package main

import (
	"encoding/json"
	"goBasic/ketanx/help"
	"fmt"
)

func main()  {
	test1()
	//test2()

}

func test1()  {
	arr := []interface{}{"北京", 24, "淘宝店"}

	jsonbs,err := json.Marshal(arr)
	help.CheckError(err)

	fmt.Println(string(jsonbs))


	fmt.Println("=======================")

	arr2 := []map[string]string{{"name":"小明","sex":"男"}, {"name":"小红","sex":"女"}}

	jsonbs2, err := json.Marshal(arr2)
	help.CheckError(err)

	fmt.Println(string(jsonbs2))


}

func test2()  {
	//数组的元素是map
	map1 := []map[string]string{
		{"name":"zhangsan", "age":"25"},
		{"name":"lisi", "age":"22"},
	}

	mapbs, err := json.Marshal(map1)
	help.CheckError(err)

	fmt.Println(string(mapbs))
}