package main

import (
	"encoding/json"
	"fmt"
)

func main()  {

	//test1()

	//test2()
	test3()
}


//json ---》 未知结构
func test1()  {
	data := `{"name":"1802班", "Count":54, "student":["周星", "刘德"], "address":{"floor":[1, 2], "classroom":"二教室"}}`

	var object interface{}
	json.Unmarshal([]byte(data), &object)

	fmt.Println(object)
	fmt.Printf("%T\n", object)

	mapObject,ok := object.(map[string]interface{})

	if ok {

		for key, value := range mapObject {
			switch value.(type) {
			case string:
				fmt.Printf("类型string:%v:值:%v\n", key, value)
			case float64:
				fmt.Printf("类型float64:%v:值:%v\n", key, value)
			case bool:
				fmt.Printf("类型bool:%v:值:%v\n", key, value)
			default:
				fmt.Printf("类型  %v:值:%v\n", key, value)
			}
		}
	}
}

func test2()  {			//interface的 类型转换
	var obj interface{}
	obj = 12

	//s := string("12")
	//cannot convert obj (type interface {}) to type string: need type assertion

	s := obj.(int)
	fmt.Println(s)
	fmt.Printf("%T\n", s)
	fmt.Printf("%T\n", obj)




}

func test3()  {
	var obj interface{}
	obj = 123

	//s := string("12")
	//cannot convert obj (type interface {}) to type string: need type assertion

	s := obj.(string)
	fmt.Println(s)
	fmt.Printf("%T\n", s)
	fmt.Printf("%T\n", obj)


}


func test4() {
	var obj interface{}
	obj = 123

	s := obj.(string)
	fmt.Println(s)
	fmt.Printf("%T\n", s)
	fmt.Printf("%T\n", obj)

}