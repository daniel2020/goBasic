package main

import (
	"goBasic/ketanx/0606rpc/bean"
	"encoding/json"
	"fmt"
	"goBasic/ketanx/help"
)

func main()  {
	//test1()
	test2()

}

func test1()  {
	p1 := bean.Person{1, "zhangsan", "123"}
	p2 := bean.Person{2, "lisi", "123"}
	p3 := bean.Person{3, "wangwu", "123"}


	personArr := []bean.Person{p1, p2, p3}

	personBs, err := json.Marshal(personArr)
	help.CheckError(err)

	fmt.Println(string(personBs))
}

func test2()  {		//继承字段
	cat := Cat{"花花", 4, "黄色", Address{"海南", "三亚", 20}}

	jsonbs, err:= json.Marshal(cat)
	help.CheckError(err)

	fmt.Println(string(jsonbs))


	//格式化 json
	jsonbs2,err := json.MarshalIndent(cat, "+", "-")
	help.CheckError(err)

	fmt.Println(string(jsonbs2))


}

type Cat struct {
	Name string
	Age int
	Color string `json:"aaa"`	//转json时，　用别名
	Address Address
}

type Address struct {
	Province string
	City string
	Number int
}



