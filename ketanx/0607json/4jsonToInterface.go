package main

import (
	"encoding/json"
	"goBasic/ketanx/help"
	"fmt"
)

func main()  {
	//test1()

	//test2()
	//test3()
	//test4()
	test5()

}

type Dog struct {
	Name string
	Age int
	Sex string
}

//json  ---> map
func test1()  {
	json1 := `{"name":"zhangsan","sex":"男"}`

	map1 := make(map[string]string)

	err := json.Unmarshal([]byte(json1), &map1)
	help.CheckError(err)

	fmt.Println(map1)
}

//json ---> struct
func test2()  {
	json1 := `{"name":"zhangsan","age":23,"sex":"男"}`
	dog := new(Dog)

	err := json.Unmarshal([]byte(json1), &dog)
	help.CheckError(err)

	fmt.Println(dog)
}

//json ---> []int
func test3()  {
	//json1 := "{1,2,3,4}"
	json2 := "[1,2,3,4]"
	arr := make([]int,0,4)

	err := json.Unmarshal([]byte(json2), &arr)
	help.CheckError(err)

	fmt.Println(arr)

}

func test4()  {		// []int ----> json
	arr1 := []int{1,2,3,4}

	jsonbs,err := json.Marshal(arr1)
	help.CheckError(err)

	fmt.Println(string(jsonbs))

}

func test5()  {		// json  ----> []struct
	jsonArr := `[{"Name":"zhangsan", "Age":22, "Sex":"男"}, {"Name":"lisi", "Age":25, "Sex":"女"}]`
	arr := make([]Dog, 0, 2)

	err := json.Unmarshal([]byte(jsonArr), &arr)
	help.CheckError(err)

	fmt.Println(arr)

}


