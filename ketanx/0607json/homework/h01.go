package main

import (
	"encoding/json"
	"goBasic/ketanx/help"
	"fmt"
)

func main() {
	test1()
}

/*
{
"employees": [
{ "firstName":"Bill" , "lastName":"Gates" },
{ "firstName":"George" , "lastName":"Bush" },
{ "firstName":"Thoma" , "lastName":"Carter" }
]
}
 */
func test1() {
	jsonstr :=
`{"employees": [
{ "firstName":"Bill" , "lastName":"Gates" },
{ "firstName":"George" , "lastName":"Bush" },
{ "firstName":"Thoma" , "lastName":"Carter" }]}`

	arrEmps := make([]Employee, 0, 3)

	err :=json.Unmarshal([]byte(jsonstr), &arrEmps)
	help.CheckError(err)

	fmt.Println(arrEmps)
}

type Employee struct {
	FirstName string `json:"firstName"`
	LastName  string `json:"lastName"`
}
