package main

import (
	"goBasic/ketanx/help"
	"fmt"
	"database/sql"
)

func main()  {
	test1()
}

func test1()  {
	//获取数据连接
	db,err := help.GetDB()
	help.CheckError(err)

	//关闭连接
	defer db.Close()

	//查询
	rows,err := db.Query("select * from person")
	help.CheckError(err)

	//组装成bean
	personList := make([]*Person, 0, 0)

	personList = rowsToPersons(rows, personList)

	fmt.Println("len:", len(personList))

	for _,bean := range personList {
		fmt.Println(bean.Id, bean.Name)
	}
}


//把 记录转
func rowsToPersons(rows *sql.Rows, personList []*Person) []*Person {
	for rows.Next() {

		id, name := 0, ""
		err := rows.Scan(&id, &name)
		help.CheckError(err)

		p := Person{id, name}
		personList = append(personList, &p)
	}
	fmt.Println("11111111")
	return personList
}



//把 记录转
func rowsToBeans(rows *sql.Rows, personList []interface{}) []interface{} {
	for rows.Next() {

		id, name := 0, ""
		err := rows.Scan(&id, &name)
		help.CheckError(err)

		p := Person{id, name}
		personList = append(personList, &p)
	}
	fmt.Println("11111111")
	return personList
}


type Person struct {
	Id int
	Name string
}

