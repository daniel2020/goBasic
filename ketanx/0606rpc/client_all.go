package main

import (
	"net/rpc"
	"goBasic/ketanx/help"
	"goBasic/ketanx/0606rpc/bean"
	"fmt"
	"net/rpc/jsonrpc"
)

func main()  {
	//test1()
	test2()

}

func test1()  {
	//1.
	client,err :=rpc.DialHTTP("tcp", "127.0.0.1:3000")
	help.CheckError(err)

	//2.
	result := 0
	err = client.Call("MyMath.Add", bean.Param{1, 5}, &result)
	help.CheckError(err)

	fmt.Println("result:", result)
}


func test2()  {
	//连接到　　rpc服务
	client,err := jsonrpc.Dial("tcp", "127.0.0.1:3000")
	help.CheckError(err)

	result := 0
	err = client.Call("MyMath.Add", bean.Param{2,3}, &result)
	help.CheckError(err)

	fmt.Println("result:", result)

}




