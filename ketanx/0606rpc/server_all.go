package main

import (
	"net/rpc"
	"goBasic/ketanx/0606rpc/bean"
	"goBasic/ketanx/help"
	"net/http"
	"fmt"
	"net"
	"net/rpc/jsonrpc"
)

func main()  {
	//test1()
	test2()

}


func test1()  {		// 3步			//不会主动关闭连接
	//1.注册服务
	err := rpc.Register(new(bean.MyMath))
	help.CheckError(err)

	//2.绑定http
	rpc.HandleHTTP()

	//3.监听端口
	err = http.ListenAndServe(":3000", nil)
	help.CheckError(err)

	fmt.Println("1111111")

}

func test2()  {							// json 的rpc调用，可以跨平台
	//1.注册rpc服务
	err := rpc.Register(new(bean.MyMath))
	help.CheckError(err)

	//2.将rpc服务绑定到http
	rpc.HandleHTTP()

	//3.监听端口
	listener,err := net.Listen("tcp", ":3000")
	help.CheckError(err)

	//4.等待连接
	conn, err := listener.Accept()			//阻塞
	help.CheckError(err)

	//5.把连接传到rpc服务里
	jsonrpc.ServeConn(conn)					//使用一次后会自动关闭连接


	fmt.Println("1111111111----结束")
}

func test3()  {

}
