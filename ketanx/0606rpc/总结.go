package main

import "net"

func main()  {
	//socket
	//a. tcp:	需要确认连接	优点：数据可靠，能
	//b. udp:	不需要连接， 优点：时间快，效率高

	//服务端
	//1绑定监听端口
	listen,_ := net.ListenTCP("tcp", ":端口号")

	//2等待客户端连接
	conn,_ := listen.Accept()

	//3和客户端数据交互 read()  write()
	conn.Read()
	conn.Write()

	//=============================================

	//客户端
	//1连接客户端
	tcpAddr_ := net.ResolveTCPAddr("tcp", "ip:port")
	tcpConn,_ := net.DialTCP("tcp", nil, tcpAddr_)

	//2和服务端交互数据
	tcpConn.Write()
	tcpConn.Read()


	//二、http
	//短连接		一次数据交互完成之后就会断开连接
	//tcp

	//http和socket的区别
	//都用于客户端与服务端之间的通信，

	//2.http是短连接，socket是长连接
	//3.http是


	//三、rpc
	//远程过程调用， 主要用于服务器与服务器之间的调用，






}

