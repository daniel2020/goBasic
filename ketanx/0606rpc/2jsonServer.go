package main

import (
	"net/rpc/jsonrpc"
	"net/rpc"
	"goBasic/ketanx/0606rpc/bean"
	"goBasic/ketanx/help"
	"net"
)

func main()  {
	//注册
	err := rpc.Register(new(bean.MyMath))
	help.CheckError(err)

	rpc.HandleHTTP()

	//监听端口
	listen,err := net.Listen("tcp", ":3000")
	help.CheckError(err)

	for {
		//等待连接
		conn, err := listen.Accept()		//等待
		help.CheckError(err)

		go func(conn2 net.Conn) {			//支持异步 //以免阻塞别的客户端连接
			jsonrpc.ServeConn(conn2)
		}(conn)

	}


}



