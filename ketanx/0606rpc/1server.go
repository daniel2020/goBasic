package main

import (
	"net/rpc"
	"goBasic/ketanx/help"
	"net/http"
	"fmt"
	"goBasic/ketanx/0606rpc/bean"
)







func main()  {
	//1.注册rpc服务
	err := rpc.Register(new(bean.MyMath))
	help.CheckError(err)

	//2.将rpc的服务绑定到http
	rpc.HandleHTTP()

	fmt.Println("---服务端 监听中--")
	//3.监听端口
	http.ListenAndServe(":3000", nil)



}


