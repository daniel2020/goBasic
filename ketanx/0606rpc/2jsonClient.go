package main

import (
	"net/rpc/jsonrpc"
	"goBasic/ketanx/help"
	"goBasic/ketanx/0606rpc/bean"
	"fmt"
)

func main()  {
	//获取使用连接服务器
	client,err := jsonrpc.Dial("tcp", "127.0.0.1:3000")
	help.CheckError(err)

	//
	sum := 0
	err = client.Call("MyMath.Add",
		bean.Param{23, 5}, &sum)

	fmt.Println(sum)




}



