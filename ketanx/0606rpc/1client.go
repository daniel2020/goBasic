package main

import (
	"net/rpc"
	"goBasic/ketanx/help"
	"fmt"
	"goBasic/ketanx/0606rpc/bean"
)

func main()  {
	//1.连接到rpc服务
	client, err := rpc.DialHTTP("tcp", "127.0.0.1:3000")
	help.CheckError(err)

	fmt.Println("连接rpc服务成功")

	//2.调用服务接口
	fmt.Println("------调用加法----")
	var result int
	err =client.Call("MyMath.Add", bean.Param{4, 5}, &result )
	help.CheckError(err)

	fmt.Println("result:", result)


	fmt.Println("------调用除法----")
	response := bean.ResponseData{}
	client.Call("MyMath.Div", bean.Param{10, 5}, &response)

	fmt.Println(response.Shang, response.Yushu)

	fmt.Println("------调用除法----")
	response1 := bean.ResponseData{}
	client.Call("MyMath.Div", bean.Param{10, 3}, &response1)

	fmt.Println(response1.Shang, response1.Yushu)



	//client.Close()
}
