package help

import "database/sql"
import (
	_ "github.com/go-sql-driver/mysql"
	"strings"
)

//获取数据库连接
func GetDB() (*sql.DB, error) {
	var databaseUrl = "root:123@tcp(127.0.0.1:3306)/testdb?charset=utf8"
	db,err := sql.Open("mysql", databaseUrl)

	if err != nil {
		return nil, err

	} else {
		return db, nil
	}
}

//关闭数据连接
func CloseDB(db *sql.DB) {
	if db != nil {
		db.Close()
	}
}


//首字母大写
func firstLetterUp(str string) string {
	str = strings.ToUpper(str[0:1]) + str[1:]
	return str
}


