package help

import (
	"net"
	"bytes"
	"fmt"
	"log"
)

func ReadScoketData(conn net.Conn) (string, error) {
	//3读去服务端的响应信息read（）
	data := [1]byte{}
	buffer := bytes.Buffer{}
	for {
		_, err := conn.Read(data[:])
		if err != nil {
			fmt.Println(err)
			return "",err
		}
		if string(data[0]) == "\t" {
			break
		}
		buffer.Write(data[:])
	}

	return buffer.String(),nil
}


func CheckError(err error)  {
	if err != nil {
		log.Fatal(err)
	}
}

func PrintError(err error)  {
	if err != nil {
		log.Fatalln(err)
	}
}

func DeleteOneINSlice(list []interface{}, index int) ([]interface{}, error) {
	if index < 0 || index >= len(list) {
		fmt.Println("index 越界")
		return list, fmt.Errorf("index 越界")
	}
	
	if index == 0 {
		list = list[1:]
		
	} else if index == len(list)-1 {
		list = list[:len(list)-1]
		
	} else if index > 0 && index < len(list) -1 {
		list = append(list[:index], list[index+1:]...)
		
	} else if len(list) <= 1 {
		list = list[:0]
	}
	return list, nil
}

