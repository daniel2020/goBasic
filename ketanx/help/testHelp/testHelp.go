package main

import (
	"fmt"
	"goBasic/ketanx/help"
)



func main()  {

	//test1()

	s1 := make([]Person, 0)
	for i:=0; i<10; i++ {
		p1 := Person{i, fmt.Sprint("zhangsan", i)}
		s1 = append(s1, p1)
	}

	fmt.Println(s1)



	s1 ,_ = help.DeleteOneINSlice(s1, 2)

	for i:=0; i<len(s1); i++ {
		fmt.Println(s1[i].name)
	}



}

func test1()  {
	s1 := []interface{}{0,1,2,3,4,5,6}
	fmt.Println(s1)
	s1, err := help.DeleteOneINSlice(s1, 1)
	help.CheckError(err)
	fmt.Println(s1)

	s1, err = help.DeleteOneINSlice(s1, 0)
	help.CheckError(err)
	fmt.Println(s1)

	s1, err = help.DeleteOneINSlice(s1, 1)
	help.CheckError(err)
	fmt.Println(s1)

	s1, err = help.DeleteOneINSlice(s1, 2)
	help.CheckError(err)
	fmt.Println(s1)

	s1, err = help.DeleteOneINSlice(s1, 0)
	help.CheckError(err)
	fmt.Println(s1)

	s1, err = help.DeleteOneINSlice(s1, 0)
	help.CheckError(err)
	fmt.Println(s1)

	s1, err = help.DeleteOneINSlice(s1, 0)
	help.CheckError(err)
	fmt.Println(s1)
}

type Person struct {
	id int
	name string
}