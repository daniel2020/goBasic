package main

import (
	"fmt"
	"io/ioutil"
)

type AA interface {
	showA()
}


type BB interface {
	AA
	showB()
}

type CC interface {
	showA()
}


type Dog struct {
}

func (d Dog) showB()  {
	fmt.Println("bbbbbb")
}

func (d Dog) showA()  {
	fmt.Println("aaaaaaaa")
}

func print(aa AA)  {
	fmt.Printf("%T\n", aa)
}

type Cat struct {

}

func (c Cat) showA()  {
	fmt.Println("aaaaaaaa")
}

func print2(aa AA)  {
	fmt.Printf("-print2-----%T\n", aa)
}


func main()  {

	//d := Dog{}
	//
	//var bb BB = d
	//bb.showA()
	//bb.showB()
	//
	//var aa AA = d
	//aa.showA()
	//
	//print(bb)

	cat := Cat{}

	var a AA = cat
	var c CC = cat

	print2(a)
	print2(c)


	ioutil.ReadAll()
}