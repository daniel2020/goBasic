package main

import (
	"net"
	"log"
	"fmt"
	"time"
)

func main() {
	//连接服务器
	//ip := "127.0.0.1"
	//port := ":3000"
	ip := "127.0.0.1"
	port := ":3000"

	conn, err := net.Dial("tcp", ip + port)
	if err != nil {
		log.Fatal(err)
	}
	//关闭连接
	defer conn.Close()

	for i:=0; i<10; i++{
		time.Sleep(time.Second)

		//发送数据
		conn.Write([]byte("你好"))

		//读取服务端数据
		bs := make([]byte, 1024)
		n, err := conn.Read(bs)
		if err != nil {
			log.Fatal(err)
		}

		fmt.Println(string(bs[:n]))

	}

}
