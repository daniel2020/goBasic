package main

import (
	"net"
	"log"
	"time"
	"fmt"
)

func main() {
	//1.监听端口，绑定端口
	listen, err := net.Listen("tcp", ":3000")
	if err != nil {
		log.Fatal(err)
	}

	for { //处理多个请求
		//2.接收 客户端连接
		conn, err := listen.Accept()
		if err != nil {
			log.Fatal(err)
		}
		//5.关闭连接
		defer conn.Close()

		go func(conn net.Conn) {
			for {
				//3.读取客户端数据
				bs := make([]byte, 1024)
				n, err := conn.Read(bs)
				if err != nil {
					fmt.Println(err)
					break
				}
				time.Sleep(time.Second * 2)

				fmt.Println(string(bs[:n]))

				time.Sleep(time.Second * 2)

				//4.向客户端 回应写数据
				str := "收到了"
				conn.Write([]byte(str))
			}
		}(conn)
	}
}
