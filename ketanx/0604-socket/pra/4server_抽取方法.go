package main

import (
	"net"
	"log"
	"time"
	"fmt"
	"bytes"
)

func main() {
	//1.监听端口，绑定端口
	listen, err := net.Listen("tcp", ":3000")
	if err != nil {
		log.Fatal(err)
	}

	i:=1
	for { //处理多个请求
		//2.接收 客户端连接
		fmt.Println("111111111--i:" , i)
		i++
		conn, err := listen.Accept()
		if err != nil {
			log.Fatal(err)
		}
		//5.关闭连接
		defer conn.Close()

		go func(conn net.Conn) {
			for {
				fmt.Println("22222222222---i:", i)
				//3.读取客户端数据
				data, err := readSocketData(conn)
				if err != nil {
					fmt.Println("err:", err)
					break
				}

				time.Sleep(time.Second * 2)

				fmt.Println(data)

				time.Sleep(time.Second * 2)

				//4.向客户端 回应写数据
				str := "收到了\t"
				conn.Write([]byte(str))
			}
		}(conn)

		go func(conn net.Conn) {
			for {

				//4.向客户端 回应写数据
				str := "收到了\t"
				conn.Write([]byte(str))
			}
		}(conn)
	}
}

func readSocketData(conn net.Conn) (string, error)  {

	buffer := bytes.Buffer{}
	bs := [1]byte{}

	for {
		_,err :=conn.Read(bs[:])
		if err != nil {
			fmt.Println("err:", err)
			return "", err
		}
		//fmt.Println("33333333333")
		if bs[0] == '\t' {
			break
		}
		buffer.Write(bs[:])
	}
	return buffer.String(), nil
}




