package main

import (
	"net"
	"log"
	"time"
	"fmt"
)

func main()  {
	//1.连接服务器
	conn,err := net.Dial("tcp", "127.0.0.1:3000")
	if err != nil {
		log.Fatal(err)
	}

	//4.关闭连接
	defer conn.Close()

	for i:=0; i<5; i++ {
		//2.发送消息
		str := "hello server"
		conn.Write([]byte(str))

		time.Sleep(time.Second * 2)

		//3.读取服务器响应消息
		bs := make([]byte, 1024)
		n, _ := conn.Read(bs)


		fmt.Println(string(bs[:n]))

		time.Sleep(time.Second * 3)
	}

}

