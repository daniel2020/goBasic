package main

import (
	"net"
	"log"
	"fmt"
	"time"
	"bytes"
)

func main() {
	ch1 := make(chan bool)

	//1.监听端口，绑定端口
	listen, err := net.Listen("tcp", ":3000")
	if err != nil {
		log.Fatal(err)
	}

	//2.接受客户端请求
	conn, err := listen.Accept()
	if err != nil {
		log.Fatal(err)
	}
	//4.关闭连接
	defer conn.Close()


	go func(conn net.Conn) {
		for  {
			//读取客户端数据
			bs := make([]byte, 1024)
			n, err := conn.Read(bs)
			if err != nil {
				fmt.Println(err)
			}

			fmt.Println(string(bs[:n]))

			//3.返回客户端数据
			conn.Write([]byte("echo："+ string(bs[:n]) ))
		}
	}(conn)


	time.Sleep(time.Second * 3)
	ch1 <- true
}

func readScoketData(conn net.Conn) (string, error) {
	data := [1]byte{}
	buffer := bytes.Buffer{}

	for {
		_,err := conn.Read(data[:])

		if err != nil {
			fmt.Println(err)
			return "", err
		}

		if data[0] == '\t' {	//读取数据结束
			break
		}

		buffer.Write(data[:])
	}
	return buffer.String(), nil
}
