package main

import (
	"net"
	"log"
	"fmt"
	"time"
)

func main()  {
	fmt.Println("启动server")

	//绑定端口， 监听端口
	listener,err := net.Listen("tcp", ":8080")

	if err != nil {
		log.Fatal(err)
	}

	fmt.Println("等待客户端连接.....")

	//等待客户端 请求
	conn, err := listener.Accept() 	//阻塞
	if err != nil {
		log.Fatal(err)
	}

	//
	defer conn.Close()
	fmt.Println("有客户端请求连接.", conn.RemoteAddr())

	for  {
		//读取 客户端发送的数据
		bs := make([]byte, 512)
		n, err := conn.Read(bs)
		if err != nil {
			log.Fatal(err)
		}

		fmt.Println("client:", string(bs[:n]))

		scan := ""
		fmt.Scanln(&scan)
		fmt.Println("server:", scan)
		conn.Write([]byte(scan))
	}

	time.Sleep(time.Second * 10)
}