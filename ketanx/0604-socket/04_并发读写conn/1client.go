package main

import (
	"net"
	"log"
	"fmt"
	"time"
)

func main()  {
	ip := "127.0.0.1"
	port := ":8080"

	//ip := "192.168.149.137"
	//port := ":2000"

	conn,err := net.Dial("tcp", ip + port)
	if err != nil {
		log.Fatal(err)
	}

	//连接
	defer conn.Close()	//延迟 关闭连接

	fmt.Println("连接上了, :", conn.RemoteAddr())

	c := make(chan bool)
	go func() {
		for  {
			conn.Write([]byte("hello world"))
			time.Sleep(time.Second * 2)
		}
	}()


	go func() {
		for {
			data := ReadSocketData(conn)
			fmt.Println(data)
		}
	}()

	<- c

}



