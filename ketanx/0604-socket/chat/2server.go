package main

import (
	"net"
	"log"
	"fmt"
	"time"
	"goBasic/ketanx/help"
)

var clientConns = make(map[int]*net.Conn)
var clientId = 0



func main() {
	ch1 := make(chan bool)

	//1.监听端口，绑定端口
	listen, err := net.Listen("tcp", ":3000")
	if err != nil {
		log.Fatal(err)
	}

	for  {
		//2.接受客户端请求
		conn, err := listen.Accept()
		if err != nil {
			log.Fatal(err)
		}
		clientId++
		clientConns[clientId] = &conn

		//4.关闭连接
		defer conn.Close()

		go func(conn net.Conn) {
			for  {
				//读取客户端数据
				data,err := help.ReadScoketData(conn)
				if err != nil {
					fmt.Println("err:", err)
					break
				}

				id := 2
				toConn := clientConns[id]
				toConn

				fmt.Println(string(data))

				//3.返回客户端数据
				conn.Write([]byte("echo："+ data ) )
			}
		}(conn)
	}

	time.Sleep(time.Second * 3)
	ch1 <- true
}



