package _1

import (
	"net"
	"log"
	"fmt"
)

func main()  {
	ip := "127.0.0.1"
	port := ":8080"

	conn,err := net.Dial("tcp", ip + port)
	if err != nil {
		log.Fatal(err)
	}

	//连接
	defer conn.Close()	//延迟 关闭连接

	fmt.Println("连接上了, :", conn.RemoteAddr())

	conn.Write([]byte("hello world"))

	bs := make([]byte, 1024)
	n, err := conn.Read(bs)
	if err != nil {
		log.Fatal(err)
	}

	fmt.Println(string(bs[:n]))


}



