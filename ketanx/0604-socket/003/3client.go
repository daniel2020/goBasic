package main

import (
	"net"
	"log"
	"fmt"
	"time"
)

func main() {
	//连接服务器
	conn, err := net.Dial("tcp", "127.0.0.1:3000")
	if err != nil {
		log.Fatal(err)
	}
	//关闭连接
	defer conn.Close()

	for i:=0; i<10; i++{
		time.Sleep(time.Second)

		//发送数据
		conn.Write([]byte("你好"))

		//读取服务端数据
		bs := make([]byte, 1024)
		n, err := conn.Read(bs)
		if err != nil {
			log.Fatal(err)
		}

		fmt.Println(string(bs[:n]))

	}

}
