package main

import (
	"net"
	"log"
	"goBasic/ketanx/help"
	"fmt"
)

func main()  {
	//生产udp协议的地址
	udpAddr,err :=  net.ResolveUDPAddr("udp", "127.0.0.1:3000")
	help.CheckError(err)
	//绑定和监听端口
	udpConn,err := net.ListenUDP("udp", udpAddr)
	help.CheckError(err)

	//阻塞 等待写数据
	data := make([]byte, 1024)
	n, addr, err := udpConn.ReadFromUDP(data)
	help.PrintError(err)

	fmt.Println(string(data[:n]))





}

