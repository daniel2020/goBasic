package main

import (
	"net"
	"log"
	"fmt"
)

func main()  {

	//1.监听端口，绑定端口
	listen, err := net.Listen("tcp", ":3000")
	if err != nil {
		log.Fatal(err)
	}

	//2.接受客户端请求
	conn, err := listen.Accept()
	if err != nil {
		log.Fatal(err)
	}
	//4.关闭连接
	conn.Close()


	for  {
		//读取客户端数据
		bs := make([]byte, 1024)
		n, err := conn.Read(bs)
		if err != nil {
			fmt.Println(err)
		}

		fmt.Println(string(bs[:n]))

		//3.返回客户端数据
		conn.Write([]byte("echo："+ string(bs[:n]) ))
	}




}

//比如客户端  发送“你好”
//服务器返回“echo” + 原数据

