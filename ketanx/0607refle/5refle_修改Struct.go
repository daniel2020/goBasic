package main

import (
	"reflect"
	"fmt"
)

type User struct {
	Name string
	Age int
	Sex string
}

func main()  {
	test1()



}

func test1()  {

	user := &User{"小小", 21, "女"}
	rValue := reflect.ValueOf(user)


	if rValue.Kind() != reflect.Ptr {
		fmt.Println("不是指针类型")
		return
	}

	value := rValue.Elem().FieldByName("Sex")

	if !value.CanSet() {
		fmt.Println("不能设置")
		return
	}


	if value.Kind() != reflect.String {
		fmt.Println("不是string类型")
	}

	fmt.Println(rValue.Elem().CanSet())

	value.SetString("男")
	fmt.Println(user)
}