package main

import (
	"fmt"
	"reflect"
)

func main()  {
	//test1()
	test2()

}


//基础数据类型
func test1()  {
	var a int = 1
	var f float64 = 1.3
	var b bool = true
	var arr = [3]int{1,2,3}
	var str = "hello"

	var person = Person{"zhangsan", 23}

	fmt.Println("=========类型========")
	fmt.Println(reflect.TypeOf(a), reflect.TypeOf(a).Name(), reflect.TypeOf(a).String())
	fmt.Println(reflect.TypeOf(f), reflect.TypeOf(f).Name(), reflect.TypeOf(f).String())
	fmt.Println(reflect.TypeOf(b), reflect.TypeOf(b).Name(), reflect.TypeOf(b).String())
	fmt.Println(reflect.TypeOf(arr), reflect.TypeOf(arr).Name(), reflect.TypeOf(arr).String())
	fmt.Println(reflect.TypeOf(str), reflect.TypeOf(str).Name(), reflect.TypeOf(str).String())

	fmt.Println(reflect.TypeOf(person), reflect.TypeOf(person).Name(), reflect.TypeOf(person).String())	//


	fmt.Println("=========值=========")
	fmt.Println(reflect.ValueOf(a), reflect.ValueOf(a).String())
	fmt.Println(reflect.ValueOf(f), reflect.ValueOf(f).String())
	fmt.Println(reflect.ValueOf(b), reflect.ValueOf(b).String())
	fmt.Println(reflect.ValueOf(arr), reflect.ValueOf(arr).String())
	fmt.Println(reflect.ValueOf(str), reflect.ValueOf(str).String())

	fmt.Println(reflect.ValueOf(person), reflect.ValueOf(person).String())
}
type Person struct {
	Name string
	Age int
}


func test2()  {
	//var person = Person{"zhangsan", 23}
	var num float64 = 3.4

	//rType := reflect.TypeOf(num)
	rValue:= reflect.ValueOf(num)

	fmt.Println("type:", rValue.Type())
	fmt.Println("kind is float64:", rValue.Kind() == reflect.Float64)
	fmt.Println("value:", rValue.Float())







}
