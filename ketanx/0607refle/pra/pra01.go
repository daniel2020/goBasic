package main

import (
	"fmt"
	"reflect"
)

/*
四、通过控制台输入方法名称、参数，实现动态调用方法

实现要求：

   1.只有一个方法，无参数

   2.有多个方法，都无参数

   3.有一种参数

   4.有多种参数

   5.有多个参数
 */

var objectMap map[string]interface{} = make(map[string]interface{})

func main()  {

	for  {
		fmt.Println("=======动态调用=======")
		fmt.Println("请输入类型:")
		strType := ""
		fmt.Scanln(&strType)

		fmt.Println("请输入调用方法:")
		strMethod := ""
		fmt.Scanln(&strMethod)

		methodCall(strType, strMethod)




	}


}


func methodCall(strType string, strMethod string) {
	cat := Cat{}

	//rType := reflect.TypeOf(cat)
	rValue:= reflect.ValueOf(cat)

	//method,_ := rType.MethodByName(strMethod)

	rMethod := rValue.MethodByName(strMethod)

	if !rMethod.IsValid() {
		fmt.Println(strMethod," 该方法不存在")
		return
	}

	vals := make([]reflect.Value, 0, 2)
	rValue.MethodByName(strMethod).Call(vals)



}

type Cat struct {
	Name string
	Age int
}
func (cat Cat) Say()  {
	fmt.Println("hello cat")
}
func (cat Cat) Increase(a int)  {

}