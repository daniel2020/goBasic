package main

import (
	"reflect"
	"fmt"
)

func main()  {

	//test1()

	//test2()

	//test3()

	test4()

}

func test1()  {
	str := "hello world"
	rValue := reflect.ValueOf(str)
	rType := reflect.TypeOf(str)

	fmt.Println(rValue)
	fmt.Println(rType)

	name := rType.Name()
	fmt.Println("name:", name)
	fmt.Println(" :", rType.String())
	fmt.Println("Kind:", rType.Kind())

	fmt.Println("--------------")

	rType2 := reflect.TypeOf(&str)			//ptr  指针
	fmt.Println("type:", rType2.Kind())

}


func test2()  {
	user := User{"李四", 22, "男"}
	rType := reflect.TypeOf(user)

	fmt.Printf("%T, %T, %v, %v\n", user, rType, user, rType)

	fmt.Println(rType.Kind())		//
	fmt.Println(rType.String())		//类型全称
	fmt.Println(rType.Name())		//类型名称
	fmt.Println(rType.NumField())	//字段数量
	fmt.Println(rType.NumMethod())	//方法数量

	fmt.Println("=======================================")

	//value
	rValue := reflect.ValueOf(user)
	fmt.Println(rValue)
	fmt.Println(rValue.NumMethod())
	fmt.Println(rValue.NumField())
	fmt.Println(rValue.String())
	fmt.Println(rValue.Kind())			//
	//fmt.Println(rValue.Elem())
	fmt.Println(rValue.Field(0))		//获取对象 第几个字段的值


	fmt.Println("========方法=========")

	//获取方法
	method1 := rType.Method(0)
	fmt.Println(method1)

	//method2 := rValue.Method(0)
	//fmt.Println(method2)


}

func test3()  {
	user := User{"李四", 22, "男"}
	//rType := reflect.TypeOf(user)
	rValue:= reflect.ValueOf(user)

	for i:=0; i<rValue.NumField(); i++ {
		field := rValue.Field(i)
		val := field.Interface()
		fmt.Printf("%v %v %v \n", field.Kind(), val, field.String())
	}

}

func test4()  {
	user := User{}

	rType := reflect.TypeOf(user)
	rValue:= reflect.ValueOf(user)


}



type User struct {
	Name string
	Age int
	Sex string
}

func (user User) Say(msg string) error {
	fmt.Println("say:", msg)
	return nil
}
