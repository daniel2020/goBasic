package main

import (
	"reflect"
	"fmt"
	"goBasic/ketanx/help"
)

func main()  {
	//a := 23

	b := 0
	err := modify(&b, 22)
	help.CheckError(err)
	fmt.Println("b:", b)



}

func modifyInt(a *int, newA int) error {

	rValue := reflect.ValueOf(a)
	//判断是否指针
	if rValue.Kind() != reflect.Ptr {
		return fmt.Errorf("不是指针")
	}
	rValue.Elem().SetInt(int64(newA))
	return nil
}

func modifyStr(str *string, newString string) error {
	rValue := reflect.ValueOf(str)
	if rValue.Kind() == reflect.String {

		return fmt.Errorf("不是指针类型")
	}
	rValue.Elem().SetString(newString)
	return nil
}

//支持基本数据类型
func modify(data interface{}, newData interface{}) error {
	rValue := reflect.ValueOf(data)
	rType := reflect.TypeOf(data)

	if rValue.Kind() != reflect.Ptr {
		return fmt.Errorf("不是指针类型")
	}

	fmt.Println(rValue.Kind())
	fmt.Println(rType.Kind())

	switch rType.Kind() {
	case reflect.String:
		fmt.Println("------int---------")
		rValue.Elem().SetString(newData.(string))
	case reflect.Int:
		fmt.Println("-------string--------")
		rValue.Elem().SetInt(int64(newData.(int64)))
	case reflect.Float64:
		fmt.Println("-------float--------")
		rValue.Elem().SetFloat(newData.(float64))
	case reflect.Bool:
		fmt.Println("--------bool-------")
		rValue.Elem().SetBool(newData.(bool))
	default:
		fmt.Println("--------其他-------")
	}

	value := reflect.ValueOf(newData)
	rValue.Elem().Set(value)

	return nil
}









