package main

import (
	"reflect"
	"fmt"
)

type Dog struct {
	DogName string
	Color string
}

type Name struct {
	FirstName string
	LastName string
}

type User struct {
	Name string
	Age int
	Sex string
	Dog
	Nm Name
}

func main()  {
	test1()
}

func test1()  {
	//user := User{"李四", 22, "男"}
	//user2 := "hello"
	user3 := User{"李四", 22, "男", Dog{"大黄", "黄色"}, Name{"zhang", "xiaoming"}}


	//rType := reflect.TypeOf(user)
	//rValue := reflect.ValueOf(user)
	//rValue := reflect.ValueOf(user2)
	rValue := reflect.ValueOf(user3)
	rType := reflect.TypeOf(user3)


	//类型判断
	if rValue.Kind() == reflect.Struct {
		fmt.Println(rValue.NumField())

		filed := rType.Field(3)
		filed2 := rType.FieldByIndex([]int{3, 0})
		fmt.Printf("字段名:%v %v\n", filed.Name, filed.Type)
		fmt.Printf("字段名:%v %v\n", filed2.Name, filed2.Type)

		fmt.Println(rValue.FieldByIndex([]int{4, 0}))

	} else {
		fmt.Println("不是结构体")

	}





}

