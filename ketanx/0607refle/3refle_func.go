package main

import (
	"reflect"
	"fmt"
)

type User struct {
	Name string
	Age int
	Sex string
}

func (user User) Say(msg string) error {
	fmt.Println("say:", msg)
	return nil
}

func (user User) Eat(msg string) error {
	fmt.Println("Eat:", msg)
	return nil
}

func main()  {
	test1()
}

func test1()  {
	user := User{"李四", 22, "男"}
	rType := reflect.TypeOf(user)
	//rValue := reflect.ValueOf(user)


	//获取方法、 出参、入参
	for i:=0; i<rType.NumMethod(); i++ {

		method := rType.Method(i)

		fmt.Printf("%v %v %v\n", method.Name, method.Type.NumIn(), method.Type.In(0))
		fmt.Printf("%v %v %v\n", method.Name, method.Type.NumIn(), method.Type.In(1))
		fmt.Printf("%v %v %v\n", method.Name, method.Type.NumOut(), method.Type.Out(0))
	}


}














