package main

import (
	"net/http"
	"fmt"
)

func main()  {

	mux := http.NewServeMux()
	mux.HandleFunc("/login", login)
	mux.HandleFunc("/register", register)

	http.ListenAndServe(":3000", mux)
}


func login(w http.ResponseWriter, r *http.Request)  {
	//获取请求 方法类型、url
	fmt.Println("有客户端连接，", r.Method)
	fmt.Println("URL:", r.URL)

	//获取请求参数 username、 password、email
	username := r.FormValue("username")
	password := r.FormValue("password")
	email 	 := r.FormValue("email")

	fmt.Println(username)
	fmt.Println(password)
	fmt.Println(email)

	//返回结果
	w.Write([]byte("ok"))

}

func register(w http.ResponseWriter, r *http.Request)  {
	//获取请求方法类型、url
	fmt.Println("请求方法:", r.Method)
	fmt.Println("url:", r.URL)

	//获取请求参数
	mail := r.FormValue("mail")
	password := r.FormValue("password")
	surepassword := r.FormValue("surepassword")
	code := r.FormValue("code")
	button := r.FormValue("button")
	checkbox := r.FormValue("checkbox")

	fmt.Println(mail, password, surepassword, code, button, checkbox)


	//
	//

	//返回结果


}