package main

import "fmt"

type errorRact struct {
	msg string
	wid float64
	len float64
}

func (e *errorRact) Error() string  {
	return fmt.Sprintf("宽度:%.2f,长度:%.2f,错误信息:%s", e.wid, e.len, e.msg)
}


//计算矩形面积
func getArea(wid,len float64) (float64, error)  {
	erroMsg := ""

	if wid < 0 {
		erroMsg += "宽度为负数"
	}
	if len < 0 {
		erroMsg += "长度为负数"
	}

	if erroMsg == "" {
		return wid * len, nil

	} else {
		return 0,&errorRact{erroMsg, wid, len}
	}
}

func main()  {

	area,error := getArea(-3,4)

	if error != nil {
		fmt.Println(error.Error())
	} else {
		fmt.Println("面积：", area)
	}

}


