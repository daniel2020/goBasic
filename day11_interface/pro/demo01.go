package main

import "fmt"

type Car struct {
	brand string
	color string
}

func (c Car) run() {
	fmt.Println(c.brand,"车启动......")
}

func (c Car) stop() {
	fmt.Println(c.brand, "车停止....")
}

func (c Car) speedUp(speed float64) {
	fmt.Println(c.brand, "车加速", speed)
}

func (c Car)  printInfo()  {
	fmt.Printf("品牌：%s, 颜色:%s\n", c.brand, c.color)
}

func (c Car) setColor() {
	c.color = "透明...."
	c.printInfo()
}


func main()  {
	c1 := Car{brand:"奥迪", color:"红色"}
	c2 := Car{brand:"马自达", color:"黑色"}


	c1.run()
	c1.speedUp(20)
	c1.stop()
	c2.run()

	fmt.Println("===========测试修改调用者属性==============")

	fmt.Println("c1:", c1)
	c1.printInfo()
	c1.setColor()!
	c1.printInfo()

	fmt.Println("c1:", c1)
}