package main

import "fmt"

type Animal interface {
	eat()
	sleep()
}

type Cat struct {
	name string
}

type Dog struct {
	color string
}

func (c Cat) eat() {
	fmt.Println("猫"+ c.name+"吃鱼")
}

func (c Cat) sleep() {
	fmt.Println("猫" + c.name + "卷着睡觉")
}

func (d Dog) eat() {
	fmt.Println(d.color + "色狗啃骨头")
}

func (d Dog)sleep() {
	fmt.Println(d.color + "色狗坐着睡觉")
}

func showAnimal(as [5]Animal)  {

	for _,a := range as {

		switch ins := a.(type) {
		case Cat:
			fmt.Println("是猫----" + ins.name)
			ins.eat()
			ins.sleep()
		case Dog:
			fmt.Println("是狗~~~~~~" + ins.color)
			ins.eat()
			ins.sleep()
		}
	}

}

func main()  {
	var as = [5]Animal{}

	c1 := Cat{"花花"}
	c2 := Cat{"灰灰"}
	c3 := Cat{"小虎"}
	d1 := Dog{"黑色"}
	d2 := Dog{"白色"}

	as[0] = c1
	as[1] = c2
	as[2] = c3
	as[3] = d1
	as[4] = d2

	showAnimal(as)


}

