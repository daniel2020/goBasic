package main

import "fmt"

type Person struct {
	name string
	age int
}

func (p Person) eat() {
	fmt.Println(p.name, "在吃饭")
}

func (p Person) printInfo(){
	fmt.Println("是人")
}

type Student struct {
	Person
	school string
}

func (stu Student) printInfo() {
	fmt.Println("---是学生")
	stu.Person.printInfo()
}

func (stu Student) study() {
	fmt.Println(stu.name, "在学习....")
}

func main()  {
	p1 := Person{name:"老王", age:47}
	p1.eat()
	p1.printInfo()

	stu := Student{Person{"小明",18}, "复旦附中"}
	stu.eat()		//继承父类的方法
	stu.printInfo()	//重写父类的方法
	stu.study()		//子类自己的方法

	//stu.study()
	//stu.eat()
	//stu.printInfo()

}
