package main

import "fmt"

type USB interface {
	start()
	end()
}

type Mouse struct {
	name string
}

func (m Mouse) start()  {
	fmt.Println("鼠标准备就绪，可以开始工作......点点点点")

}

func (m Mouse) end()  {
	fmt.Println("鼠标 工作......可以退出")
}

type Upan struct {
	name string
	size float64
}

func (u Upan) start()  {
	fmt.Println("U盘连接成功，可以开始传输数据....")
}

func (u Upan) end()  {
	fmt.Println("U盘使用结束，可以  弹出--")

}

func (u Upan) copy()  {
	fmt.Println("U盘 ----copy数据...")
}

func testInterface(usb USB)  {
	usb.start()
	usb.end()
}

func main()  {
	m := Mouse{"罗技小红"}
	fmt.Println(m.name)
	f := Upan{"闪迪64G", 64}

	var usb USB

	usb = m
	usb.start()
	usb.end()

	fmt.Println("--------------")

	testInterface(m)
	testInterface(f)



}

