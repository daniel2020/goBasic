package main

import "fmt"

/*
定义一个IEngine接口，3个方法：start(),speedup(),stop()，表示启动，加速，停止
定义两个结构体实现该接口：YAMAHA 和 HONDA
实现方式分别是：
	YAMAHA
		启动：60，加速80，停止0

	HONDA
		启动：40，加速120，停止0

定义一个Car结构体，将IEngine作为字段，再提供一个car的方法：testIEngine()，
用于测试该小汽车的发动机。也就是在testIEngine()中调用start(),speedup(),stop()方法。
*/
type IEngine interface {		//引擎接口
	start()
	speedup()
	stop()
}

type YAMAHA struct {
	initSpeed float64
}

func (engine YAMAHA) start(){
	fmt.Println("启动：60")
}
func (engine YAMAHA) speedup()  {
	fmt.Println("加速:80")
}
func (engine YAMAHA) stop()  {
	fmt.Println("停止：０")
}


type HONDA struct {
	initSpeed float64
}

func (engine HONDA) start(){
	fmt.Println("启动：60")
}
func (engine HONDA) speedup()  {
	fmt.Println("加速:80")
}
func (engine HONDA) stop()  {
	fmt.Println("停止：０")
}


type Car struct {
	engine IEngine
}

func (c Car) testIEngine() {
	c.engine.start()
	c.engine.speedup()
	c.engine.stop()
}

func main()  {

	c1 := Car{engine:YAMAHA{}}
	c1.testIEngine()

	fmt.Println("=======")
	c2 := Car{engine:HONDA{}}
	c2.testIEngine()
}
