package main

import (
	"math"
	"fmt"
)

/*
1.定义一个三维空间的点(Point),设置三个属性分别表示x轴的值，y轴的值，z轴的值。
提供一个方法，用于求两点之间的距离

*/
type Point struct {
	x float64
	y float64
	z float64
}

func (p1 *Point) getDistance(p2 *Point) float64 {
	d2 := math.Pow(p1.x - p2.x,2) + math.Pow(p1.y-p2.y, 2) + math.Pow(p1.z-p2.z, 2)
	return math.Sqrt(d2)
}

func main()  {
	p1 := Point{0,0,0}
	p2 := Point{1,1,1}

	fmt.Println(p1.getDistance(&p2))

}
