package main

import (
	"math"
	"fmt"
)

/*
5.定义一个接口：形状
	定义两个方法：
		周长()
		面积()

定义三个结构体分别实现该接口：
	三角形：周长()，面积()
	矩形：周长()，面积()
	圆形：周长()，面积()

在主函数中：分别创建三种形状的对象，并调用对应的周长和面积的方法

*/
type Shape interface {
	getPeri() float64
	getArea() float64
}
/*
Rectangle	//矩形
Triangle	//三角形
circle		//圆
*/
type Rectangle struct {
	len float64
	wid float64
}

func (r Rectangle) getPeri() float64  {
	return (r.len + r.wid) * 2
}
func (r Rectangle) getArea() float64 {
	return r.len * r.wid
}

//----------------------三角形
type Triangle struct {
	a float64
	b float64
	c float64
}

func (t Triangle) getPeri() float64 {
	return t.a + t.b + t.c
}

func (t Triangle) getArea() float64  {
	p := (t.a + t.b + t.c) / 2
	return math.Sqrt(p * (p-t.a) * (p-t.b) * (p-t.c))
}


//-------------------------圆
type Circle struct {
	r float64
}

func (c Circle) getPeri() float64 {
	return 2 * math.Pi * c.r
}

func (c Circle) getArea() float64 {
	return math.Pi * c.r * c.r
}

func main()  {
	//矩形
	r := Rectangle{3,4}

	//三角形
	t := Triangle{3, 4, 5}


	//圆
	c := Circle{10}


	var s Shape
	s = r
	fmt.Println(r.getArea())
	fmt.Println(r.getPeri())
	fmt.Println(s.getArea())
	fmt.Println(s.getPeri())

	s = t
	fmt.Println(s.getArea())
	fmt.Println(s.getPeri())

	s = c
	fmt.Println(s.getPeri())
	fmt.Println(s.getArea())
}


