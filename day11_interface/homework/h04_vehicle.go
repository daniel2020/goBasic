package main

import "fmt"

/*
4.创建一个车结构体，提供属性：颜色，速度。方法：移动()。停止()。
创建两个结构体：自行车，跑车，继承车的结构体。分别新增属性和方法。创建对象，进行测试。\
*/
type Vehicle struct {
	color string
	speed float64
}

func (v Vehicle) move()  {
	fmt.Println("Vehicle车移动中....")
}
func (v Vehicle) stop()  {
	fmt.Println("Vehicle车  停下-----")
}

type Bike struct {		//自行车
	Vehicle
	chain string		//车链
	pedal string		//踏板
}

func (b Bike)dropChain()  {
	fmt.Println("自行车", b.chain , "关键时刻掉链...")
}

type Roadster struct {	//跑车
	Vehicle
	engine string		//引擎
}

func (r Roadster)engineDow()  {
	fmt.Println("跑车", r, "抛锚")
}


func main()  {
	b1 := Bike{Vehicle{"黑色",10}, "不锈钢链条", "脚踏板"}

	r1 := Roadster{Vehicle{"红色", 130}, "引擎马力十足"}

	b1.move()
	b1.stop()
	b1.dropChain()

	r1.engineDow()
	r1.move()
	r1.stop()



}


