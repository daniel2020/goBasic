package main

import "fmt"

/*
2.定义一个学生类，6个属性：姓名，年龄，性别，英语成绩，语文成绩，数学成绩，
提供3个方法，求总分(),求平均分(),打印信息()
*/
type Student struct {
	name string
	age int
	sex string
	enScore float64
	chScore float64
	maScore float64
}

/**
获取总分
 */
func (stu Student) getSumScore() float64 {
	return stu.enScore + stu.chScore + stu.maScore
}

/**
获取平均分
 */
func (stu Student) getAvgScore() float64  {
	return stu.getSumScore() / 3
}

func (stu Student) prinfInfo() {
	s := ""
	s += fmt.Sprint("\t学生姓名:", stu.name)
	s += fmt.Sprint("\t年龄:", stu.age)
	s += fmt.Sprint("\t性别:", stu.sex)
	s += fmt.Sprint("\t英语分数:", stu.enScore)
	s += fmt.Sprint("\t语文分数:", stu.chScore)
	s += fmt.Sprint("\t数学分数:", stu.maScore)

	fmt.Println(s)
}

func main()  {
	stu := Student{"小明", 15, "男", 85, 88, 91}
	fmt.Println(stu.getSumScore())
	fmt.Println(stu.getAvgScore())

	stu.prinfInfo()


}