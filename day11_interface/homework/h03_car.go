package main

import "fmt"

/*
3.定义一个car的结构体，字段有名字，速度。定义一个方法，run(),打印该车的速度
*/

type Car struct {
	name string
	speed float64
}

func (c Car) run() {
	fmt.Println("速度:",c.speed)
}

func main()  {
	c := Car{"奥迪", 120}
	c.run()


}
