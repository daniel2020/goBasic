package main

import (
	"fmt"
	"math"
)

/*
7.自定义错误：设计一个函数用于求三角形的周长，
如果三条边长无法构建成一个三角形，返回一个错误。
*/
type RectangleErr struct {
	msg string
	a float64
	b float64
	c float64
}

func (re *RectangleErr) Error() string  {
	return  fmt.Sprint("三角行三边：", re.a, re.b, re.c, "\t错误信息:", re.msg)
}

type Rectangle struct {
	a float64
	b float64
	c float64
}

func (r Rectangle) getPeri() (float64, error)  {
	if (r.a + r.b > r.c) && (r.a + r.c > r.b) && (r.b + r.c > r.a) {
		p := (r.a + r.b + r.c) / 2
		return math.Sqrt(p*(p-r.a)*(p-r.b)*(p-r.c)), nil

	} else {
		return 0, &RectangleErr{"三边不能组成三角形", r.a, r.b, r.c}
	}
}

func main()  {
	a := 1.0 //3.0
	b := 4.0
	c := 5.0
	r := Rectangle{a, b, c}

	peri,error := r.getPeri()

	if error == nil {
		fmt.Println("周长:", peri)

	} else {

		fmt.Println(error.Error())
	}
}

