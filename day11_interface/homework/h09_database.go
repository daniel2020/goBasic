package main

import "fmt"

/*
2.综合题：
定义一个DataBase接口，4个方法：insert(),update(),query(),delete()
定义两个结构体实现该接口：Mysql和Oracle
定义一个Project结构体：两个字段：name表示项目名字，DataBase表示项目用的数据库，定义一个方法：testDataBase(),测试DataBase的4个方法。
*/


type DataBase interface {
	insert()
	update()
	query()
	delete()
}

type Mysql struct {
	name string
}

func (db Mysql) insert() {
	fmt.Println("向数据库", db.name, "插入１条数据----")
}
func (db Mysql) update() {
	fmt.Println("向数据库", db.name, "update１条数据----")
}
func (db Mysql) query() {
	fmt.Println("向数据库", db.name, "查询数据----")
}
func (db Mysql) delete() {
	fmt.Println("向数据库", db.name, "delete数据----")
}


type Oracle struct {
	name string
}
func (db Oracle) insert() {
	fmt.Println("向数据库", db.name, "插入１条数据----")
}
func (db Oracle) update() {
	fmt.Println("向数据库", db.name, "update１条数据----")
}
func (db Oracle) query() {
	fmt.Println("向数据库", db.name, "查询数据----")
}
func (db Oracle) delete() {
	fmt.Println("向数据库", db.name, "delete数据----")
}

type Project struct {
	name string
	db DataBase
}

func (pro Project) testDataBase() {
	fmt.Println("项目名:", pro.name)
	pro.db.insert()
	pro.db.update()
	pro.db.query()
	pro.db.delete()
}

func main()  {

	pro1 := Project{"项目１", Mysql{"mysql"}}
	pro2 := Project{name:"项目2"}

	pro1.testDataBase()
	fmt.Println("===============================")

	pro2.db = Oracle{"oracle"}
	pro2.testDataBase()
	fmt.Println("===============================")

	pro2.db = Mysql{"mysql"}
	pro2.testDataBase()
}