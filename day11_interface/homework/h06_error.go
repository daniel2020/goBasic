package main

import (
	"fmt"
	"math"
)

/*
6.自定义错误：设计一个函数用于求圆的面积，
如果半径小于0，返回一个错误。
*/

type ErrorCircl struct {
	msg string
	radius float64
}

func (e *ErrorCircl) Error() string  {
	return fmt.Sprintf("半径:%.2f,错误信息:%s",e.radius, e.msg)
}

type Circle struct {
	r float64
}

func (c Circle) getArea() (float64, error) {
	if c.r < 0 {
		return 0, &ErrorCircl{"半径不能小于0", c.r}
	}
	area := math.Pi * c.r * c.r
	return area, nil
}


func main()  {
	r := -1.0	//10.0
	c := Circle{r}

	area, error := c.getArea()
	if error == nil {
		fmt.Println("面积:", area)

	} else {
		fmt.Println("错误------：", error.Error())
	}

}




