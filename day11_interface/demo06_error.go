package main

import (
	"fmt"
	"errors"
)


func main()  {

	/*

	 */
	//1.创建1个error对象					errors.New(...)
	fmt.Println("-----------------")
	err1 := errors.New("测试error")
	fmt.Println(err1.Error())
	fmt.Printf("%T\n", err1)


	//2.创建error对象方式2    fmt.Errorf()
	err2 := checkAge2(2)
	if err2 != nil {
		fmt.Println(err2.Error())
		return
	}
	fmt.Println("程序....go on.....")

	//3.创建error对象方式3
	error3 := fmt.Errorf("错误信息:%d\n", 100)
	fmt.Println(error3.Error())
	fmt.Printf("%T\n", error3)


	test0()

}

func checkAge2(age int) error {
	if age < 0 {
		err := fmt.Errorf("给定年龄是：%d,不合法\n", age)
		return err
	}
	fmt.Println("年龄是：", age)
	return nil
}

func test0()  {
	fmt.Println("=========test0============")
	//1
	error1 := errors.New("111111")
	fmt.Printf("%T ,%s\n", error1, error1.Error())

	//2
	error2 := fmt.Errorf("%s\n", "22222222")
	fmt.Printf("%T ,%s\n", error2, error2.Error())

}

