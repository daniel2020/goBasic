package main

import "fmt"

//控接口
type A interface {

}

type Cat struct {
	name string
	age int
}

type Person struct {
	name string
	sex string
}

func test1(a A)  {
	fmt.Println("test1--", a)
}

func test2(a interface{}) {
	fmt.Println("test2--", a)
}

//测试  接收任意类型 为入参-----相当于java中的object
func test3(slice2 []interface{})  {
	for i:=0; i<len(slice2); i++ {
		fmt.Print("i:",i,"\t")
		switch ins := slice2[i].(type) {
		case Cat:
			fmt.Println("\tcat对象：",ins.name, ins.age)
		case Person:
			fmt.Println("\tperson对象：",ins.name, ins.sex)
		case int:
			fmt.Println("\tint类型：", ins)
		case string:
			fmt.Println("\tstring类型", ins)
		}
	}
}

func test4(a int) interface{} {
	switch a {
	case 1:
		return Cat{"大花猫", 2}
	case 2:
		return Person{"李四", "男"}
	case 3:
		return 10
	case 4:
		return "hello world"
	case 5:
		s1 := []int{1,2,3,4}
		return s1
	case 6:
		map1 := map[int]interface{}{}
		map1[1]=100
		map1[2]="abcdedf"
		map1[3]=Cat{"大花猫222",3}
		map1[4]=Person{"赵六", "男"}
		return map1

	}
	return "参数不在范围内"
}

func main()  {

	var a1 A = Cat{"花猫", 5}
	var a2 A = Person{"张三", "男"}
	var a3 A = "hahah"
	var a4 int = 10


	test1(a1)
	test1(a2)
	test1(a3)
	test1(a4)

	test2(a1)
	test2(a2)
	test2(a3)
	test2(a4)

	fmt.Println("=============测试函数入参  任意类型===================")
	map1 := make(map[string]interface{})
	map1["name"] = "张三"
	map1["age"] = 30
	fmt.Println(map1)
	fmt.Println(100,"hahha","hello", Cat{"小天",2})

	slice1 := make([]interface{},0,10)
	slice1 = append(slice1, a1, a2, a3, a4)
	fmt.Println(slice1)

	test3(slice1)


	fmt.Println("============测试返回值 任意类型================")
	for i:=1; i<=6; i++ {
		var a interface{}
		a = test4(i)
		fmt.Println(a)
	}
}

