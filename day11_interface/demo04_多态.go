package main

import (
	"fmt"
	"math"
)

type Shape interface {
	getArea() (float64)
	getPeri() (float64)
}
/*
Rectangle	//矩形
Triangle	//三角形
circle		//圆
*/
type Rectangle struct { //矩形
	len float64
	width float64
}

func (r Rectangle) getArea() (area float64) {
	return r.len * r.width
}
func (r Rectangle) getPeri() (peri float64) {
	return (r.len + r.width) * 2
}


type Triangle struct { //三角形
	a float64
	b float64
	c float64
	hight float64
}

func (t Triangle) getPeri() (float64) {
	return t.a + t.b + t.c
}

func (t Triangle)getArea() (float64) {
	p:= t.getPeri() / 2
	return math.Sqrt(p * (p-t.a) * (p-t.b) * (p-t.c))
}


type Circle struct { //圆
	r float64
}

func (c Circle) getPeri() (float64)  {
	return 2 * math.Pi * c.r
}

func (c Circle) getArea() (float64) {
	return math.Pi * c.r * c.r
}


func testShape(s Shape)  {

	fmt.Printf("周长:%.2f\n", s.getPeri())
	fmt.Printf("面积：%.2f\n", s.getArea())
}

//转型1
func getType(s Shape)  {
	if ins,ok := s.(Triangle); ok {
		fmt.Println("是三角形,三边是",ins.a,ins.b,ins.c)

	} else if ins, ok := s.(Circle); ok {
		fmt.Println("是圆形,半径", ins.r)

	} else {
		fmt.Println("矩形")
	}

}

func getType2(s Shape)  {
	switch s.(type) {
	case Triangle :
		fmt.Println("三角形---")
	case Circle :
		fmt.Println("圆形------")
	case Rectangle:
		fmt.Println("矩形----------")
	}


}


func main()  {
	r := Rectangle{3, 4}
	fmt.Println(r.getArea(), r.getPeri())
	testShape(r)

	t := Triangle{a:3,b:4,c:5}
	fmt.Printf("a:%.2f, b:%.2f, c:%.2f\n",t.a, t.b, t.c)
	fmt.Println(t.getPeri(), t.getArea())
	testShape(t)

	c := Circle{10}
	fmt.Println(c.getPeri(), t.getArea())
	testShape(c)		//多态

	fmt.Println("========================")


	var arr = [4]Shape {t, r, c}

	for _,v := range arr {
		getType(v)
	}

	fmt.Println("------------------------")
	for _,v := range arr {
		getType2(v)
	}
}




