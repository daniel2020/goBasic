package main

import (
	"fmt"
	"math/rand"
	"time"
)

func main()  {


	arr := [3][4]int{{1,2,3,4}, {5,6,7,8}, {9,10,11,12}}
	fmt.Println(arr)
	fmt.Println("二维数组的长度", len(arr))

	fmt.Println(arr[0])
	fmt.Println("一维数组的长度:", len(arr[0]))

	fmt.Println(arr[0][0], arr[0][1], arr[2][2])

	//遍历二维数组

	for i:=0; i<len(arr); i++ {
		for j:=0; j<len(arr[i]); j++ {
			fmt.Print(arr[i][j], "\t")
		}
		fmt.Println()
	}

	

	fmt.Println("=================================")
	f1()
}

//二维数组　　查找最大值最小值

func f1()  {
	arr := [3][4]int{}
	rand.Seed(time.Now().UnixNano())

	//初始化
	num := 1
	for i:=0; i<len(arr); i++ {
		for j:=0; j<len(arr[i]); j++ {
			arr[i][j] = rand.Intn(100)
			num++
		}
	}

	fmt.Println(arr)

	//最大值
	max := arr[0][0]
	for i:=0; i<len(arr); i++ {
		for j:=0; j<len(arr[i]); j++ {
			if arr[i][j] > max {
				max = arr[i][j]
			}
		}
	}

	fmt.Println("max:", max)


	//最小值
	min := arr[0][0]
	for i:=0; i<len(arr); i++ {
		for j:=0; j<len(arr[i]); j++ {
			if arr[i][j] < min {
				min = arr[i][j]
			}
		}
	}
	fmt.Println("min:", min)
}


