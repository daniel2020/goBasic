package main

import "fmt"

func main()  {

	//数组声明
	var arr [4]int

	//数组存放数据
	arr[0] = 1
	arr[1] = 2
	arr[2] = 3
	arr[3] = 4

	//数组元素访问
	fmt.Println(arr[0])
	fmt.Println(arr[3])

	//数组遍历
	for i:=0; i<len(arr); i++ {
		fmt.Println(arr[i])
	}

	//数组的长度、容量
	fmt.Println("len:", len(arr))
	fmt.Println("cap:", cap(arr))


	//修改数据
	arr[0] = 100
	fmt.Println(arr)

	fmt.Println("=========")

	//数组  其他声明 创建方式
	var a [4]int // var a = [4]int{}
	fmt.Println(a)

	var b = [4]int{1,2,3,4}
	fmt.Println(b)

	var c = [5]int{1,2,3}		//存部分元素
	fmt.Println(c)

	var d = [5]int{1:1, 3:3}	//指定位置
	fmt.Println(d)

	e := [5]string{"rose", "王二狗", "jack"} //存放字符串
	fmt.Println(e)

	f := [...]int{1,2,3,4,5}				//不指定数组size， 自动推断
	fmt.Println(f)

	g := [...]int{1:3, 6:5}
	fmt.Println(g, "长度:", len(g))

}






















