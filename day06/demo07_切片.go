package main

import "fmt"

/*
切片：　　
	变长数组
	是一个引用类型的容器，　指向了一个底层数组
 */
func main()  {
	arr := [4]int{1,2,3,4}
	fmt.Println(arr)

	var s1 []int
	fmt.Println(s1)
	fmt.Println(s1 == nil)

	s2 := []int{1,2,3,4}
	fmt.Println(s2)


	s3 := make([]int, 3, 8)
	fmt.Println(s3)
	fmt.Printf("&s3:%p\n", &s3)
	fmt.Println("切片长度:", len(s3), "，　切片容量:", cap(s3))

	//赋值
	s3[0] = 1
	s3[1] = 2
	s3[2] = 3
	fmt.Println(s3)
	//s3[3] = 4

	fmt.Println("=============")
	//遍历

	for i:=0; i<len(s3); i++ {
		fmt.Println(s3[i])
	}

	for index, value := range s3  {
		fmt.Println(index, value)
	}


}

