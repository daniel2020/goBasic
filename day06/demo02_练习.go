package main

import "fmt"

func main()  {
	f1()
	f2()
	f3()
	f4()
}
/*
练习1：创建一个int类型的数组，存储4个数。分别进行赋值，并打印结果。
练习2：创建一个double类型的数组，存储2个小数。
练习3：创建一个String类型的数组，存储3个字符串。
练习4：给定一个数组，arr1 := [10] int{5,4,3,7,10,2,9,8,6,1}，求数组中所有数据的总和。
 */

func f1()  {
	//练习1：创建一个int类型的数组，存储4个数。分别进行赋值，并打印结果。
	var arr [4]int

	arr[0] = 10
	arr[1] = 20
	arr[2] = 30
	arr[3] = 40

	fmt.Println(arr)
}

func f2()  {
	//练习2：创建一个double类型的数组，存储2个小数。
	var arr = [2]float64{1.5, 3.14}

	fmt.Println(arr)
}

func f3()  {
	//练习3：创建一个String类型的数组，存储3个字符串。

	arr := [3]string{"hello", "daniel", "hahah"}
	fmt.Println(arr)
}

func f4()  {
	//练习4：给定一个数组，arr1 := [10] int{5,4,3,7,10,2,9,8,6,1}，求数组中所有数据的总和。
	arr1 := [10] int{5,4,3,7,10,2,9,8,6,1}

	sum := 0
	for i:=0; i<len(arr1); i++ {
		sum += arr1[i]
	}

	fmt.Println("sum:", sum)


}











