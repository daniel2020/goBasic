package main

import (
	"math/rand"
	"time"
	"fmt"
)

func main()  {
	//6.给定一个整型数组，长度为10。数字取自随机数。

	arrRadom := [10]int {}

	//用随机数   初始化数组
	for i:=0; i< len(arrRadom); i++ {
		arrRadom[i] = getRadomNum()
	}

	//检查结果
	fmt.Println(arrRadom)


	//排序
	for i:=0; i<len(arrRadom)-1; i++ {
		for j:=0; j<len(arrRadom)-i-1;  j++{
			if arrRadom[j] > arrRadom[j+1] {
				arrRadom[j], arrRadom[j+1] = arrRadom[j+1], arrRadom[j]
			}
		}
	}

	fmt.Println(arrRadom)




}

/**
获取随机数
 */
func getRadomNum() (num int) {

	rand.Seed(time.Now().UnixNano())

	return rand.Intn(100)
}

