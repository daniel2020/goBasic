package main

import "fmt"

func main()  {
	//3.创建float64类型数组，并存储4个浮点小数，打印输出

	arr := [4]float64 {1.2, 3.14, 1.1415, 2.99}

	for i:=0; i<len(arr); i++ {
		fmt.Println(arr[i])
	}

	for index, value := range arr {
		fmt.Println(index,"--", value)
	}

}
