package main

import "fmt"

func main()  {
	//5.题目：某个公司采用公用电话传递数据，数据是四位的整数，
	// 在传递过程中是加密的，加密规则如下：
	// 每位数字都加上5,然后用和，除以10的余数代替该数字，
	// 再将第一位和第四位交换，第二位和第三位交换。
	
    num := 1978
    newNum := encrypt(num)

    fmt.Println(newNum)
	
	
}

/*
加密
 */
func encrypt(num int) (newNum int) {

	//拆解
	a := num / 1000
	b := num %1000 / 100
	c := num % 100 / 10
	d := num % 10

	//代替
	a = (a + 5) % 10
	b = (b + 5) % 10
	c = (c + 5) % 10
	d = (d + 5) % 10

	//换位
	a,d = d,a
	b,c = c,b

	//拼接
	newNum = a * 1000 + b * 100 + c * 10 + d

	return ;
}

func encrypt2(num int) (newNum int) {

	//拆解
	a := num / 1000
	b := num %1000 / 100
	c := num % 100 / 10
	d := num % 10

	s1 := []int{a,b,c,d}

	//代替
	for i:=0; i<len(s1); i++ {
		s1[i] = (s1[i] + 5) % 10
	}

	//换位
	s1[0],s1[3] = s1[3],s1[0]
	s1[1],s1[2] = s1[2],s1[1]

	//拼接
	newNum = s1[0] * 1000 + s1[1] * 100 + s1[2] * 10 + s1[3]

	return ;
}