package main

import (
	"math/rand"
	"time"
	"fmt"
)

func main()  {
	//6.给定一个整型数组，长度为10。数字取自随机数。

	arrRadom := make([]int, 10)

	//用随机数   初始化数组
	for i:=0; i< 10; i++ {
		var x int
		for {
			x := getRadomNum()
			if search(arrRadom,x) == -1 {
				break
			}
		}

		arrRadom[i] = x
	}

	//检查结果
	fmt.Println(arrRadom)


	//排序
	for i:=0; i<len(arrRadom)-1; i++ {
		for j:=0; j<len(arrRadom)-i-1;  j++{
			if arrRadom[j] > arrRadom[j+1] {
				arrRadom[j], arrRadom[j+1] = arrRadom[j+1], arrRadom[j]
			}
		}
	}

	fmt.Println(arrRadom)




}

/**
获取随机数   去重
 */
func getRadomNum() (num int) {

	rand.Seed(time.Now().UnixNano())
	return rand.Intn(100)
}

/*
顺序查找
*/
func search(s1 []int, num int) (index int)  {
	for i:=0; i<len(s1); i++ {
		if num == s1[i] {
			return i
		}
	}
	return -1
}

