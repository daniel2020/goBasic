package main

import "fmt"

func main()  {
	//1.创建int类型数组，并存储5个数值，打印输出
	arr := [5]int{1,2,3,4,5}

	for i:=0; i<len(arr); i++ {
		fmt.Println(arr[i])
	}


	fmt.Println("---------------------")

	for index,value := range arr {
		fmt.Println(index, "--", value)
	}

}
