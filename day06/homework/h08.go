package main

import (
	"math/rand"
	"time"
	"fmt"
)

func main()  {
	//8.创建一个切片，并向切片中存储10个数字，打印输出。

	s1 := []int {}

	for i:=0; i<10; i++ {
		s1 = append(s1, getRadomNum())
	}

	for _,value := range s1 {
		fmt.Println(value)
	}

}

func getRadomNum() (num int) {
	rand.Seed(time.Now().UnixNano())
	return rand.Intn(100)
}
