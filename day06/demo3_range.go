package main

import "fmt"

func main()  {

	//数组遍历的方法


	arr := [10]int{1,2,3,4,5,6,7,8,9,10}
	for index, value := range arr {
		fmt.Println("下标:", index, ",数值:", value)
	}

	arr1 := [10] int{5,4,3,7,10,2,9,8,6,1}
	sum := 0
	for _,value := range arr1 {
		sum += value
	}
	fmt.Println("sum:", sum)

}

