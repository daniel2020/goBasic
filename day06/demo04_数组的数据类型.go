package main

import "fmt"

func main()  {


	arr1 := [4]int{2,3,4,5}
	arr2 := [4]float64{2.15, 3.14, 6.19}
	arr3 := [4]int{5,6,7,8}
	arr4 := [2]string{"hello", "world"}


	fmt.Printf("arr1:%T\t %v\n", arr1, arr1)
	fmt.Printf("arr2:%T\n", arr2)
	fmt.Printf("arr3:%T\n", arr3)
	fmt.Printf("arr4:%T\n", arr4)
	fmt.Println("================")

	num := 10
	num2 := num
	fmt.Println("num:", num, ",num2:", num2)
	num2 = 20
	fmt.Println("num:", num, ",num2:", num2)

	//数组
	arr5 := arr1
	fmt.Println(arr5 == arr1)	//比较数组对应数值是否相等
	fmt.Println(arr1, arr5)

	arr5[0] = 100
	fmt.Println(arr1, arr5)

	fmt.Printf("%p\t%p", &arr5, &arr1)




}
