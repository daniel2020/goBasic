package main

import (
	"strings"
	"fmt"
	"io"
	"strconv"
	"os"
	"bufio"
)

type WorldCup struct {
	year int
	address string
	gold string
	//年份|举办地|冠军国
}


func main()  {
	map1 := readFileToMapRecord()
	reader := bufio.NewReader(os.Stdin)

	for {
		//从键盘读取 年份
		fmt.Println("请输入要查询的年份(over 为退出):")
		instr,_ := reader.ReadString('\n')
		instr = instr[:len(instr)-1]

		if instr == "over" {
			fmt.Println("游戏结束")
			break
		}

		//查询
		year,_ := strconv.Atoi(instr)
		value,ok := map1[year]

		if ok {
			fmt.Printf("%d，举办地：%s，冠军国：%s\n", value.year, value.address, value.gold)
		} else {
			fmt.Printf("%d，没有举办", year)
		}
	}
}

func readFileToMapRecord() map[int]*WorldCup {
	path := "/toMid/0F_Chain/01_BChain0514/01_go/pro/a01.text"
	file,_ := os.Open(path)

	reader := bufio.NewReader(file)

	//表头
	ss,_ := reader.ReadString('\n')
	strs := strings.Split(ss,"\n")
	strs  = strings.Split(strs[0],"|")
	fmt.Println(strs[0], strs[1], strs[2])

	//存放记录的map
	map1 := make(map[int]*WorldCup)

	for  {
		ss,err := reader.ReadString('\n')

		if err ==io.EOF {
			fmt.Println("文件读取结束")
			break
		}

		//构建WorldCup对象
		strs = strings.Split(ss, "\n")
		strs = strings.Split(strs[0], "|")
		year,_ := strconv.Atoi(strs[0])
		record:= WorldCup{year, strs[1], strs[2]}

		//放入map
		map1[year] = &record

		fmt.Println(record.year, record.address, record.gold)

	}
	fmt.Println("==============================\n\n")

	return map1
}

