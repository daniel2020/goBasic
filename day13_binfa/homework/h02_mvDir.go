package main

import (
	"os"
	"io/ioutil"
	"fmt"
)

func main() {
	srcPath := "/home/daniel/android"
	destPath := "/home/daniel/android02"

	srcPath, destPath = destPath, srcPath		//用于方便多次测试

	mvDir(srcPath, destPath)



}

/**
移动目录
*/
func mvDir(srcPath, destPath string) {

	infoList, _ := ioutil.ReadDir(srcPath)
	os.Mkdir(destPath, os.ModePerm)

	for _, info := range infoList {
		srcSubName := srcPath + "/" + info.Name()
		destSubName := destPath + "/" + info.Name()

		if info.IsDir() {
			os.Mkdir(destSubName, os.ModePerm)
			mvDir(srcSubName, destSubName)
		} else {
			moveFile(srcSubName, destSubName)
		}

		os.Remove(srcSubName)
	}
	os.Remove(srcPath)

}

/*
移动文件
*/
func moveFile(srcPath, destPath string) {
	bs, err := ioutil.ReadFile(srcPath)
	err = ioutil.WriteFile(destPath, bs, os.ModePerm)

	if err != nil {
		fmt.Printf("copyFile function, file:%s err:%s\n", srcPath, err.Error())
	}
}
