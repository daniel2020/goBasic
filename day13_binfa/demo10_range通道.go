package main

import "fmt"

func main()  {

	ch1 := make(chan string)

	go sendData(ch1)

	for value := range ch1 {
		fmt.Println("从通道中读取数据:", value)

	}


}

func sendData(ch1 chan string)  {

	for i:=1; i<=100 ; i++ {
		ch1 <- fmt.Sprint("数据", i)
	}

	close(ch1)
}