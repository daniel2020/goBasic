package main

import (
	"fmt"
	"time"
)

/*
练习：使用关闭通道结合for range从通道中读取数据
	一个goroutine写数据，两个goroutine读数据。。
 */
func main()  {
	ch1 := make(chan string)

	go send(ch1)
	go get1(ch1)
	go get2(ch1)

	time.Sleep(time.Second*10)

}

func send(ch1 chan string)  {
	for i:=0; i<10 ; i++ {
		fmt.Println("send------写数据i:", i)
		ch1 <- fmt.Sprint("数据", i)
		time.Sleep(time.Millisecond * 100)
	}

	close(ch1)

}

func get1(ch1 chan string)  {
	for {
		data, ok := <- ch1
		fmt.Println("\tget1------读数据:", data)
		if !ok {
			fmt.Println("读取结束----")
			break
		}
	}
}


func get2(ch1 chan string)  {
	for {
		data,ok := <- ch1
		fmt.Println("\t\tget2------读数据:", data)
		if !ok {
			fmt.Println("读取结束----")
			break
		}
	}
}
