package main

import "fmt"

/*
练习1：创建并启动一个子 goroutine，打印100个数字，要保证在main goroutine结束前结束。
 通道实现。
练习2：创建并启动两个子goroutine，一个打印100个数字，另一个打印100个字母，要保证在main goroutine结束前结束。
*/

func main()  {
	var ch1 chan bool = make(chan bool)

	go func() {
		for i:=0; i<100; i++ {
			fmt.Println("子goroutine---i:", i)
		}

		ch1 <- true
	}()

	<- ch1
	fmt.Println("------main over----")

}
