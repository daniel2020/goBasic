package main

import (
	"fmt"
	"time"
)

func main()  {

	var ch1 chan int
	fmt.Println(ch1)
	fmt.Printf("%T\n", ch1)

	ch1 = make(chan int)
	fmt.Println(ch1)

	ch2 := make(chan bool)

	//启动携程  执行匿名函数
	go func() {
		fmt.Println("子goroutine....")
		data:= <- ch1
		time.Sleep(time.Second * 3)
		fmt.Println("子goroutine从通道中读取main读取的数据是:", data)
		ch2 <- true	//向通道写入数据表示结束
	}()


	ch1 <- 100

	<- ch2
	fmt.Println("-----main over-----")

}



