package main

import "fmt"

func main()  {

	ch1 := make(chan int)

	go sendData(ch1)

	for {
		data,ok := <- ch1
		fmt.Println(data, ok)
		if !ok {
			fmt.Println("接收数据结束")
			break
		}
	}

}

func sendData(ch1 chan int)  {

	for i:=0; i<10; i++ {
		ch1 <- i
	}

	close(ch1)
	fmt.Println("发送数据完毕")
}
