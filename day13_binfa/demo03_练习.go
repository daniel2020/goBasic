package main

import (
	"fmt"
	"time"
)

/*
练习1：
一条协程打印1000数字，另一条协程打印1000字母
 观察运行结果。。
*/

func main()  {
	go printNum()

	go printLetter()

	time.Sleep(time.Second * 4)
}

func printNum()  {
	for i:=0; i<1000; i++ {
		fmt.Println("printNum.....i:", i)
		time.Sleep(1)
	}
}

func printLetter()  {
	for i:=0; i<1000; i++ {
		fmt.Printf("\t%c\n", i%26 + 'A')
		time.Sleep(1)
	}
}



