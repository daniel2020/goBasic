package main

import (
	"fmt"
	"time"
)

/*
练习2：
 子goroutine1，打印1-5个数字，先睡眠，每睡眠250毫秒，打印一个数字
 子goroutine2，调研A-E个字母，先睡眠，每睡眠400毫秒，打印一个字母。
观察程序的运行结果。
 主goroutine中睡眠3000毫秒
*/
func main()  {

	go print1()
	go printA()

	time.Sleep(time.Millisecond * 3000)


}

func print1()  {
	for i:=1; i<=5; i++{
		time.Sleep(time.Millisecond * 250)
		fmt.Println("goroutine1:", i)
	}
}

func printA()  {
	for i:='A'; i<='E'; i++ {
		time.Sleep(time.Millisecond * 400)
		fmt.Printf("\tgoroutine2----:%c\n", i)
	}
}