package main

import (
	"fmt"
	"time"
)

/*
练习2：创建并启动两个子goroutine，一个打印100个数字，
另一个打印100个字母，要保证在main goroutine结束前结束。
*/
var ch1 chan bool
var ch2 chan bool

func main()  {
	ch1 = make(chan bool)
	ch2 = make(chan bool)
	go print1()
	go printA()

	<- ch2
	<- ch1
	fmt.Println("====== main over =======")
}

func print1()  {

	for i:=0; i<100; i++ {
		fmt.Println("print1---i:", i)
		time.Sleep(time.Millisecond*80)
	}
	ch1 <- true
}

func printA()  {
	for i:=0; i<100 ; i++ {
		fmt.Printf("\tprintA-------%c\n", (i%26)+'A')
		time.Sleep(time.Millisecond * 200)
	}
	ch2 <- true
}