package main

import "fmt"

/*

练习2：给定一个数字，如果是1：打印熊大，如果是2：熊二，如果是3，光头强，否则不知道
练习3：使用if语句，
   成绩：
   [90,100],优秀
   [80,89],良好
   [70,79],中等
   [60,69],及格
   [0,59],不及格
练习4：给定月份，输出该月的天数。
 1，3，5，7，8，10，12—》31
 4，6，9，11—》30
 2—》29/28
*/


func main()  {
	//f1()
	//f2()
	//f3()
	f4()

}

func f1 () {
	//练习1：给定要给数字，判断是正数，负数，零
	num := 2

	if (num > 0) {
		fmt.Println("正数")
	} else if (num < 0) {
		fmt.Println("负数")
	} else {
		fmt.Println("零")
	}
}


func f2 () {
	//练习2：给定一个数字，如果是1：打印熊大，如果是2：熊二，如果是3，光头强，否则不知道
	num := 4

	if (num == 1) {
		fmt.Println("熊大")

	} else if (num == 2) {
		fmt.Println("熊二")

	} else if (num == 3) {
		fmt.Println("光头强")

	} else {
		fmt.Println("不知道")
	}
}

func f3 () {
	/*
	练习3：使用if语句，
   成绩：
   [90,100],优秀
   [80,89],良好
   [70,79],中等
   [60,69],及格
   [0,59],不及格
	 */
	score := 66

	if (score >= 90 && score <= 100) {
		fmt.Println("优秀")

	} else if (score >= 80 && score <= 89) {
		fmt.Println("良好")

	} else if (score >= 70 && score <= 79) {
		fmt.Println("中等")

	} else if (score >= 60 && score <= 69) {
		fmt.Println("及格")

	} else if (score >= 0  && score <= 59 ) {
		fmt.Println("不及格")

	} else {
		fmt.Println("分数错误")
	}
}

func f4()  {
	/*
练习4：给定月份，输出该月的天数。
 1，3，5，7，8，10，12—》31
 4，6，9，11—》30
 2—》29/28
	 */

	var month, day int

	month = 4

	if (month == 1 || month == 3 || month == 5 || month == 7 || month == 8 || month == 10 || month ==12) {
		day = 31

	} else if (month == 4 || month == 6 || month == 9 || month == 11) {
		day = 30

	} else if (month == 2){
		day = 28

	} else  {
		fmt.Println("月份有误")
	}

	fmt.Printf("%d月份有%d天", month, day)



}














