package main

import "fmt"

//if的变形写法
func main()  {


	num := 3
	if num > 0 {
		fmt.Println("正数")
	}
	fmt.Println(num)


	if num2 := -5; num2 < 0 {
		fmt.Println("负数")
	}
	//fmt.Println(num2)

}

