package main

import (
	"fmt"
	"math"
)

func main () {

	fmt.Println(math.Abs(-5))		//绝对值
	fmt.Println(math.Floor(3.1))	//向下取整
	fmt.Println(math.Ceil(3.1))		//向上取整

	fmt.Println( math.Floor(3.7 + 0.5) )	//四舍五入
	fmt.Println( math.Floor(3.3 + 0.5) )

	fmt.Println("=====================")
	fmt.Println(math.Pow(2,3))	//n次方
	fmt.Println(math.Pow(3, 2))
	fmt.Println(math.Pow10(4))	//10的ｎ次方


	fmt.Println(math.Mod(15, 4))	//求余数
	fmt.Println(math.Modf(3.18)) 		//返回值
	fmt.Println(math.Sqrt(16)	)//开根号

	//ｎ




}
