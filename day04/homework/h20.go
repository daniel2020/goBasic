package main

import "fmt"

/*
20.求1-100内，奇数的和
 */
func main()  {

	sum := 0
	for i:=1; i<=100; i++ {

		if i%2 != 0 {
			sum += i
			fmt.Println(i)
		}
	}

	fmt.Println("sum:", sum)

}
