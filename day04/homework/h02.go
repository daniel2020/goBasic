package main

import "fmt"

/*
2.使用if语句完成：给定数字：如果为1，就输出星期一，如果为2，就输出星期二，以此类推，一直到7，输出星期日。
如果是其他数字，就输出"错误信息"。
 */
func main()  {

	var day int
	day = 5
	var dayOfweek string

	if day == 1 {
		dayOfweek = "星期一"

	} else if day == 2 {
		dayOfweek = "星期二"

	} else if day == 3 {
		dayOfweek = "星期三"

	} else if day == 4 {
		dayOfweek = "星期四"

	} else if day == 5 {
		dayOfweek = "星期五"

	} else if day == 6 {
		dayOfweek = "星期六"

	} else if day == 7 {
		dayOfweek = "星期日"

	} else {
		fmt.Println("错误参数")
	}

	fmt.Println(dayOfweek)

}
