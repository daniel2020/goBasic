package main

import "fmt"

/*
19.求1-100内，偶数的和
 */

func main()  {

	sum := 0
	for i:=1; i<=100; i++ {
		if i % 2 == 0 {
			sum += i
			fmt.Println("i:", i)
		}
	}

	fmt.Println("sum:", sum)

}
