package main

import "fmt"

/*
4.使用if语句完成：给定月份，输出该月属于哪个季节。
3,4,5 春季 6,7,8 夏季  9,10,11 秋季 12, 1, 2 冬季

 */
func main()  {

	month := 12
	var season string

	if month == 3 || month == 4 || month == 5 {
		season = "春季"

	} else if month == 6 || month == 7 || month == 8 {
		season = "夏季"

	} else if month == 9 || month == 10 || month == 11 {
		season = "秋季"

	} else if month == 12 || month == 1 || month == 2 {
		season = "冬季"

	} else {
		fmt.Println("非法参数")
	}

	fmt.Println(season)
}
