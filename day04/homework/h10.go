package main

import "fmt"

/*
10.求给定的年份，是否是闰年。

	满足以下两点中任意一点就是闰年
	A：能被4整除，但是不能被100整出。
	B：能被400整除。
 */
func main()  {
	var year int
	var isLeapYear bool = false

	fmt.Println("输入一个年份:")
	fmt.Scanln(&year)
	fmt.Println(year)

	if year < 0 {
		fmt.Println("年份有误")

	} else if (year % 4 == 0 && year % 100 != 0) || (year % 400 == 0 ) {
		isLeapYear = true
	}

	if isLeapYear {
		fmt.Println(year, "是闰年" )
	} else {
		fmt.Println(year, "是平年")
	}

}