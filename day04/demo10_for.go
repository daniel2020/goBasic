package main

import "fmt"

func main () {

/*
for 表达式1; 表达式2; 表达式3 {
	循环体
	．．．
	．．．
}
*/
	//--------5个练习题
	//f1()
	//f2()
	//f3()
	//fxx()
	f5()

}

func f1()  {
	//2.使用for循环，打印58-23数字
	for i:=58; i>=23; i-- {
		fmt.Println(i)
	}
}

func f2 () {
	//3.使用for循环，打印'A'到'Z'字母
	for letter:='A'; letter<='Z'; letter++ {
		fmt.Printf("%c\n", letter)
	}
}

func f3 () {
	//4.使用for循环，打印1-100内的数字，每行打印10个。
	for i:=1; i<=100; i++ {
		fmt.Print(i, " ")

		if (i % 10 == 0) {
			fmt.Println()
		}
	}
}

func fxx () {
	//1.求1-5的数值的和
	sum:=0
	for i:=1; i<=5; i++ {
		sum += i;
	}

	fmt.Println("sum:", sum)
}

func f5()  {
	//5.打印1-100内，能被3整除，但是不能被5整除的数，并统计个数，每行打印5个。

	count := 0
	for i:=1; i<=100; i++ {
		if (i % 3 == 0 && i % 5 != 0) {
			count ++
			fmt.Print(i, " ")
			if (count % 5 == 0) {
				fmt.Println()
			}
		}
	}
}



























