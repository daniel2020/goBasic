package main

import "fmt"

/*
17.使用for循环，计算5的阶乘：5!=5*4*3*2*1
 */
func main()  {

	ji := 1
	for i:=1; i<=5; i++ {
		ji *= i
	}

	fmt.Println(ji)

}

