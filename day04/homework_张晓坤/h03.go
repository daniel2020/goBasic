package main

import "fmt"

/*
2.使用if语句完成：给定数字：如果为1，就输出星期一，如果为2，就输出星期二，以此类推，一直到7，输出星期日。
如果是其他数字，就输出"错误信息"。
 */
func main()  {

	day := 9
	var dayOfweek string

	switch day {
	case 1:
		dayOfweek = "星期一"
	case 2:
		dayOfweek = "星期二"
	case 3:
		dayOfweek = "星期三"
	case 4:
		dayOfweek = "星期四"
	case 5:
		dayOfweek = "星期五"
	case 6:
		dayOfweek = "星期六"
	case 7:
		dayOfweek = "星期日"
	default:
		fmt.Println("非法参数")
	}

	fmt.Println(dayOfweek)

}
