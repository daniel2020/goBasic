package main

import (
	"time"
	"math/rand"
	"fmt"
)

/*
18.猜数游戏
猜数游戏：
1.系统产生一个个随机数：[1,100]
2.键盘输入：
	49
		提示：小了，下次大点
	78
		提示：大了，下次小点
	66
		提示：大了，下次小点
	55
		猜对了。。。
 */
func main()  {
	var ranNum int
	var guessNum int

	//产生随机数
	ranNum = getRandomNumber()

	//循环猜数
	for {
		//输入猜数
		fmt.Println("输入猜的数：")
		fmt.Scanln(&guessNum)

		//判断大小
		if guessNum > ranNum {
			fmt.Println("大了，下次小点")

		} else if guessNum < ranNum {
			fmt.Println("小了，下次大点")

		} else {
			fmt.Println("哈哈哈，猜对了，可以呀")
			break;
		}

		//重来
	}

}

/**
生成一个1---100之间的随机数
 */
func getRandomNumber()(num int){

	//时间戳
	seed := time.Now().UnixNano()
	//种子
	rand.Seed(seed)
	//生产随机数
	return rand.Intn(100 + 1)
}

