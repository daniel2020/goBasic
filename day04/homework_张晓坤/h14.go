package main

import "fmt"

/*
19.求1-100内，偶数的和
14.作业19用switch完成。
 */

func main()  {

	sum := 0
	for i:=1; i<=100; i++ {

		switch {
		case i % 2 == 0 :
			sum += i
		}
	}

	fmt.Println(sum)


}
