package main

import "fmt"

/*
12.int age = ...;
bool sex = ...;

如果age>18，并且 sex为false，
输出“女大十八变。。。。。”
 */
func main()  {
	var age int = 19
	var sex bool = false

	if age > 18 && sex == false {
		fmt.Println("女大十八变。。。。。")
	}


}