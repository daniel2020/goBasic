package main

import "fmt"

/*
4.使用if语句完成：给定月份，输出该月属于哪个季节。
3,4,5 春季 6,7,8 夏季  9,10,11 秋季 12, 1, 2 冬季

6.作业10，使用switch完成。
 */

func main()  {
	month := 2
	var season string

	switch month {
	case 3,4,5:
		season = "春季"
	case 6,7,8:
		season = "夏季"
	case 9,10,11:
		season = "秋季"
	case 12,1,2:
		season = "冬季"
	default:
		fmt.Println("参数错误")
	}

	fmt.Println(season)

}