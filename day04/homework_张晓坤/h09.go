package main

import "fmt"

/*
9.作业13使用switch完成

13.用if语句完成：从键盘输入考试分数(整数)，
如果成绩大于等于90，输出学霸实在牛，
如果大于等于80，输出学神要加油，
如果大于等于70，输出学民好害羞，
如果大于等于60输出学弱打酱油，
如果小于60，输出学渣泪在流。
 */

func main()  {
	var score int
	var msg string

	//从键盘读取分数
	fmt.Println("从键盘输入考试分数(整数):")
	fmt.Scanln(&score)

	switch {
	case score <= 100 && score >= 90 :
		msg = "学霸实在牛"
	case score < 90 && score >= 80 :
		msg = "学神要加油"
	case score < 80 && score >= 70 :
		msg = "学民好害羞"
	case score < 70 && score >= 60 :
		msg = "学弱打酱油"
	case score < 60 && score >= 0 :
		msg = "学渣泪在流"
	default:
		fmt.Println("分数有误")

	}

	fmt.Println(score, msg)


}
