package main

import "fmt"

/*
8.使用if语句完成：给定年龄，如果小于18岁，输出青少年，
如果大于等于18并且小于30岁，输出青年，否则输出中老年。
 */

func main()  {

	var age int
	var msg string

	fmt.Println("输入一个整数年龄:")
	fmt.Scanln(&age)

	fmt.Println(age)

	if age > 0 && age < 18 {
		msg = "青少年"

	} else if age >= 18 && age < 30 {
		msg = "青年"

	} else if age >= 30 {
		msg = "中老年"

	} else {
		fmt.Println("年龄有误")
	}

	fmt.Println("age:", age , ", 年龄阶段:", msg)


}