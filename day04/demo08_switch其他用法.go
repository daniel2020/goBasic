package main

import "fmt"

func main()  {


	//case值是 bool值
	switch {
	case true:
		fmt.Println(".....执行...true..1111")
	case false:
		fmt.Println(".....执行....false....222")
	}


	//用switch 写月份
	var day int
	month:=3
	switch  month {
	case 1,3,5,7,8,10,12:
		day = 31
	case 4,6,9,11:
		day = 30
	case 2:
		day = 28
	default:
		fmt.Println("月份有误")
	}

	fmt.Println(month, "月有" , day, "天")
	
	
	//作用域
	switch language:="golang"; language {
	case "golang" :
		fmt.Println("Go语言.....")
	case "java":
		fmt.Println("java语言.....")
	case "Python":
		fmt.Println("python语言....")
	case "javaScript":
		fmt.Println("javaScript.....")
	}

	//
	//练习1：使用switch实现成绩：优秀，良好，中等，及格，不及格
	score := 90.7
	var e string


	fmt.Println(e)

	switch score / 10 {
	case 0,1,2,3,4,5:
		e = "不及格"
	case 6:
		e = "及格"
	case 7:
		e = "中等"
	case 8:
		e = "良"
	case 9,10:
		e = "优秀"
	default:
		e = "分数有误："
	}

	fmt.Println(e)
	fmt.Println(score / 10 )

	switch {
	case score >= 90 && score <= 100 :
		e = "优秀"
	case score >= 80 :
		e = "良好"
	case score >= 70 :
		e = "中等"
	case score >= 60 :
		e = "及格"
	case score >= 0 :
		e = "不及格"
	default:
		e = "分数有误"
	}
	fmt.Println(e)
}


















