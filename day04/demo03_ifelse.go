package main

import "fmt"

/**


 */
func main()  {
	f1()
	f2()
	f3()

}

func f1 () {
	//练习1：给定一个年龄，如果大于18岁，输出你已经不是小孩子了。否则输出尚未成年

	age := 18
	if (age > 18) {
		fmt.Println("你已经不是小孩子了")
	} else {
		fmt.Println("尚未成年")
	}
}

func f2()  {
	//练习2：给定一个性别：如果是男，输出去男厕所，否则去女厕所
	sex := '男'

	if (sex == '男') {
		fmt.Println("去男厕所")
	} else {
		fmt.Println("去女厕所")
	}
}

func f3()  {
	//练习3：给定一个数字，输出奇偶性。能否被2整除
	num := 10

	if (num % 2 == 0) {
		fmt.Println("偶数")
	} else {
		fmt.Println("奇数")
	}
}

