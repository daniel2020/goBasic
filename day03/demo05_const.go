package main

import "fmt"

func main() {
	//1.常量定义
	//const 常量名 数据类型 = 值
	const PATH string = "www.baidu.com"	//显示定义
	const PI = 3.14						//隐式定义

	fmt.Println(PATH)
	fmt.Println(PI)

	//2. 修改
	//PATH = "www.baidu.ccccc"

	//3.定义一组常量
	const c1, c2, c3  = 1, 3.14, "hahah"
	fmt.Println(c1, c2, c3)


	const (
		MALE = 1
		FEMAL = 2
		UNKOWN = 0
	)
	fmt.Println(MALE, FEMAL, UNKOWN)

	//4.
	const (
		a int = 100
		b
		c string = "hahah"
		d
		e
	)

	fmt.Printf("a:%T, %d\n", a, a)
	fmt.Printf("b:%T, %d\n", b, b)
	fmt.Printf("c:%T, %s\n", c, c)
	fmt.Printf("d:%T, %s\n", d, d)
	fmt.Printf("e:%T, %s\n", e, e)

	f1()
}

func f1()  {
	const (
		spring = 1
		summer = 2
		autumn = 3
		winter = 4
	)

	fmt.Println(spring)
	fmt.Println(winter)
}


