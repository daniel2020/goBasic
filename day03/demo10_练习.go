package main

import "fmt"

/**
练习1：每种数据类型，分别定义2个变量，打印输出数据类型和数值。
练习2：打印输出：面朝"大海,春暖"花开
 */

func main()  {
	f1()
	f2()
}

func f1()  {
	//练习2：打印输出：面朝"大海,春暖"花开
	fmt.Println(`面朝"大海,春暖"花开`)

	fmt.Println("--面朝\"大海,春暖\"花开--")

}

func f2()  {
	var a = 2
	var s = "hello"

	fmt.Printf("a:%d,%T,%p\n", a, a, &a)
	fmt.Printf("s:%s,%T,%p\n", s, s, &s)

}
