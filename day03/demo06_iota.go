package main

import "fmt"

func main() {

	//f01()

	f02()


}

func f01 () {
	//iota
	const (
		a = 1
		b = 2
		c = iota
	)
	fmt.Println(c)


	const (
		a1 = iota
		b1 = iota
		c1 = iota
		d1 = iota
	)
	fmt.Println(a1, b1, c1, d1)

	const (
		a2 = 3
		b2 = iota
		c2 = iota
		d2
		e2
	)
	fmt.Println(a2, b2, c2, d2, e2)
}

func f02 () {
	const (
		a = iota
		b
		c
		d = "ha"
		e
		f = 100
		g
		h = iota
		i
	)
	fmt.Println(a,b,c,d,e,f,g,h,i)

}