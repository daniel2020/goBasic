package main

import "fmt"

/**
3.给定两个整数变量，求两个数的和，差，乘积，求商，求余数

 */

func main()  {
	//f1()
	//f2()
	//f3()
	//f4()
	f5()
}

func f1 () {
	//求和
	a := 3
	b := 15

	sum := a + b
	fmt.Println("sum:", sum)
}

func f2 () {
	//求差
	a := 3
	b := 15

	fmt.Println(b-a)
}

func f3 () {
	//求乘积
	a := 3
	b := 15

	mul := a * b
	fmt.Println(mul)
}

func f4 () {
	//求商
	a := 3
	b := 15

	fmt.Println(b/a)
}

func f5 () {
	//求余数
	//a := 3
	b := 15
	fmt.Println(b % 4)
}