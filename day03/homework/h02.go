package main

import "fmt"

/**
2.声明几个常量。
 */

func main()  {
	const PI, WORKDAYS = 3.14, 7
	fmt.Println(PI)
	fmt.Println(WORKDAYS)

	const (
		spring = 1
		summer = 2
		autoum = 3
		winter = 4
	)
	fmt.Println(spring)
	fmt.Println(summer)
	fmt.Println(autoum)
	fmt.Println(winter)

}
