package main

import "fmt"

/**
9.交换2个变量的值。
a := 3
b := 2
 */

func main()  {
	a := 3
	b := 2

	fmt.Println("a:",a, "b:", b)

	c := a
	a = b
	b = c

	fmt.Println("a:",a, "b:", b)

}

