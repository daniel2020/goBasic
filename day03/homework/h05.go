package main

import "fmt"

/**
5.求res1和res2的值

a := 4
b := 3
res1 := a < b && b / 2 == 1 && a % 3 != 0
false

res2 := (a+b)*3 < a<<2 || (a-b) >0
true
 */
func main()  {
	a := 4
	b := 3
	res1 := a < b && b / 2 == 1 && a % 3 != 0
	fmt.Println(res1)	//false

	res2 := (a+b)*3 < a<<2 || (a-b) >0
	fmt.Println(res2)	//true

}

