package main

import "fmt"

/**
4.给定两个整数变量，比较>,<,>=,<=,==,!=
 */

func main()  {
	a := 3
	b := 4

	fmt.Println(a > b)
	fmt.Println(a < b)
	fmt.Println(a >= b)
	fmt.Println(a <= b)
	fmt.Println(a == b)
	fmt.Println(a != b)
}
