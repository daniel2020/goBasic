package main

import "fmt"

/**
12.键盘上输入你的身高和体重并打印输出

m：1.68
cm：168
 */
func main()  {
	var high , weight float32

	fmt.Println("输入你的身高和体重")
	fmt.Scanf("%f,%f", &high, &weight)

	fmt.Printf("m:%.2f\n", high)
	fmt.Printf("cm:%d\n", int(high*100))


}
