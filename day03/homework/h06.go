package main

import "fmt"

/**
6.求a和b的结果：
a := 2
b := 3
a *= b
b %= 5
a <<= 2
b &= a
 */
func main() {
	a := 2
	b := 3	//
	a *= b	//a=6
	b %= 5	//b=3
	a <<= 2	//a=24
	b &= a	//b=4		???

	fmt.Println(a)
	fmt.Println(b)



}