package main

import "fmt"

/**
10.定义一个四位数的整数，分别获取各个位数的值

1976
	1千位
	9百位
	7十位
	6个位

 */
func main()  {
	num := 1976

	fmt.Println("num:", num)

	a := num / 1000
	b := num % 1000 / 100
	c := num % 100 / 10
	d := num % 10

	fmt.Println("a:", a)
	fmt.Println("b:", b)
	fmt.Println("c:", c)
	fmt.Println("d:", d)


}
