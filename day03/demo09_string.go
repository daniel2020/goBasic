package main

import "fmt"

func main() {

	var s1 string
	s1 = "天气很热"
	s2 := "热热热啊..."

	fmt.Printf("%T,%s\n", s1, s1)
	fmt.Println(s2)

	var i1 = 'A'
	fmt.Println(i1)
	fmt.Printf("%c, %q\n", i1, i1)



	fmt.Println("hello\nworld\tmemeda11111")

	fmt.Println("\"aaaaaa\"")

	s3 := `abcdefg`
	fmt.Println(s3)

	fmt.Println(`你好"冬天",春天来了`)



}
