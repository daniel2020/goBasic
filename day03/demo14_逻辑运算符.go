package main

import "fmt"

func main()  {

	f1 := true
	f2 := false
	f3 := true

	res1 := f1 && f2 && f3
	fmt.Println(res1)

	res2 := f1 || f2 || f3
	fmt.Println(res2)

	fmt.Println(f1, !f1)
	fmt.Println(f2, !f2)

	fmt.Println("================")
	a := 3
	b := 2
	c := 5
	res3 := a>b && c%a == b && a < (c/b)	//false
	res4 := b*2 < c || a/b != 0 || c/a > b	//true
	res5 := !(c/a == b)						//true

	fmt.Println(res3)
	fmt.Println(res4)
	fmt.Println(res5)




}

