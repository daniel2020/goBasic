package main

import "fmt"

func main() {

	//布尔
	var b1 bool
	b1 = true
	b2 := false
	fmt.Println(b1, b2)
	fmt.Printf("%T,%t\n", b1, b1)

	//整数
	var i1 int8 = 100
	fmt.Println(i1)

	var i2 uint8 = 255
	fmt.Println(i2)

	var i3 int = 1000
	fmt.Println(i3)

	var i4 byte = 200
	fmt.Println(i4)

	var i8 uint8
	i8 = i4
	fmt.Println(i8)


	//浮点数
	var f1 float32
	f1 = 3.14
	var f2 float64
	f2 = 3.14
	fmt.Println(f1, f2)
	fmt.Printf("%T,%.2f\n", f1, f1)




}


