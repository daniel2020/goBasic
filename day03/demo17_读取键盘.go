package main

import (
	"fmt"
	"bufio"
	"os"
)

func main() {

f1()


}

func f1()  {
	/*	//
		var a int
		var b float64
		var c string

		fmt.Printf("%p\n", &a)
		fmt.Println("请输入")
		//阻塞式
		fmt.Scanln(&a, &b, &c)

		fmt.Println("a:", a)
		fmt.Println("b:", b)
		fmt.Println("c:", c)


		//非阻塞式
		fmt.Println("请输入一个整数，一个浮点，一个字符串：")
		fmt.Scanf("%d,%f,%s", &a, &b, &c)
		fmt.Println("a:", a)
		fmt.Println("b:", b)
		fmt.Println("c:", c)
	*/
	//var s1, s2, s3 string
	//fmt.Println("请输入3个字符串(用,分开)")
	//fmt.Scanf("%s,%s,%s", &s1, &s2, &s3)
	//fmt.Println("s1:", s1)
	//fmt.Println("s2:", s2)
	//fmt.Println("s3:", s3)
	//

	fmt.Println("请输入一个字符串")
	reader := bufio.NewReader(os.Stdin)
	s1,_ := reader.ReadString('\n')
	fmt.Println(s1)

	fmt.Printf("%d,%d\n", '\r', '\n')

	fmt.Println("请输入一个字符串")
	data,_,_ := reader.ReadLine()
	fmt.Printf("%T,%v\n", data, data)
}