package main

import "fmt"

func main()  {
	a := 3
	b := 5
	c := 3

	fmt.Println(a > b, a > c)
	fmt.Println(a <= b, a <= c)

	fmt.Println(a == b)
	fmt.Println(a == c)

	fmt.Println(a != b)
	fmt.Println(a != c)
}

