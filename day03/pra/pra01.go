package main

import "fmt"

/**


练习2：同时定义3个整数，

练习3：同时定义3个字符串

练习4：定义变量后，没有初始值，直接访问变量？

练习5：尝试定义全局变量
 */

// 练习5：尝试定义全局变量
var x = 10

func main() {

	//f01()

	//f02()

	//f03()

	//f04()

	f05()

}

func f01 () {
//练习1：定义1个整数，1个小数，访问变量，打印数值和类型，更改变量的数值，打印数值
	//int
	//float64

	var i =10
	f := 3.14

	fmt.Printf("i:%d,%T, f:%f,%T\n", i, i, f, f)
	fmt.Println("i:", i, "f:", f)
}

func f02 () {
	//练习2：同时定义3个整数，
	var a,b,c = 1, 2, 3
	fmt.Println(a, b, c)

}

func f03 () {
//	练习3：同时定义3个字符串
	s1, s2, s3 := "hello", "world", "golang"

	fmt.Printf("s1:%s, s2:%s, s3:%s", s1, s2, s3)

}

func f04 () {
	//练习4：定义变量后，没有初始值，直接访问变量？

	var a int
	var b float64
	var c bool
	var d string
	var e []int

	fmt.Println(a)
	fmt.Println(b)
	fmt.Println(c)
	fmt.Println(d)
	fmt.Println(e)
}

func f05 () {
	fmt.Println("全局变量:", x)
}


