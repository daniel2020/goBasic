package main

import "fmt"

/**
1.// 常量组中定义常量，如果没有赋值和上一行一致
	const (
		a = iota
		b
		c
		d = "ha"
		e
		f = 100
		g
		h = iota
		i
	)
	const(
		j = iota
	)
	fmt.Println(a,b,c,d,e,f,g,h,i,j)


2.const (
		x = 'A'
		y
		m = iota
		n
	)


3.格式化：打印
	a := 100
	b := 3.14
	c := "hello"
	d := `王二狗住在隔壁`
	e := true
	f := 'A'
 */

func main() {
	f01()
}

func f01 () {
	/**
	1.// 常量组中定义常量，如果没有赋值和上一行一致
	 */
	const (
		a = iota
		b
		c
		d = "ha"
		e
		f = 100
		g
		h = iota
		i
	)
	const(
		j = iota
	)
	fmt.Println(a,b,c,d,e,f,g,h,i,j)

	const (
		x = 'A'
		y
		m = iota
		n
	)
	fmt.Printf("%T,%d\n", x,x)
	fmt.Println(y, m, n)

	fmt.Println("1111111111111")


}

