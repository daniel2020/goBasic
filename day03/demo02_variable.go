package main

import "fmt"

var b = 10
//c := 12  //这种方式不适用于 全局变量

func main() {
	var a = 11;
	fmt.Println(a)
	fmt.Println(b)
}

func function1 () {
	fmt.Println(b)
}

