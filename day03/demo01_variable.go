package main

import "fmt"

func main() {
	//f1()

	f2()


}

func f1 () {
	// 变量声明 方式1
	var a int
	a = 10
	fmt.Println(a)
	//fmt.Printf("%P", a)

	// 变量声明  方式2
	var b int = 20
	fmt.Println(b)


	//3. 类型省略, 自动类型推断
	var c = 30
	fmt.Println(c)
	fmt.Printf("%T, %d, %b, %o, %x\n", c, c, c, c, c)
	fmt.Printf("%f\n", 3.14)
	fmt.Printf("%t\n", false)


	// 4, 省略简短
	d := 50;
	fmt.Println(d)

	//更改变量的值
	d = 51
	fmt.Println(d)



	// 5. 同时定义多个变量
	var m, n, k int
	m = 1
	n = 2
	k = 3
	fmt.Printf("m:%d, n:%d, k:%d\n", m, n, k)

	var p, q int = 10, 20
	fmt.Printf("p:%d, q:%d\n", p, q)

	var r, t, s = 100, 3.14, "memeda"
	fmt.Printf("r:%d,%T	t:%f,%T	s:%s,%T\n", r,r, t,t, s,s)

	// 定义一组变量
	var (
		y = 10
		z int
	)
	fmt.Println(y, z)

	name, age := "王二狗", 30
	fmt.Println(name, age)
	name, sex := "rose", "女"
	fmt.Println(name, age, sex)

}

func f2 () {
/**
	%T,打印变量的数据类型
	%d,打印整数，10进制的
	%f,打印浮点，小数
	%.2f,%.3f
	%t,打印bool
	%s,打印字符串
	%v,原样输出
	%c,打印对应的字符
	%q,

	整数
		int8, 	int16, 	int32, 	int64
		uint8,	uint16,	uint32,	uint64
		byte,	rune
	浮点数
		float32,	float64
	布尔	bool:true, false
	字符串 string
 */
	var i1 int8  = 10
	var i2 uint16 = 20
	var f1 float32 = 30
	var f2 float64 = 40
	var f3 float64  = 10
	var b1 bool = true
	var b2 bool = false
	var s1 string = "abcdef"
	var s2 string = "hello world!!"

	var c1  = 'A'
	var c2  = 97


	fmt.Printf("i1:%T,%d\n", i1, i1)
	fmt.Printf("i2:%T,%v\n", i2, i2)
	fmt.Printf("f1:%T,%f\n", f1, f1)
	fmt.Printf("f2:%T,%.2f\n", f2, f2)
	fmt.Printf("f3:%T,%.3f\n", f3, f3)

	fmt.Printf("b1:%T,%t\n", b1, b1)
	fmt.Printf("b2:%T,%t\n", b2, b2)

	fmt.Printf("s1:%T,%s\n", s1, s1)
	fmt.Printf("s2:%T,%v\n", s2, s2)

	fmt.Printf("c1:%T,%c,%d\n", c1, c1, c1)
	fmt.Printf("c2:%T,%c,%d\n", c2, c2, c2)




}












