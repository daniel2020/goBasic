package main

import "fmt"

/*
一、分解质因数：比如90，分解出：90=2*3*3*5
 */

func main()  {

	num := 125
	yinzi := make([]int,0, 10)

	fmt.Println()

	yinzi = decompose(num, yinzi)

	fmt.Printf("--%p\n", yinzi)
	fmt.Println(yinzi)

}

/**
分解
 */
func decompose(num int, yinzi []int) []int {
	for i:=2; i<num; i++ {
		if (num % i == 0) && isPrime(i) {
			yinzi = append(yinzi, i)

			j := num / i
			if isPrime(j) {
				yinzi = append(yinzi, j)
			} else {
				yinzi = decompose(j, yinzi)		//递归分解
			}
			break
		}
	}
	fmt.Printf("%p\n", yinzi)
	return yinzi
}

//是否为质数
func isPrime(num int) bool {
	flag := true

	for i:=2; i<num; i++ {
		if num % i == 0 {
			flag = false
			break
		}
	}
	return flag
}
