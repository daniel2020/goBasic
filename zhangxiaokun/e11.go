package main

import (
	"math"
	"fmt"
)

/*
一球从100米高度自由落下，每次落地后又返跳回原高度的一半，
再下落，求它第10次落地时，共经过多少米？
*/

func main()  {

	n := 10

	sum := getJourney(n)
	fmt.Println(n, sum)
}

func getJourney(n int) float64{
	if n == 1 {
		return 100.0;

	} else {
		return getJourney(n-1) + 100.0 / math.Pow(2, float64(n)-2)
	}
}
