package service

import (
	"goBasic/zhangxiaokun/bean"
	"goBasic/zhangxiaokun/dao"
	"fmt"
)

type UserService struct {
	userDao *dao.UserDao
}

func (userService *UserService) Init(userDao *dao.UserDao) {
	userService.userDao = userDao
}

/**
userName, passwd
 */
func (userService *UserService) Login(user *bean.User) bool {
	userList := userService.userDao.GetAllUser()

	for _,userDB := range userList {
		if user.UserName == userDB.UserName && user.Passwd == userDB.Passwd {
			return true;
		}
	}
	return false
}

/**
用户注册
 */
func (userService *UserService) Register(user *bean.User) bool {
	userDao := userService.userDao

	//校验
	if user.UserName == "" && user.Passwd == "" {
		fmt.Println("测试失败---用户或密码不能为空")
		return false
	}

	userName := userDao.GetUserByUserName(user.UserName)

	//用户已存在
	if userName != nil {
		fmt.Println("用户名已被使用")
		return false
	}

	//添加用户
	userId := userDao.SaveUser(user)
	fmt.Println("注册成功:userId:", userId)
	return true
}

func (userService *UserService) GetAllUser() []*bean.User {
	return  (userService.userDao.GetAllUser())
}

