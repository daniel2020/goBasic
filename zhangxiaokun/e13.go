package main

import "fmt"

/*
自行设计对称加密算法，对 hello world 进行加密和解密
*/
func main()  {
	str := "hello world"
	bs := make([]byte, 0, len(str))
	var  n byte = 10

	for i:=0; i<len(str); i++ {
		bs = append(bs, str[i] + n)
	}

	fmt.Println("加密前:", str)
	decodeStr := string(bs)
	fmt.Println("加密后:", decodeStr)
}


