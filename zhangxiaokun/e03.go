package main

import (
	"math/rand"
	"time"
	"fmt"
)

/*
三、定义一个struct，表示Employee类。
字段有name，age，salary，创建5个Employee对象，并存入到切片中，
按照工资从高到底排序。并遍历输出。 */

type Employee struct {
	name string
	age int
	salary float64
}

func main()  {
	emps := make([]Employee,0,5)

	emps = getEmps(emps)
	fmt.Println(emps)

	//排序
	sortEmpsBySalary(emps)

	fmt.Println(emps)

}

func getEmps(emps []Employee) []Employee {
	rand.Seed(time.Now().UnixNano())

	for i:=1; i<=5; i++ {
		emp := Employee{}
		emp.name = fmt.Sprint("emp", i)
		emp.salary = float64(rand.Intn(10000)+1000)
		emps = append(emps, emp)
	}

	emps[0].age = 33
	emps[1].age = 20
	emps[2].age = 23
	emps[3].age = 28
	emps[4].age = 20

	return emps
}

/**
员工列表 按工资排序
 */
func sortEmpsBySalary(emps []Employee)  {
	for i:=0; i<len(emps)-1; i++ {
		for j:=0; j<len(emps)-i-1; j++ {

			if emps[j].salary < emps[j+1].salary {
				emps[j], emps[j+1] = emps[j+1], emps[j]
			}
		}
	}

}
