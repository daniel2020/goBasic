package main

import (
	"strings"
	"fmt"
	"strconv"
)

/*
二、将以下字符串：”18,26,39!10,20,76!48,29,15”，
切割后存入一个int类型的二维数组中。
使用循环遍历该二维数组。
并找出二维数组中的最大值以及最小值。 */
func main()  {
	str := "18,26,39!10,20,76!48,29,15"
	numArr := [3][3]int{}

	//数据解析
	strArr := strings.Split(str, "!")
	for i:=0; i<len(strArr); i++ {
		nStrArr := strings.Split(strArr[i], ",")

		numArr[i][0],_ = strconv.Atoi(nStrArr[0])
		numArr[i][1],_ = strconv.Atoi(nStrArr[1])
		numArr[i][2],_ = strconv.Atoi(nStrArr[2])
	}

	//获取最大值、最小值
	max,min := getMaxAndMIn(&numArr)

	fmt.Println("二维数组:\n", numArr)
	fmt.Printf("最大值:%d, 最小值:%d\n", max, min)


}

func getMaxAndMIn(numArr *[3][3]int) (max int, min int) {
	max = numArr[0][0]
	min = numArr[0][0]

	for i:=0; i<len(numArr); i++ {
		for j:=0; j<len(numArr[i]); j++ {

			//找最大值
			if max < numArr[i][j] {
				max = numArr[i][j]
			}
			//找最小值
			if min > numArr[i][j] {
				min = numArr[i][j]
			}
		}

	}
	return max, min
}


