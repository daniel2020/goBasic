package main

import "fmt"

/*
计算 sum(n) = 1/3 + 2/5 + 3/7 + ... + n/2n+1 的前n项和
*/
func main()  {

	n := 3
	sum := getSum(n)
	fmt.Printf("前%d项和:%f\n", n, sum)

}


//前n项的和
func getSum(n int) float64{
	sum := 0.0

	for i:=1; i<=n; i++ {
		sum += float64(i) / float64((2 * i + 1))
		fmt.Println(i, 2*i+1, sum)
	}

	return sum
}