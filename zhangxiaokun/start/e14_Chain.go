package main

import (
	"goBasic/zhangxiaokun/bean"
	"fmt"
	"goBasic/zhangxiaokun/service"
	"goBasic/zhangxiaokun/dao"
)

/*
通过go语言，实现私链，完成
创建用户，
用户转账,
挖矿(PoS)，
按交易hash查询交易信息的功能
*/



func main()  {

	userService, transaService := initChain()
	fmt.Println("==============")
	fmt.Println("userService:", userService)
	fmt.Println("transaService:", transaService)

	//注册
	gotoRegister(userService)
	//登录
	gotoLogin(userService)
	//用户列表
	showAllUsers(userService)

	//转账交易
	gotoTransferAccount(userService, transaService)
	//用户列表
	showAllUsers(userService)
	//交易列表
	showAllTransa(transaService)

}



func showAllTransa(transaService *service.TransaService) {
	transaList := transaService.GetAllTransa()

	fmt.Println("======交易列表========")
	fmt.Println("hashId\t\t\tfromUserId\ttoUserId")
	for _,transa := range transaList {
		fmt.Println(transa.HashId, transa.FromUserId, transa.ToUserId, transa.Amount)
	}

}

func showAllUsers(userService *service.UserService) {
	userList := userService.GetAllUser()

	fmt.Println("======用户列表========")
	fmt.Println("userId\t账户名\t密码\t余额")
	for _,user := range userList{
		fmt.Println(user)
	}
}

/*
转账
 */
func gotoTransferAccount(userService *service.UserService, transaService *service.TransaService) {
	fmt.Println("====转账====")
	var fromUserId, toUserId int
	var amount float64

	fmt.Println("输入 转出账号:")
	fmt.Scanln(&fromUserId)
	fmt.Println("输入 转入账号:")
	fmt.Scanln(&toUserId)
	fmt.Println("输入 转账金额:")
	fmt.Scanln(&amount)


	transa := bean.Transa{FromUserId:fromUserId, ToUserId:toUserId, Amount:amount}
	hashId := transaService.TransferAccounts(&transa)


	fmt.Println("转账成功, hashId:", hashId)
	transaDB := transaService.GetTransaByHashId(hashId)
	fmt.Printf("hashId:%d, fromUserId:%d, toUserId:%d, amount%f\n", transaDB.HashId, transaDB.FromUserId, transaDB.ToUserId, transaDB.Amount)

}

/**
登录
 */
func gotoLogin(userService *service.UserService) bool {
	var userName string
	var passwd string

	fmt.Println(" 请输入登录用户名:")
	fmt.Scanln(&userName)
	fmt.Println(" 请输入登录密码:")
	fmt.Scanln(&passwd)

	user := bean.User{UserName:userName, Passwd:passwd}
	isSuccess := userService.Login(&user)


	if isSuccess {
		fmt.Println("登录成功-----")
	} else {
		fmt.Println("登录失败-----")
	}


	return isSuccess
}

/*
注册
 */
func gotoRegister(userService *service.UserService) bool {
	var userName string
	var passwd string

	fmt.Println("请注册")
	fmt.Println("请输入用户名:")
	fmt.Scanln(&userName)
	fmt.Println("请密码:")
	fmt.Scanln(&passwd)


	user := bean.User{UserName:userName, Passwd:passwd}
	fmt.Println(user)
	isSuccess := userService.Register(&user)

	if isSuccess {
		fmt.Println("注册成功-----")
		return true

	} else {
		fmt.Println("注册失败-----")
		return false
	}

}


func initChain() (*service.UserService, *service.TransaService) {
	//初始化
	userDao := dao.UserDao{}
	tranDao := dao.TransaDao{}

	transaService := &service.TransaService{}
	transaService.Init(&userDao, &tranDao)

	userService := &service.UserService{}
	userService.Init(&userDao)




	//添加几条默认数据
	userId1 := userDao.SaveUser(&bean.User{UserName:"admin", Passwd:"123", Balance:1000})
	userId2 := userDao.SaveUser(&bean.User{UserName:"zhangsan", Passwd:"123", Balance:1000})

	transa := bean.Transa{FromUserId:userId1, ToUserId:userId2, Amount:50}
	hashId := transaService.TransferAccounts(&transa)


	u1 := userDao.GetUser(userId1)
	t1 := tranDao.GetTransa(hashId)

	fmt.Println(u1)
	fmt.Println(t1)

	return  userService, transaService
}

