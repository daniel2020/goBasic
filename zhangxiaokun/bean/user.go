package bean

import (
	"fmt"
)

type User struct {
	UserId   int
	UserName string
	Passwd   string
	Balance  float64
}

func (user *User) AddBalance(money float64) bool {
	user.Balance += money
	return true
}

func (user *User) SubBalance(money float64) bool {
	if user.Balance >= money {
		user.Balance -= money
		return true

	} else {
		fmt.Println("余额不足")
		return false
	}
}

