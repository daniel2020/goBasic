package main

import (
	"goBasic/zhangxiaokun/bean"
	"fmt"
)

func main()  {
	//testUserDao()

	testTransaDao()

}

func testTransaDao()  {
	tDao := bean.TransaDao{}

	transa1 := bean.Transa{FromUserId:101, ToUserId:102, Amount:50}
	transa2 := bean.Transa{FromUserId:102, ToUserId:103, Amount:10.3}

	tDao.SaveTransa(&transa1)
	tDao.SaveTransa(&transa2)

	tMap := tDao.TrMapHashId
	fmt.Println(tMap)

	for _,v := range tMap {
		fmt.Println(v)
	}


}

func testUserDao()  {
	userDao := bean.UserDao{}
	userDao.SaveUser(&bean.User{UserName:"zhangsan", Passwd:"123"})
	userDao.SaveUser(&bean.User{UserName:"lisi", Passwd:"123"})
	userDao.SaveUser(&bean.User{UserName:"wangwu", Passwd:"123"})


	//fmt.Println("111")
	//fmt.Println(userDao)
	fmt.Println(userDao.GetUser(1))
	fmt.Println(userDao.GetUser(2))
	fmt.Println(userDao.GetUser(3))
}