package main

import (
	"sync"
	"bufio"
	"os"
	"fmt"
)

/*
四、主goroutine中从键盘读取一个英文字符串，并逐个将每个字符写入到一个通道中，
另外创建两个子goroutine进行读取，并打印输出结果。
同时使用WaitGroup保证主goroutine在子goroutine执行结束后再结束。 */

var wg sync.WaitGroup

func main()  {
	ch1 := make(chan byte, 10)
	wg.Add(3)

	str := getString()

	go sendStr(ch1, &str, "生产者")
	go getData(ch1, "消费者1")
	go getData(ch1, "消费者2")

	wg.Wait()
}

/*
从键盘读取一段字符串
 */
func getString() string {

	reader := bufio.NewReader(os.Stdin)
	str,err := reader.ReadString('\n')

	if err != nil {
		fmt.Println("err:", err.Error())
	}

	str = str[:len(str)-1]

	return str
}

/**
往通道发送
 */
func sendStr(ch1 chan byte, str *string, name string)  {
	defer wg.Done()

	bs := []byte(*str)
	fmt.Println(bs)

	for i:=0; i<len(bs); i++ {
		ch1 <- bs[i]
		fmt.Printf("gotoutine--%s 发送数据:%c\n", name, bs[i] )
	}
	close(ch1)
}

/*
从通道读取字符，并输出
 */
func getData(ch1 chan byte, name string)  {
	defer wg.Done()

	for data := range ch1{
		fmt.Printf("gotoutine--%s 取到数据:%c\n", name, data)
	}

}