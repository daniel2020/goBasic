package main

import (
	"sync"
	"fmt"
	"math/rand"
	"time"
)

/*
练习题：模拟火车站卖票
火车票100张，4个售票口出售(4个goroutine)。
 */
var wg sync.WaitGroup

func main()  {
	wg.Add(4)
	tick := 100

	go sell(&tick, "售票口1:")
	go sell(&tick, "售票口2:")
	go sell(&tick, "售票口3:")
	go sell(&tick, "售票口4:")

	wg.Wait()
	fmt.Println("------main over----")

}


func sell(tick *int, name string)  {
	rand.Seed(time.Now().UnixNano())
	for {
		time.Sleep(time.Duration(rand.Intn(1000)))

		fmt.Println(name, *tick)
		*tick--
		if *tick <= 0 {
			break
		}
	}
	fmt.Println("\t---子G某 over---")
	wg.Done()
}