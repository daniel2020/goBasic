package main

import (
	"time"
	"fmt"
)

/*
两个写，
1个读
*/
func main()  {
	
	fun1()


	time.Sleep(time.Second)
	fmt.Println("....main...over")

}

func fun1()  {
	num := 5

	for i:=1; i<=3;  i++ {
		go func() {
			fmt.Println(i, num)
		}()

	}
	fmt.Println("....for over")

}