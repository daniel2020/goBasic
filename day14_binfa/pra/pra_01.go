package main

import "fmt"

/*
练习1：缓冲通道
子goroutine从缓冲通道中读取数据
另一个子goroutine从该缓冲通道中写入数据
 */
func main()  {

	ch1 := make(chan string, 5)

	go send1(ch1)

	for  {
		data, ok := <- ch1

		if !ok {
			fmt.Println("读取完毕....")
			break
		}
		fmt.Println("读取到数据:", data)
	}

}

func send1(ch1 chan string)  {
	for i:=1; i<=100; i++ {
		ch1 <- fmt.Sprint("发送数据:",i)
	}
	close(ch1)
}
