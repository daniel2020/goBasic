package main

import (
	"time"
	"fmt"
)

func main()  {
	ch1 := make(chan string)

	go fun1(ch1)

	time.Sleep(time.Second * 3)

	data := <- ch1
	fmt.Println(data)
	ch1 <- "上学吗"


	fun2(ch1)
	fun3(ch1)

}

func fun1(ch1 chan string)  {
	ch1 <- "我是小明"

	time.Sleep(time.Second*2)
	data:=<-ch1

	fmt.Println("回应:", data)
}

//功能：只有写入数据
func fun2(ch1 chan <- string)  {

}

//功能：只有读取数据
func fun3(ch1 <- chan string)  {

}