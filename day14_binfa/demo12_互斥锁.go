package main

import (
	"sync"
	"fmt"
	"time"
)

func main()  {

	var mutext sync.Mutex

	//fmt.Printf("%T\n", mutext)
	//fmt.Println( mutext)

	fmt.Println("main，即将锁定mutex")
	mutext.Lock()
	fmt.Println("mian已经锁定")

	for i:=1; i<=3; i++ {
		go func(i int) {
			fmt.Println("子G", i, "即将锁定")
			mutext.Lock()
			fmt.Println("子G", i, "已经锁定")

		}(i)
	}

	time.Sleep(5 * time.Second)

	fmt.Println("main即将解锁....")
	mutext.Unlock()





}

