package main

import (
	"fmt"
	"time"
)

/*
练习2：一个goroutine写入100个数据，两个goroutine读取数据
 */
func main()  {
	ch1 := make(chan int, 5)

	go send(ch1)
	go get1(ch1)
	go get1(ch1)

	time.Sleep(time.Second * 5)
}

func send(ch1 chan int)  {
	for i:=1; i<=100; i++ {
		ch1 <- i
		fmt.Println("数据:", i)
	}
	close(ch1)
}

func get1(ch1 chan int)  {
	for {
		data,ok := <- ch1

		if !ok {
			fmt.Println("读取数据完毕 get1" )
			break
		}
		fmt.Println("\tge1拿到数据:", data)
	}
}

func get2(ch1 chan int)  {

	for data := range ch1 {
		fmt.Println("\t\tget2拿到数据:", data)
	}
}