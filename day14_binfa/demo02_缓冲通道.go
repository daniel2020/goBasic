package main

import "fmt"

func main()  {
	//非缓冲


	//2.缓冲通道
/*	ch2 := make(chan int, 5)
	fmt.Println("缓冲通道:", len(ch2), cap(ch2))

	go func() {
		data:=<- ch2
		fmt.Println("读取到数据:", data)
	}()

	ch2 <- 1
	fmt.Println(len(ch2), cap(ch2))

	ch2 <- 2
	fmt.Println(len(ch2), cap(ch2))

	ch2 <- 3
	fmt.Println(len(ch2), cap(ch2))

	ch2 <- 4
	ch2 <- 5
	fmt.Println(len(ch2), cap(ch2))

	ch2 <- 6
	fmt.Println(len(ch2), cap(ch2))

*/

	ch3 := make(chan string, 5)
	fmt.Printf("%T\n", ch3)
	go sendData(ch3)

	for  {
		data,ok := <- ch3
		fmt.Println("\t读取数据:",data)

		if !ok {
			fmt.Println("读取完毕....")
			break
		}
	}
}

func sendData(ch3 chan string)  {

	for i:=1; i<=100; i++ {
		ch3 <- fmt.Sprint("发送数据:" , i)
		fmt.Println("已经写出数据:", i)
	}
	close(ch3)
}