package main

import (
	"fmt"
	"sync"
)

func main()  {

	var wg sync.WaitGroup
	//fmt.Printf("%T\n", wg)
	//fmt.Println( wg)

	wg.Add(2)

	go PrintNum1(&wg)
	go PrintNum2(&wg)

	wg.Wait()
	fmt.Println("解除阻塞")
}

func PrintNum1(wg *sync.WaitGroup)  {
	for i:=1; i<=100; i++ {
		fmt.Println("子G1  i：",i)
	}
	wg.Done()
}

func PrintNum2(wg *sync.WaitGroup)  {
	for i:=1; i<=100; i++ {
		fmt.Println("\t子G2  i：",i)
	}
	wg.Done()
}
