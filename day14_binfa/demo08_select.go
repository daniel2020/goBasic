package main

import (
	"fmt"
	"time"
)

func main()  {
	ch1 := make(chan int)
	ch2 := make(chan int)

	go func() {
		time.Sleep(time.Second * 4)
		data := <- ch1
		fmt.Println("子G中：", data)
		fmt.Println("gorountine......")
	}()


	select {
	case ch1 <- 100:	//阻塞
		fmt.Println("ch1中写出数据.....")
	case ch2 <- 200:
		fmt.Println("ch2中写出数据-------")
	case <- time.After(3 * time.Second): //
		fmt.Println("超时了......")
	}

	//time.Sleep(1)
}




