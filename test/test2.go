package main

import (
	"fmt"
	"unicode/utf8"
)

func main()  {
	//test01()

	//test02()

	test03()

}

func test01() {
	arr := [...]int{0,1,2,3,4,5,6,7,8,9}

	s1 := arr[2:6]
	s2 := s1[3:5]
	fmt.Printf(
		"s1:%v,\tlen(s1):%d,\tcap(s1):%d\n", s1, len(s1), cap(s1))
	fmt.Printf("s2:%v,\tlen(s2):%d,\tcap(s2):%d\n", s2, len(s2), cap(s2))

}

func test02()  {
	arr := [...]int{0,1,2,3,4,5,6,7,8,9}

	s1 := append(arr[:3], arr[4:]...)
	fmt.Println(len(s1), cap(s1), s1, arr)

	//s1 = append(s1, 10)
	//fmt.Println(len(s1), cap(s1), s1, arr)
	//
	//s1 = append(s1, 11)
	//fmt.Println(len(s1), cap(s1), s1, arr)

	s1 = append(arr[:3], arr[4:]...)
	fmt.Println(len(s1), cap(s1), s1, arr)		//切片删除元素会从底层数组删除元素


	s1 = s1[1:]				//删除头元素
	fmt.Println(len(s1), cap(s1), s1, arr)

	s1 = s1[:len(s1)-1] 	//删除尾元素
	fmt.Println(len(s1), cap(s1), s1, arr)


}


//测试字符串  中文
func test03()  {
	str := "yes,孔一学院!"
	fmt.Printf("%X\n", []byte(str))

	fmt.Printf("%x\n", []byte(str))

	for i, char := range str {
		fmt.Printf("%d, %x\n", i, char)
	}

	for i, char := range str {
		fmt.Printf("(%d, %c)  ", i, char)
	}

	fmt.Println("===================")

	fmt.Println(utf8.RuneCountInString(str))

	bs := []byte(str)
	for len(bs) > 0 {
		char, size := utf8.DecodeRune(bs)
		bs = bs[size:]
		fmt.Printf("%c  ", char)

	}

	fmt.Println("\n========================")
	for i,char := range []rune(str) {					//上层的用法
		fmt.Printf("(%d, %c)  ", i, char)
	}






}


