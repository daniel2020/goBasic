package main

import "fmt"

type node struct {
	data int
	pre *node
	after *node
}

func main()  {
	headNode :=	getNodeLink()

	showNodeLink(headNode)

}

func rever(n1 *node)  {
	n1.after, n1.pre = n1.pre, n1.after
}


func showNodeLink(n1 *node) {
	fmt.Println(n1.data)

	if n1.after != nil {
		showNodeLink(n1.after)
	}

}

func getNodeLink() *node  {
	n1 := node{data:1}
	n2 := node{data:2}
	n3 := node{data:3}
	n4 := node{data:4}
	n5 := node{data:5}

	n1.after = &n2

	n2.pre = &n1
	n2.after = &n3

	n3.pre = &n2
	n3.after = &n4

	n4.pre = &n3
	n4.after = &n5

	n5.pre = &n4

	return &n1
}