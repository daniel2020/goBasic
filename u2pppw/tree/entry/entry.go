package main

import (
	"goBasic/u2pppw/treepw/tree"
	"fmt"
	"goBasic/u2pppw/queue"
	"reflect"
)

func main()  {
	//test2()

	test3()

}

type myTreeNode struct {
	node *tree.Node
}

func (myNode *myTreeNode) postOrder () {
	if myNode == nil || myNode.node == nil {
		return
	}

	left := myTreeNode{myNode.node.Left}
	right := myTreeNode{myNode.node.Right}

	left.postOrder()
	right.postOrder()

	myNode.node.Print()
}


func test2()  {
	var root tree.Node
	root = tree.Node{Value:3}
	root.Left = &tree.Node{}
	root.Right = &tree.Node{5, nil, nil}
	root.Right.Left = new(tree.Node)
	root.Left.Right = tree.CreatNode(2)
	root.Right.Left.SetValue(4)

	root.Traverse()

	fmt.Println("==================")

	myNode := myTreeNode{&root}
	myNode.postOrder()
}

func test3()  {
	s1 := []int{0,1,2,3,4,5,6,7}
	fmt.Println(s1)


	q1 := []queue.Queue{}
	fmt.Println(q1)

	q2 := queue.Queue{10,20}
	fmt.Println(q2)

	for i:=0; i<10; i++ {
		q2.Push(i)
		fmt.Println(q2)
	}
	
	fmt.Println("==================")
	for i:=0; i<13; i++ {
		if q2.IsEmpty() {
			fmt.Println("已经没有元素")

		}  else {
			ele := q2.Pop()
			fmt.Println(ele, q2)
		}
	}
	
	
	fmt.Println(reflect.TypeOf(q2).Kind()  )
	

}



