package tree

import "fmt"

type Node struct {
	Value       int
	Right, Left *Node
}

func (node Node) Print()  {
	fmt.Println(node.Value, " ")
}

func (node *Node) SetValue(value int)  {
	node.Value = value
}



func CreatNode(value int) *Node {
	return &Node{Value:value}
}




func main()  {

//test1()


test2()



}

func test1()  {
	var root Node
	fmt.Println(root)

	root = Node{Value:3}
	root.Left = &Node{}
	root.Right = &Node{5, nil, nil}
	fmt.Println(root)

	root.Right.Left = new(Node)
	root.Left.Right = CreatNode(2)
	fmt.Println(root)

	//nodes := []Node {
	//	{Value:3},
	//	{},
	//	{6, nil, &root},
	//}
	//fmt.Println(nodes)

	root.Right.Left.SetValue(4)
	root.Right.Left.Print()
	fmt.Println()


	root.Print()
	root.SetValue(100)

	pRoot := &root
	pRoot.Print()
	pRoot.SetValue(300)
	pRoot.Print()

	fmt.Println("===========================")

	var pRoot2 *Node

	fmt.Println(pRoot2)

	pRoot2.SetValue(200)
}

func test2()  {
	var root Node
	root = Node{Value:3}
	root.Left = &Node{}
	root.Right = &Node{5, nil, nil}
	root.Right.Left = new(Node)
	root.Left.Right = CreatNode(2)
	root.Right.Left.SetValue(4)

	root.Traverse()
}