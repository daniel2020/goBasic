package main

import (
	"fmt"
	"goBasic/u2pppw/retiever/real"
	"goBasic/u2pppw/retiever/mock"
	"time"
	"strings"
	"io"
	"bufio"
)

type Retriever interface {
	Get(url string) string
}

type Poster interface {
	Post(url string,
		form map[string]string) string
}

const url string = "http://www.imooc.com"

func download(r Retriever) string {
	return r.Get(url)
}

func post(poster Poster)  {
	poster.Post(url,
		map[string]string{
			"name":"ccmouse",
			"course":"golang",
		})
}

//接口和接口组合　　//一个接口　由多个小接口组合起来
type RetrieverPoster interface {
	Retriever
	Poster
}

func session(s RetrieverPoster) string {
	s.Post(url, map[string]string{
		"contents":"another face imooc.com ",
	})

	return s.Get(url)
}


func main()  {
	retriever := mock.Retriever{"this is a fake imooc.com"}

	var r Retriever = &retriever

	//fmt.Println(download(r))

	//fmt.Printf("%T %v\n", r, r)
	inspect(r)

	r = &real.Retriever{UserAgent:"Mozilla/5.0", TimeOut:time.Minute}
	//fmt.Printf("%T %v\n", r, r)
	inspect(r)


	fmt.Println("========================")
	realRetriever := r.(*real.Retriever)	//转接口实例 转成 实现类对象
	fmt.Println(realRetriever.TimeOut)


	if retriever,ok := r.(*mock.Retriever); ok {
		fmt.Println(retriever.Contents)
	} else {
		fmt.Println("not a mock.Retriever ")
	}

	//retriever := r.(mock.Retriever)
	//fmt.Println(retriever)


	fmt.Println("Try a session")
	fmt.Println(session(&retriever))


}

func inspect(r Retriever)  {
	fmt.Println("Inspecting", r)

	fmt.Printf(" > %T %v\n", r, r)

	fmt.Print(" > Type switch:")

	switch v := r.(type) {
	case *mock.Retriever:
		fmt.Println(v.Contents)
	case *real.Retriever:
		fmt.Println(v.UserAgent)
	}

	fmt.Println()
}

func test1()  {

	reader := strings.NewReader("111111111")

	PrintFileContents(reader)


}

func PrintFileContents(r io.Reader)  {
	scanner := bufio.NewScanner(r)

	for scanner.Scan() {
		fmt.Println(scanner.Text())
	}

	//bytes.newreader

}