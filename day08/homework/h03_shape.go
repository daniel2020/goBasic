package main

import (
	"math"
	"fmt"
)

func main()  {
	area1,peri1 := triangle(2, 10)
	fmt.Println(area1,peri1)

	area2,peri2 := rectangle(3, 4)
	fmt.Println(area2,peri2)

	area3,peri3 := circle(10)
	fmt.Println(area3, peri3)
	
}
/*
定义3个函数，分别求三角形，圆形，矩形的周长和面积：
*/

func triangle(base float64, high float64) (area float64, peri float64)  {
	area = base * high / 2
	//周长????
	return
}
/*
矩形
*/
func rectangle(len float64, width float64) (area float64, peri float64)  {
	area = len * width
	peri = 2 * (len + width)
	return
}

/*
求圆的　面积、周长
 */
func circle(r float64) (area float64, peri float64)  {
	area = math.Pi * r * r
	peri = math.Pi * r * 2
	return
}
