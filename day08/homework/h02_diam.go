package main

import "fmt"

func main()  {
	n := 8
	printDiam(n)
}

/*
打印菱形
 */
func printDiam(n int)  {

	//上三角
	for i:=1; i<=n; i++ {
		//空白
		for j:=1; j <= n-i; j++ {
			fmt.Print("-")
		}
		//星
		for k:=1; k<=i*2-1; k++ {
			fmt.Print("*")
		}
		fmt.Println()
	}


	//下三角
	for i:=1; i<=n-1; i++ {
		//空白
		for j:=1; j<=i; j++ {
			fmt.Print("-")
		}
		//星
		for k:=1; k<=2*(n-i)-1; k++ {
			fmt.Print("*")
		}
		fmt.Println()
	}
}