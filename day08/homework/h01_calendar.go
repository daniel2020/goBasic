package main

import "fmt"

/*
万年历
a：先输出提示语句，并接受用户输入的年、月。
b：根据用户输入的年，先判断是否是闰年。
C：根据用户输入的月来判断月的天数。
D：用循环计算用户输入的年份距1900年1月1日的总天数。
E：用循环计算用户输入的月份距输入的年份的1月1日共有多少天。
F：相加D与E的天数，得到总天数。
G：用总天数来计算输入月的第一天的星期数。
H：根据G的值，格式化输出这个月的日历！
效果显示：
*/
func main()  {
	calendar1()

}

/*
万年历
*/
func calendar1()  {
	var year, month, days int

	fmt.Println("请输入年:")
	fmt.Scanln(&year)
	fmt.Println("请输入月:")
	fmt.Scanln(&month)

	fmt.Printf("年:%d,　月%d\n", year, month)

	//1900.1.1 ---至于 当前年(year-1)的天数
	for i:=1900; i<year; i++ {
		days += getDaysOfYear(i)
	}

	//当前年份当前月份
 	for i:=1; i<month; i++ {
		days += getDaysOfMonth(year, i)
	}

	daysCurrentMonth := getDaysOfMonth(year, month)

	//上1个月最后一天  星期几
	dayInWeek := days % 7

	showCalendar(dayInWeek, daysCurrentMonth)

}

/*
打印日历
 */
func showCalendar(dayInWeek, daysCurrentMonth int) {
	fmt.Println("一\t二\t三\t四\t五\t六\t日")
	for i:=0; i<dayInWeek; i++ {
		fmt.Print("--\t")
	}

	//当前月
	for i:=1; i<=daysCurrentMonth; i++ {
		fmt.Print(i, "\t")
		if dayInWeek == 6 {
			fmt.Println()
		}
		dayInWeek ++
		dayInWeek %= 7

	}
	fmt.Println()
}

/**
判断指定年份是否为闰年
 */
func isLeapYear(year int) bool {
	return (year % 4 == 0 && year % 100 != 0) || (year % 400 == 0)
}

/*
获取指定月份的天数
*/
func getDaysOfMonth(year, month int) int  {
	var days int

	switch month {
	case 1,3,5,7,8,10,12:
		days = 31
	case 4,6,9,11:
		days = 30
	case 2:
		if isLeapYear(year) {
			days = 29
		} else {
			days = 28
		}
	default:
		fmt.Println("月份有误")
	}

	return days
}

/*
获取指定年份的一年天数
 */
func getDaysOfYear(year int) int {
	if isLeapYear(year) {
		return 366
	} else {
		return 365
	}
}
