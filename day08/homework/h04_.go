package main

import (
	"fmt"
	"sort"
)
/*
定义一个函数，求给定两个数的最大公约数，最小公倍数
公约数
24
1	2	3	4	6	8	12	24
32
1	2	4	8	16	32

1	2	4	8

24
24	48	72	96	。。。
32
32	64	96	。。。
96
*/

func main()  {
	maxComDivisor, minComMuti := f1(32,24)
	fmt.Printf("最大公约数:%d, 最大公倍数:%d\n", maxComDivisor, minComMuti)

	//fmt.Println(getDivisor(24))

	//test()
}

/*
最大公约数, 最小公倍数
 */
func f1(a int, b int) (maxComDivisor int, minComMuti int)  {
	if a > b {		//确保ａ是较小的数
		a,b = b,a
	}

	s1 := getDivisor(a)
	s2 := getDivisor(b)

	for i:=len(s1)-1; i>=0; i-- {
		if searchInts(s2, s1[i]) != -1 {
			maxComDivisor = s1[i]
			minComMuti = a * b / maxComDivisor
			return
		}
	}

	maxComDivisor = 1
	minComMuti = a * b / maxComDivisor
	return;
}


/*
获取约数
*/
func getDivisor(num int) []int {
	s1 := make([]int,0,num/2)

	for i:=1; i<=num; i++ {
		if num % i == 0 {
			s1 = append(s1, i)
		}
	}
	return s1
}

/*
查找元素
*/
func searchInts(s1 []int, num int) (index int){
	for i:=0; i<len(s1); i++ {
		if s1[i] == num {
			return i
		}
	}
	return -1
}

func test()  {
	s1:=[]int{1,2,3,4,6,8,12,24}
	fmt.Println(sort.SearchInts(s1, 100))		//
}
