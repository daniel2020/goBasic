package main

import "fmt"

func main()  {
	res1 := fun1(30)
	fmt.Println(res1)

	fmt.Println(fun2())

	fun3()

	_,res2 := fun4()
	fmt.Println(res2)

	fun5()

}

func fun1(age int) int  {
	if age < 0 {
		return  0
	} else if age == 0 {
		return 0

	} else {
		return age
	}
}

func fun2() int  {
	n := 10
	if n > 0 {
		fmt.Println("是整数")
		//return n
	}

	fmt.Println("ｎ的数值是：", n)
	return 0
}

func fun3()  {
	m := 1
	if m > 0 {
		return
	}
	fmt.Println(m)
	fmt.Println("fun3...")
	//return
	fmt.Println("over....")

}

func fun4() (int, string) {
	return 0,"abc"
}

func fun5()  {
	for i:=1; i<=10; i++ {
		if i == 5 {
			//continue
			//break
			return

		}
		fmt.Println(i)
	}
	fmt.Println("fun5.....over....")


}