package main

import "fmt"

func main()  {
	a := 1
	fmt.Println("函数调用前a:", a)
	fun1(a)
	fmt.Println("函数调用后a:", a)
	fmt.Println("------------------")

	arr1 := [4]int{1,2,3}
	fmt.Println("函数调用前arr:", arr1)
	fun2(arr1)
	fmt.Println("函数调用后arr:", arr1)
	fmt.Println("------------------")

	s1 := []int{1,2,3,4,5}
	fmt.Println("函数调用前:", s1)
	fun3(s1)
	fmt.Println("函数调用后:", s1)




	fmt.Println("==========================================")

	map1 := make(map[int]string)
	map1[1] = "hello"
	map1[2] = "world"
	map1[3] = "hahah"
	fmt.Println("函数调用前map1:", map1)
	fun4(map1)
	fmt.Println("函数调用后map1:", map1)
}

func fun1( a int)  {
	fmt.Println(a)
	a = 100
	fmt.Println("函数中的a:", a)
}

func fun2(arr [4]int) {
	fmt.Println(arr)
	arr[0] = 100
	fmt.Println("函数中的arr:", arr)
}

func fun3(s2 []int) {
	fmt.Println(s2)
	s2[0] = 100
	fmt.Println("函数中的s2", s2)

}

func fun4(map1 map[int]string)  {
	fmt.Println("3333", map1)
	map1[1]="aa"
	fmt.Println("函数中的map1:", map1)

}