package main

import "fmt"

var num2  = 100


func main()  {
	n := 100
	fmt.Println(n)

	if a := 10; a > 0 {
		fmt.Println(a)
		fmt.Println(n)
	}

	//fmt.Println(a)  //无法使用a
	fmt.Println(n)

	if b:=20; n > 0 {
		n := 200
		fmt.Println(b)
		fmt.Println(n)
	}
	//fmt.Println(b)
	fmt.Println(n)


	{
		m := 300
		fmt.Println(m)
	}
	//fmt.Println(m)

	fun1()

	fun2()
}

func fun1()  {
	fmt.Println("fun1...")
	num1 := 100
	fmt.Println("num1:", num1)
	num2 := 2000
	fmt.Println("fun1:",num2)

}

func fun2()  {
	fmt.Println("..fun2...")
	//fmt.Println(num1)
	fmt.Println("fun2....num2:", num2)

}
