package main

import "fmt"

func main()  {

	sum := getSum(5)
	fmt.Println(sum)

	a := getJiechen(5)
	fmt.Println(a)


	//兔子繁衍　　斐波那契数列
	fmt.Println("兔子:", getRabit(3))
	fmt.Println("兔子:", getRabit(4))
	fmt.Println("兔子:", getRabit(5))
	fmt.Println("兔子:", getRabit(6))

	fmt.Println("====循环求兔子===")
	fmt.Println("兔子:", getRabit(5))
	fmt.Println("兔子:", getRabit(6))




}

func getSum(n int) int {
	if n == 1 {
		return 1
	}
	return getSum(n-1) + n
}


func getJiechen(n int) int {
	if n == 1 {
		return 1
	}
	return getJiechen(n-1) * n
}

/*
斐波那契额数列
*/
func getRabit(day int) int {
	if day == 1 || day == 2 {
		return 1
	}
	return getRabit(day-1) + getRabit(day-2)
}

func getRabit2(day int) int {
	a := 1
	b := 1
	c := a + b

	if day == 1 || day == 2 {
		return 1
	} else if day == 3{
		return 2
	}

	for i:=4; i<=day; i++  {
		a = b
		b = c
		c = a + b
	}

	return c
}
