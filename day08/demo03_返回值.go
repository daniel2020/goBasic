package main

import "fmt"

func main()  {
	//求1-10的和，并放大2倍输出
	res := getsum()
	fmt.Printf("%T,%d\n", res, res)
	fmt.Println(res * 2)

	fmt.Println(getSum2())

	peri, area := rectangle(3,4)
	fmt.Println("周长:", peri, "面积:", area)

	peri3, area3 := rectangle2(3,4)
	fmt.Println( peri3,  area3)

	peri2,_ := rectangle(1,2)
	fmt.Println(peri2)

	_,area2 := rectangle(1,2)
	fmt.Println(area2)

}

func getsum() (sum int) {
	sum = 0
	for i:=0; i<=10; i++ {
		sum += i
	}
	return sum
}

func getSum2() (sum int) {
	for i:=0; i<=10; i++ {
		sum += i
	}
	return ; //sum省略不写
}

func rectangle(len, wid float64) (float64, float64)  {
	peri := (len + wid) * 2
	area := len * wid
	return peri, area
}

func rectangle2(len, wid float64) (peri, area float64)  {
	peri = (len + wid) * 2
	area = len * wid
	return
}