package main

import (
	"math"
	"fmt"
)

func main()  {
	max := getMax(4,6)
	fmt.Println(max)

	sum := getSum(4, 7)
	fmt.Println(sum)

	peri, area := circle(10)
	fmt.Println("peri:", peri, ",area:", area)


}
/*
练习1：定义一个函数，接收两个参数，用于比较两个数的大小，并将大的数返回。
练习2：定义一个函数，用于接收两个参数，求和，并将结果返回。
练习3：定义一个函数，用于求圆的面积和周长，并返回结果。
 面积：PI * R * R
 周长：2 * PI * R
 */

func getMax(a,b int) (int) {
	if a > b {
		return a
	} else {
		return b
	}
}

func getSum(a,b int) (int)  {
	return a + b
}

func circle(r float64) (peri float64, area float64) {
	peri = 2 * math.Pi * r
	area = math.Pi * r * r
	return
}