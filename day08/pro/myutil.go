package main

import (
	"sort"
	"fmt"
)

func main()  {
	map1 := make(map[string]string)
	map1["addd"] = "baidu1111"
	map1["abc"] = "hellow"
	map1["xyz"] = "baidu"
	map1["bbb"] = "ziru"
	map1["bcd"] = "gooogle"

	s1 := GetMapKeyString(map1)
	fmt.Println(s1)

	s2 := GetMapKeySorted(map1)
	fmt.Println(s2)

}

/**
获取map中的key列表
 */
func GetMapKeyString(map1 map[string]string) ([]string) {
	keys := make([]string, 0, len(map1))

	for k,_ := range map1 {
		keys = append(keys, k)
	}
	return keys
}

func GetMapKeyInt(map1 map[int]string) ([]int) {
	keys := make([]int, 0, len(map1))

	for k,_ := range map1 {
		keys = append(keys, k)
	}
	return keys
}

/**
获取map中已排序的key列表
 */
func GetMapKeySorted(map1 map[string]string) ([]string) {
	keys := GetMapKeyString(map1)
	sort.Strings(keys)
	return keys
}

/*
查找元素
*/
func searchInts(s1 []int, num int) (index int){
	for i:=0; i<len(s1); i++ {
		if s1[i] == num {
			return i
		}
	}
	return -1
}
