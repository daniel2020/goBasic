package main

import "fmt"

func main()  {
	res1 := f1(5)
	fmt.Println(res1)

	f2(4)

	sum := getSum(5)
	fmt.Println(sum)

	fmt.Println("输入要去求阶乘的数:")
	var n int
	fmt.Scanln(&n)
	res2 := f3(n)
	fmt.Printf("%d阶乘=%d\n", n, res2)

	res3 := f4(80, 8)
	fmt.Println(res3)

}

func getSum(n int) (sum int) {

	for i:=1; i<=n; i++ {
		sum += i
	}
	return sum;
}


/*
1.定义一个函数，用于求5的阶乘。然后进行调用
2.定义一个函数，打印三角形，然后进行调用
*/

func f1(n int) (result int)  {
	//定义一个函数，用于求5的阶乘。然后进行调用
	result = 1

	for i:=1; i<=5; i++ {
		result *= i
	}
	return result
}

func f2(n int)  {
	//2.定义一个函数，打印三角形，然后进行调用

	for i:=1; i<=n; i++ {
		for j:=0; j<=n-i; j++ {
			fmt.Print("*")
		}
		fmt.Println()
	}
}

/*
1.定义一个方法：求n的阶乘。调用的时候，main中由键盘输入。通过参数传入函数中
2.定义一个方法：求2个数的商。2个数由参数传入。。
 */
func f3(n int) (res int) {
	res = 1
	for i:=1; i<=n; i++ {
		res *= i
	}
	return res;
}

func f4(a int, b int) (res int) {

	return a / b;
}

func fun3(a int, b int)  {

}

func fun4(a, b int) {

}

func fun5(a,b,c int, s1,s2,s3 string) {

}