package main

import "fmt"

func main()  {
	f6()
}

/*
乘法口诀
 */
func f6()  {
	/*
	 乘法表：									行
	 1×1=1									1
	 2×1=2 2×2=4							2
	 3×1=3 3×2=6 3×3=9						3
	.....
	 9×1=9 9×2=18 9×3=27.。。。。9×9=81
	*/

	for i:=1; i<=9; i++ {
		for j:=1; j<=i; j++ {
			fmt.Printf("%d*%d=%d\t", i, j, i*j)
		}
		fmt.Println()
	}
}
