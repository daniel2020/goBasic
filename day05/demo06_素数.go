package main

import "fmt"

func main()  {
	f10()
}


func f10()  {
	//练习6：打印2-100内的素数
	count := 0	// 素数个数
	for j:=2; j<=100; j++ {
		num := j
		flag := true
		for i:=2; i<num; i++ {
			if num % i == 0 {
				flag = false
				break;
			}
		}

		if flag {
			count++
			fmt.Println(num)
		}
	}
	fmt.Println("素数个数:", count)
}
