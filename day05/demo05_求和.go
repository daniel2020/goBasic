package main

import (
	"fmt"
	"math"
)

func main()  {
	f11()
}


func f11()  {
	//练习7：2 + 22 + 222+2222+22222
	var n,m int
	var sum float64

	fmt.Println("规则:n=2, m=5,  2 + 22 + 222 + 2222 + 22222 ")
	fmt.Println("输入累加基数n:")
	fmt.Scanln(&n)
	fmt.Println("输入累加次数m:")
	fmt.Scanln(&m)

	fmt.Printf("n:%d, m:%d\n", n, m)

	for i:=1; i<=m; i++ {

		addNum := 0.0			//
		for j:=0; j<i; j++{
			weizhi := math.Pow10(j) * float64(n)
			fmt.Printf("%.f ", weizhi)

			addNum += weizhi
		}

		fmt.Println("addNum:", addNum)
		sum += addNum

	}

	fmt.Printf("sum:%.f\n", sum)

}
