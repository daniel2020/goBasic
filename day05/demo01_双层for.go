package main

import (
	"fmt"
)

func main()  {

	//f1()
	//f2()
	//f3()
	//f4()
	//f5()
	//f6()
	//f7()
	//f8()
	//f9()
	f10()
	//f11()

}

func f1()  {
	/*
		*****
		*****
		*****
		*****
		*****
	*/
	//for i:=0; i<5; i++ {
	//	for j:=0; j<5; j++ {
	//		fmt.Print("*")
	//	}
	//	fmt.Println()
	//}
}

func f2()  {
/*

*
**
***
****
*****

 */
	for i:=1; i<=5; i++ {

		for j:=1; j<=i; j++ {
			fmt.Print("*")
		}
		fmt.Println()
	}
}

func f3()  {
/*
*****
****
***
**
*

 */
	for i:=1; i<=5; i++ {
		for j:=1; j<=5-i+1; j++ {
			fmt.Print("*")
		}
		fmt.Println()
	}
}

func f4()  {
/*
    *
   **
  ***
 ****
*****

 */

	for i:=1; i<=5; i++ {

		for j:=1; j<=5-i; j++ {
			fmt.Print("-")
		}

		for k:=1; k<=i; k++ {
			fmt.Print("*")
		}
		fmt.Println()

	}

}

func f5()  {
/*
    *****
   *****
  *****
 *****
*****

*/

	for i:=1; i<=5; i++ {
		for j:=1; j<=5-i; j++ {
			fmt.Print("-")
		}

		for k:=1; k<=5; k++ {
			fmt.Print("*")
		}
		fmt.Println()

	}
}




func f8() {
/*
使用数字：0-9，打印出所有的3位数。
 */
	for i:=1; i<=9; i++ {
		for j:=0; j<=9; j++{
			for k:=0; k<=9; k++ {
				//fmt.Printf("%d%d%d ", i, j, k)
				fmt.Println(i*100 + j*10 + k)
			}
			fmt.Println()
		}
	}
}









