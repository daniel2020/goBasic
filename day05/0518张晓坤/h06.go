package main

import "fmt"

/*
作业6：一个四位数，恰好等于去掉它的首位数字之后所剩的三位数的3倍，这个四位数是多少。
 */
func main()  {

	for i:=1000; i<10000; i++ {
		if i % 1000 * 3 == i {
			fmt.Println(i)
		}

	}


}
