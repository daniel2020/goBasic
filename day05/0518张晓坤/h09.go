package main

import (
	"math"
	"fmt"
)

/*
作业9：有一个两位数，如果在它的前面添加一个3，可以得到一个三位数，
把3添加在它的后面，也可以得到一个三位数。这两个三位数相差468，
求原来的两位数。

*/
func main()  {

	for i:=10; i<100; i++ {
		a := i + 300
		b := i*10 + 3

		if (math.Abs(float64(a-b)) == 468) {
			fmt.Println(i)
		}
	}
}
