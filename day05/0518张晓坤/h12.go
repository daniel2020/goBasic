package main

import "fmt"

/*
作业12：
*******
 *****
  ***
   *
*/
func main()  {
	for i:=1; i<=4; i++ {

		for j:=1; j<i; j++ {
			fmt.Print("-")
		}

		for k:=1; k<=2*(4-i+1)-1; k++ {
			fmt.Print("*")
		}

		fmt.Println()
	}
}

