package main

import (
	"fmt"
	"math/rand"
	"time"
)

func main()  {

	for count:=0; count<=5; count++{

		//是否继续游戏
		var going string
		fmt.Println("")
		fmt.Println("\n\n=======================================")
		fmt.Println("石头剪子步游戏\n是否继续玩游戏(Y/y:继续)：")
		fmt.Scanln(&going)
		if (going != "Y") && (going != "y") {
			break
		}

		//系统出
		rand.Seed(time.Now().UnixNano())
		systemPlaer := rand.Intn(3) + 1

		//玩家出
		fmt.Print("1:剪子\t2:石头\t3:布\n"+"请输入:")
		var player int
		fmt.Scanln(&player)

		if !(player >= 1 && player <= 3) {
			fmt.Println("输入有误，重来")
			continue
		}

		fmt.Println("player:", player, "systemPlaer:", systemPlaer)

		//搏杀
		var result string
		if player == systemPlaer {			//平局
			result = "dogfall"
		} else {
			if fight(player, systemPlaer) {	//非平局，搏杀
				result = "win"
			} else {
				result = "lose"
			}
		}

		//稍微暂停
		//stop := (rand.Float64() * math.Pow10(8));
		time.Sleep(100000000)

		//显示结果
		switch result {
		case "dogfall":
			fmt.Println("咱这默契没毛病~~,老铁")
		case "win":
			fmt.Println("厉害哟～～你赢了")
		case "lose":
			fmt.Println("怂了吧， 你输了")
		}

	}

}

/**
retun a 是否赢
 */
func fight(a int, b int) (win bool)  {
	if a == 1 && b == 3 {
		b = 0
	}
	if a == 3 && b == 1 {
		a = 0
	}

	return a>b;
}

func testRandom()  {
	for i:=1; i<=100; i++ {
		rand.Seed(time.Now().UnixNano())
		fmt.Println(rand.Intn(3) + 1)
		time.Sleep(1000000)
	}
}



