/*
作业5：两个自然数x，y相除，商3余10，被除数，除数，商，余数的和是163，
求被除数，除数

x 除 y = 3 ...10
3y + 10 = x


x + y + 3 + 10 = 163
x + y = 150

3y + 10 + y = 150


*/
package main

import "fmt"

func main()  {

	for x :=1; x <=150; x++ {
		for y:=1; y <= 150; y++ {
			if (3 * y + 10 == x) && (x + y == 150) {
				fmt.Printf("x:%d, y:%d\n", x, y)
			}
		}
	}
}
