package main

import "fmt"

/**
数组声明
元素访问
赋值
遍历

 */
func main()  {
//	var 数组名 [数组大小]元素的类型
	var arr [5]int

	//赋值
	arr[0] = 1
	arr[1] = 2
	arr[2] = 3
	arr[3] = 4
	arr[4] = 5

	//赋值
	fmt.Println(arr[0])
	fmt.Println(arr[1])
	fmt.Println(arr[2])
	fmt.Println(arr[3])
	fmt.Println(arr[4])
	fmt.Println("===================")

	//遍历
	for i:=0; i<len(arr); i++ {
		fmt.Println(arr[i])
	}
	fmt.Println("+++++++++++++++++")

	//用range 遍历数组
	for i,v := range arr {
		fmt.Printf("index:%d---v:%d\n", i, v)
	}
	fmt.Println("_________________")
	for _,v := range arr {
		fmt.Println(v)
	}
}

