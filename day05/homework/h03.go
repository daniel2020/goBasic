package main

import "fmt"

/**
作业3：统计1月-12月，奇数月的总天数。
 */
func main()  {

	var daySum int

	for i:=1; i<=12;  i+=2 {
		days := getDaysOfMonth(i)
		daySum += days

		fmt.Print(i, "月， ")
	}

	fmt.Println("奇数月的总天数:", daySum)
}


/**
根据指定月份的天数
 */
func getDaysOfMonth(month int) (days int) {
	//var days int
	switch month {
	case 1,3,5,7,8,10,12:
		days = 31
	case 4,6,9,11:
		days = 30
	case 2:
		days = 28
	}
	return days
}

