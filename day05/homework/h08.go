package main

import "fmt"

/*

作业8：某数学竞赛中，参赛人数大约在380-450之间。
比赛结果，全体考生的总平均分为76分，男生的平均分为75，女生的平均分为80.1，
求男女生各有多少人。

 */
func main()  {

	for count:=380; count<=450; count++ {

		for man:=1; man<=count; man++ {
			//男生总分 + 女生总分 = 全班总分
			//男生人数*男生平均分 + 女生人数*女生平均分 = 全班人数*全班平均分
			if (75 * man + int(80.1 * float64(count-man)) == 76 * count) {
				fmt.Printf("男生人数:%d, 女生人数:%d, 总人数:%d\n", man, count-man, count)

				//fmt.Printf("count:%d, man:%d, woman:%.f, 男总:%d, 女总:%f, 总:%d\n",
				//	count, man, float64(count-man) ,75*man ,80.1*float64(count-man), 76*count)
			}
		}

	}
	fmt.Println("11111111")

}
