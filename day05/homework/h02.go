package main

import "fmt"

func main()  {
	//作业2：统计1月-5月的总天数。

	daySum := 0

	for month :=1; month <=5; month++ {
		days := getDaysOfMonth(month)
		daySum += days
	}

	fmt.Println("１月－５月的总天数：", daySum)


}

/**
根据指定月份的天数
 */
func getDaysOfMonth(month int) (days int) {
	//var days int
	switch month {
	case 1,3,5,7,8,10,12:
		days = 31
	case 4,6,9,11:
		days = 30
	case 2:
		days = 28
	}
	return days
}

