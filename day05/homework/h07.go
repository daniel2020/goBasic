package main

import "fmt"

/*
作业7:打印出1-500所有的自然数中不包含4的自然数-------位数不确定情况下，拆数要考虑几位；倒过来，拼接数则不用考虑
 */
func main()  {


	for i:=0; i<=5;  i++{			//百位
		if i==4 {
			continue
		}
		for j:=0; j<=9; j++ {		//十位
			if j==4 {
				continue
			}
			for k:=1; k<=9; k++ {	//个位
				if ( k==4 ){
					continue
				}
				fmt.Println(i*100+j*10+k)
			}

		}
	}
}
