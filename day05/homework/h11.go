package main

import "fmt"

/*
作业11：
    *
   ***
  *****
 *******
*********

*/
func main()  {

	for i:=1; i<=5; i++ {

		for j:=1; j <= 5-i; j++ {
			fmt.Print("-")
		}

		for k:=1; k<=i*2-1; k++ {
			fmt.Print("*")
		}

		fmt.Println()
	}
}
