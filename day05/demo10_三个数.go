package main

import "fmt"

func main()  {
	/*
	比较三个数:  从小到大输出
	 */

	a,b,c := 1,2,3
	//1.比较ａ、ｂ
	if a > b {
		a,b = b,a
	}

	//2.比较ａ、ｃ
	if a > c {
		a,c = c,a
	}

	//3.比较ｂ、ｃ
	if b > c {
		b,c = c,b
	}

	fmt.Printf("%d <= %d <= %d\n", a, b, c)
}

