package main

import "fmt"

func main()  {
	//f1()
	//f2()
	//f3()
	f4()

}


/*
练习1：打印输出1-100内，能被3整除，但是不能被5整除的数，只打印输出前5个即可。
练习2：素数题目中，可否使用break或continue
练习3：多层循环嵌套，break，continue，结束是哪一层？
*/

func f1()  {
	//练习1：打印输出1-100内，能被3整除，但是不能被5整除的数，只打印输出前5个即可。
	count := 0
	for i:=1; i<=100; i++ {
		if (i % 3 == 0) && (i % 5 != 0) {
			count++
			if (count > 5) {
				break
			}
			fmt.Println(i)

		}
	}
}

func f2()  {
	//练习2：素数题目中，可否使用break或continue

	count := 0	// 素数个数
	for j:=2; j<=100; j++ {
		num := j
		flag := true
		for i:=2; i<num; i++ {
			if num % i == 0 {
				flag = false
				break;
			}
		}

		if flag {
			count++
			fmt.Println(num)
		}
	}
	fmt.Println("素数个数:", count)
}

func f3()  {
	//练习3：多层循环嵌套，break，continue，结束是哪一层？
	for i:=1; i<=5; i++ {
		for j:=1; j<=5; j++ {
			if j == 3 {
				continue
			}
			if i == 4 {
				break
			}
			fmt.Println("i:", i, ",j:", j)
		}
	}
}

//跳到外层循环
func f4()  {
	out:for i:=1; i<=5; i++ {
		for j:=1; j<=5;  j++{
			if j == 3 {
				continue break
			}
			fmt.Println("i:", i, "j:", j)
		}
	}
}

