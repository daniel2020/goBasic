package main

import "fmt"

func main()  {
	f7()
}
func f7()  {
	/*
	水仙花数
	 水仙花数：三位数：[100,999]
			每个位上的数字的立方和，刚好等于该数字本身。那么就叫水仙花数，4个
			比如：153
				1*1*1 + 5*5*5 + 3*3*3 = 1+125+27=153
	 */

	for i:=100; i<1000; i++ {

		num := i
		a := num / 100
		b := num % 100 / 10
		c := num % 10

		//fmt.Println(a, b, c)

		if num == (a*a*a + b*b*b + c*c*c) {
			fmt.Println(num)
		}
	}
}