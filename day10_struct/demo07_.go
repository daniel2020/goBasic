package main

import "fmt"

type Book struct {
	bookName string
	price float64
}

type Student struct {
	name string
	age int
	books []*Book
}


func main()  {
	//1创建书
	b1 := Book{"孙子兵法", 12.3}
	b2 := Book{"红楼梦", 77}
	b3 := Book{"西游记", 88.9}
	//2创建书架
	books := make([]*Book, 0, 10)
	books = append(books, &b1, &b2, &b3)

	//3.创建学生
	stu := Student{"小明", 15, books}
	stu2:= Student{"小红", 17,
	[]*Book{{"笑傲江湖",79.2},{"金蛇传", 88}, {"射雕英雄传", 76}}}


	//4.学生的容器
	ss := make([]*Student, 0, 10)
	ss = append(ss, &stu, &stu2)

	for i:=0; i<len(ss); i++ {
		student := ss[i]
		fmt.Printf("第%d个学生信息:", i+1)
		fmt.Printf("\t姓名:%s\n", student.name)
		fmt.Printf("\t年龄:%d\n", student.age)

		books := student.books
		if len(books) == 0 {
			fmt.Println("\t\t该学生不看书")
		} else {
			for j:=0; j<len(books); j++ {
				bookP := books[j]
				fmt.Printf("\t\t第%d本书:书名:%s\t价格:%.2f\n", j+1, bookP.bookName, bookP.price)
			}

		}
	}





}


