package main

import "fmt"

//1.定义父类
type Person struct {
	name string
	age int
}

//2.定义子类
type Student struct {
	Person		//模拟继承结构
	school string
}


func main()  {
	//创建父类对象
	p1 := Person{"小明", 18}
	fmt.Println(p1.name, p1.age)

	//2.创建子类对象
	var s1 Student
	s1.name = "lisi"
	s1.age = 19
	s1.school = "北京大学"

	fmt.Println(s1.name, s1.age, s1.school)

	s2 := Student{Person{"zhangsan", 23}, "清华大学"}
	fmt.Println(s2.name, s2.age, s2.school)


	s3 := Student{Person:Person{name:"rose",age:22}, school:"北师大"}
	fmt.Println(s3.Person)
	fmt.Println(s3.Person.name, s3.Person.age, s3.school)



}

