package main

import "fmt"

type cat struct {
	name string
	age int
}

func changeName1(c cat) {
	c.name = "花花"
}

func changeName2(c *cat)  {
	c.name = "默默"
}

func getCat() cat {
	c := cat{"露娜", 1}
	return c
}

func getCat2() *cat {
	c := new(cat)
	c.name = "露娜"
	c.age = 2
	return c
}

func main()  {
	c1 := cat{"tom", 1}
	fmt.Println(c1)
	changeName1(c1)
	fmt.Println(c1)

	changeName2(&c1)
	fmt.Println(c1)


	//出参
	c2 := getCat()
	c3 := getCat2()


}

