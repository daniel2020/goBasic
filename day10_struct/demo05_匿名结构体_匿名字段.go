package main

import "fmt"

type car struct {
	color string
}

type student struct {
	//name string
	//age int
	string		//匿名字段
	int
}

func main()  {

	//匿名函数
	func () {
		fmt.Println("11111")
	}()

	c := car{color:"火红色"}
	fmt.Println(c)


	//匿名结构体
	p1 := struct {
		name string
		age int
	}{
		name:"zhansan",
		age:30,
	}
	fmt.Println(p1.name, p1.age)



	//匿名字段
	stu := student{"zhangsan", 18}
	fmt.Println(stu)
	fmt.Println(stu.string, stu.int)

}