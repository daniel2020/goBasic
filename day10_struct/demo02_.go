package main

import "fmt"

type Dog struct {
	color string
	age int
	sex string
	kind string
}

func main()  {
	var d1 Dog
	d1.color = "yellow"
	d1.age = 2
	d1.sex = "男"
	d1.kind = "雪橇"

	fmt.Println(d1)

	d2 := Dog{color:"black", age:3, sex:"男", kind:""}
	fmt.Println(d2.color, d2.age)

	d3 := Dog{"white", 4, "女", "泰迪"}
	fmt.Println(d3.color, d3.age, d3.sex)


	fmt.Println("d3:", d3)
	d4 := d3
	d4.age = 5
	fmt.Println("d3:", d3)
	fmt.Println("d4:", d4)

	fmt.Printf("")


	fmt.Println("============")
	d5 := new(Dog)
	d5.color = "黄色"
	d5.age = 2
	d5.kind = "中华田园犬"
	d6 := d5
	fmt.Println("d5:", d5, "d6", d6)
	d6.age = 3
	d6.color = "黄黑----"
	fmt.Println("d5:", d5, "d6", d6)

	fmt.Printf("%T,%T\n", d5, d6)
	fmt.Printf("%p,%p\n", d5, d6)






}
