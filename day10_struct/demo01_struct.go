package main

import "fmt"

type Person struct {
	name string
	age int
	sex string
}

func main()  {
	//创建对象  方式1
	var p1 Person
	fmt.Println(p1)

	p1.name = "zhansan"
	p1.age = 20
	p1.sex = "男"
	fmt.Println(p1)
	fmt.Printf("姓名:%s,年龄:%d,性别:%s\n", p1.name, p1.age, p1.sex)

	//创建对象  方式2
	p2 := Person{}
	p2.name = "rose"
	p2.age = 11
	p2.sex = "女"
	fmt.Printf("姓名:%s,年龄:%d,性别:%s\n", p1.name, p1.age, p1.sex)

	//方式3
	p3 := Person{name:"李晓华", age:28, sex:"女"}
	fmt.Println(p3)

	p4 := Person{
		name:"隔壁老王",
		age:54,
		sex:"男",
	}
	fmt.Println(p4)



	//创建对象  方式4
	p5 := Person{"zhangsan", 71, "男"}
	fmt.Println(p5)

	//创建对象  方式5
	p6 := new(Person)
	fmt.Printf("%T,%T\n", p1, p6)
	(*p6).name = "jerry"
	p6.age = 30
	p6.sex = "女"
	fmt.Println(p6)
	fmt.Printf("%p\n", p6)


}