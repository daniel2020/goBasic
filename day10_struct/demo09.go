package main

import (
	"fmt"
	"goBasic/day10_struct/employee"
)

/*
父类：动物name，age
猫：新增属性color
狗：新增属性kind
*/

type Animal struct {
	name string
	age  int
}

type Cat struct {
	Animal
	color string
}

type Dog struct {
	Animal
	kind string
}

func main() {
	fmt.Println(employee.Aaa)
	e1 := employee.Emp{"zhangsan", 18, 10000.0}
	fmt.Printf("e1:%T\n", e1)
	fmt.Println("e1:", e1)

	e2 := employee.Emp{"lisi", 20, 12000.0}
	fmt.Printf("e2:%T\n", e2)
	fmt.Println("e2:", e2)
}

func f1() {
	dog := Dog{Animal{"啸天", 5}, "大型犬"}
	cat := Cat{Animal: Animal{name: "花花", age: 2}, color: "黄色"}

	fmt.Println(dog.name, dog.age, dog.kind)
	fmt.Println(cat.Animal.name, cat.Animal.age, cat.color)

}
