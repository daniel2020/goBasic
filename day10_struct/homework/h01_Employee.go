package main

import "fmt"

/*
1创建一个结构体Employee,设置字段id,name,age,salary，创建几个结构体对象，分别赋值，并打印。
*/

type Employee struct {
	id int
	name string
	age int
	salary float64
}

func main()  {
	emp1 := Employee{1, "zhangsan", 22, 12500}

	var emp2 Employee
	emp2.id = 2
	emp2.name = "lisi"
	emp2.age = 33
	emp2.salary = 11000

	emp3 := Employee{id:3, name:"wangwu", age:28}
	emp3.salary = 18000

	emps := []*Employee{}
	emps = append(emps, &emp1, &emp2, &emp3)

	//遍历
	for _,emp := range emps {
		fmt.Printf("id:%d,name:%s,age:%d,salary:%.2f\n", emp.id,emp.name,emp.age,emp.salary)
	}



}


