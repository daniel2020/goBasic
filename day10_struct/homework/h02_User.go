package main

import "fmt"

/*
2创建一个结构体User，设置字段username，password，创建几个结构体对象，分别赋值，并打印。
*/
type User struct {
	username string
	password string
}

func main()  {
	u1 := User{username:"zhangsan",password:"123"}
	u2 := User{"lisi", "123456"}
	var u3 User
	u3.username = "wangwu"
	u3.password = "abc123"

	fmt.Println(u1.username, u1.password)
	fmt.Println(u2.username, u2.password)
	fmt.Println(u3)

}


