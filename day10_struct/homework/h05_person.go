package main

import (
	"fmt"
)

/*
5创建Person结构体。属性：姓名，年龄。方法：显示信息。
创建两个结构体：学生和工人。继承Person。分别新增属性成绩和工资
 */

type Person struct {
	name string
	age int
}

type Student struct {
	Person
	score int
}

type Worker struct {
	Person
	salary float64
}

func main()  {
	student := Student{Person{"zhangsan",33}, 95}
	worker := Worker{Person{"lisi", 22}, 8800}

	fmt.Println("student name:", student.name, "age:", student.age, "score:", student.score)
	fmt.Println(worker.name, worker.age, worker.salary)


}

