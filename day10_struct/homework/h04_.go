package main

import "fmt"

/*
4创建一个车结构体，提供属性：颜色，速度。方法：移动()。停止()。
创建两个结构体：自行车，跑车，继承车的结构体。分别新增属性和方法。创建对象，进行测试。
*/

/*
车
 */
type Vehicle struct {	//车
	color string
	speed float64
}

type Bike struct {		//自行车
	Vehicle
	chain string		//车链
	pedal string		//踏板
}

type Roadster struct {	//跑车
	Vehicle
	engine string		//引擎
}

func main()  {
	b1 := Bike{Vehicle:Vehicle{"黄色", 10}, chain:"可调档位车链", pedal:"新踏板"}

	r1 := Roadster{Vehicle{"大红色", 120}, "雷诺引擎"}

	fmt.Printf("颜色:%s, 速度:%.2f, 链条:%s, 踏板:%s\n", b1.color, b1.speed, b1.chain, b1.pedal)
	fmt.Printf("颜色:%s, 速度:%.2f, 引擎:%s\n", r1.color, r1.speed, r1.engine)





}

func add(a,b int) ( int,  bool)  {

	return 10, true
}



















