package main

import "fmt"

/*
3.定义一个结构体表示车轮：内半径，外半径。再定义一个结构体表示小汽车：车名，4个车轮。
*/

type Wheel struct {
	radiusOut float64
	radiusIn float64
}

type Car struct {
	carName string
	wheels []*Wheel
}

func main()  {
	wheels := make([]*Wheel,0,5)

	for i:=0; i<4; i++ {
		w := Wheel{40, 33.5}
		wheels = append(wheels, &w)
	}

	car := Car{"奔驰xxx", wheels}

	fmt.Println("car:", car.carName)
	for i:=0; i<len(car.wheels); i++ {
		fmt.Printf("车轮%d: 外半径:%.2f,内半径:%.2f\n", i, car.wheels[i].radiusOut, car.wheels[i].radiusIn)
	}
}














