package main

import "fmt"

type Student struct {
	name string
	age int
	books []*Book
}

type Book struct {
	bookName string
	price float64
	author string
}

func main()  {
	//1.创建书
	b1 := Book{"天龙八部1", 55.4, "金庸"}
	b2 := Book{"天龙八部2", 56.4, "金庸"}
	b3 := Book{"天龙八部3", 57.4, "金庸"}
	//2.创建书架
	books := []*Book{}
	books = append(books, &b1, &b2, &b3)
	//3.创建学生对象
	s1 := Student{"小明", 15, books}

	fmt.Println(s1)
	s1.books[0].bookName = "天龙八部10"

	fmt.Println(s1)

	fmt.Println(b1 , b2, b3)
	fmt.Println(s1.books[0], s1.books[1], s1.books[2])


	fmt.Printf("%p, %p\n", books, s1.books)
	fmt.Printf("%T,%T,%T\n", s1, s1.books, s1.books[0].bookName)


}

