package main

import "fmt"

type Book struct {
	bookName string
	price float64
	author string

}

type Person struct {
	name string
	age int
	book Book
}

func main()  {
	b1 := Book{}
	b1.bookName = "金瓶梅"
	b1.price = 45.8
	b1.author = "田中天"

	p1 := Person{}
	p1.name = "zhangsan"
	p1.age = 30
	p1.book = b1

	fmt.Println(p1)
	fmt.Println(p1.name, p1.age,
		p1.book.bookName, p1.book.author, p1.book.bookName)



	p2 := Person{name:"lisi",age:28,
	book:Book{bookName:"笑傲江湖",price:2.4, author:"金庸"}}

	fmt.Println(p2)


}
